import {library} from '@fortawesome/fontawesome-svg-core';
import {faInstagram} from "@fortawesome/free-brands-svg-icons/faInstagram";
import {faTwitter} from "@fortawesome/free-brands-svg-icons/faTwitter";
import {faGithub} from "@fortawesome/free-brands-svg-icons/faGithub";
import {faProductHunt} from "@fortawesome/free-brands-svg-icons/faProductHunt";
import {faTelegram} from "@fortawesome/free-brands-svg-icons/faTelegram";
import {faSlack} from "@fortawesome/free-brands-svg-icons/faSlack";
import {faDiscord} from "@fortawesome/free-brands-svg-icons/faDiscord";
import {faTrello} from "@fortawesome/free-brands-svg-icons/faTrello";
import {faGitlab} from "@fortawesome/free-brands-svg-icons/faGitlab";
import {faGithubSquare} from "@fortawesome/free-brands-svg-icons/faGithubSquare";
import {faSoundcloud} from "@fortawesome/free-brands-svg-icons/faSoundcloud";
import {faBell as faBellOutlined} from "@fortawesome/free-regular-svg-icons/faBell";
import {faBell} from "@fortawesome/free-solid-svg-icons/faBell";
import {faClock} from "@fortawesome/free-solid-svg-icons/faClock";
import {faEdit} from "@fortawesome/free-solid-svg-icons/faEdit";
import {faQuestionCircle} from "@fortawesome/free-solid-svg-icons/faQuestionCircle";
import {faGlobe} from "@fortawesome/free-solid-svg-icons/faGlobe";
import {faUsers} from "@fortawesome/free-solid-svg-icons/faUsers";
import {faComments} from "@fortawesome/free-solid-svg-icons/faComments";
import {faPlusSquare} from "@fortawesome/free-solid-svg-icons/faPlusSquare";
import {faReply} from "@fortawesome/free-solid-svg-icons/faReply";
import {faThumbtack} from "@fortawesome/free-solid-svg-icons/faThumbtack";
import {faInfoCircle} from "@fortawesome/free-solid-svg-icons/faInfoCircle";
import {faStream} from "@fortawesome/free-solid-svg-icons/faStream";
import {faTrash} from "@fortawesome/free-solid-svg-icons/faTrash";
import {faChevronLeft} from "@fortawesome/free-solid-svg-icons/faChevronLeft";
import {faHome} from "@fortawesome/free-solid-svg-icons/faHome";
import {faCalendarCheck} from "@fortawesome/free-solid-svg-icons/faCalendarCheck";
import {faShip} from "@fortawesome/free-solid-svg-icons/faShip";
import {faFire} from "@fortawesome/free-solid-svg-icons/faFire";
import {faChartBar} from "@fortawesome/free-regular-svg-icons/faChartBar";
import {faPlus} from "@fortawesome/free-solid-svg-icons/faPlus";
import {faCogs} from "@fortawesome/free-solid-svg-icons/faCogs";
import {faPlug} from "@fortawesome/free-solid-svg-icons/faPlug";
import {faCheck} from "@fortawesome/free-solid-svg-icons/faCheck";
import {faCheckCircle} from "@fortawesome/free-solid-svg-icons/faCheckCircle";
import {faDotCircle} from "@fortawesome/free-solid-svg-icons/faDotCircle";
import {faPencilAlt} from "@fortawesome/free-solid-svg-icons/faPencilAlt";
import {faCamera} from "@fortawesome/free-solid-svg-icons/faCamera";
import {faUserPlus} from "@fortawesome/free-solid-svg-icons/faUserPlus";
import {faCode} from "@fortawesome/free-solid-svg-icons/faCode";
import {faLink} from "@fortawesome/free-solid-svg-icons/faLink";
import {faColumns} from "@fortawesome/free-solid-svg-icons/faColumns";
import {faSun} from "@fortawesome/free-solid-svg-icons/faSun";
import {faArrowCircleRight} from "@fortawesome/free-solid-svg-icons/faArrowCircleRight";
import {faArrowCircleLeft} from "@fortawesome/free-solid-svg-icons/faArrowCircleLeft";
import {faUpload} from "@fortawesome/free-solid-svg-icons/faUpload";
import {faGlobeAmericas} from "@fortawesome/free-solid-svg-icons/faGlobeAmericas";
import {faCheckSquare} from "@fortawesome/free-solid-svg-icons/faCheckSquare";
import {faTasks} from "@fortawesome/free-solid-svg-icons/faTasks";
import {faComment} from "@fortawesome/free-solid-svg-icons/faComment";
import {faSignInAlt} from "@fortawesome/free-solid-svg-icons/faSignInAlt";
import {faRocket} from "@fortawesome/free-solid-svg-icons/faRocket";
import {faEllipsisV} from "@fortawesome/free-solid-svg-icons/faEllipsisV";
import {faMarkdown} from "@fortawesome/free-brands-svg-icons/faMarkdown";
import {faEye} from "@fortawesome/free-solid-svg-icons/faEye";
import {faArrowsAlt} from "@fortawesome/free-solid-svg-icons/faArrowsAlt";
import {faTrophy} from "@fortawesome/free-solid-svg-icons/faTrophy";
import {faStar} from "@fortawesome/free-regular-svg-icons/faStar";
import {faUserCircle} from "@fortawesome/free-solid-svg-icons/faUserCircle";
import {faLock} from "@fortawesome/free-solid-svg-icons/faLock";
import {faBullhorn} from "@fortawesome/free-solid-svg-icons/faBullhorn";
import {faBug} from "@fortawesome/free-solid-svg-icons/faBug";
import {faPlay} from "@fortawesome/free-solid-svg-icons/faPlay";
import {faArrowCircleDown} from "@fortawesome/free-solid-svg-icons/faArrowCircleDown";
import {faSearch} from "@fortawesome/free-solid-svg-icons/faSearch";
import {faShieldAlt} from "@fortawesome/free-solid-svg-icons/faShieldAlt";
import {faBed} from "@fortawesome/free-solid-svg-icons/faBed";
import {faTwitch} from "@fortawesome/free-brands-svg-icons/faTwitch";
import {faMugHot} from "@fortawesome/free-solid-svg-icons/faMugHot";
import {faExternalLinkAlt} from "@fortawesome/free-solid-svg-icons/faExternalLinkAlt";
import {faCog} from "@fortawesome/free-solid-svg-icons/faCog";
import {faSignOutAlt} from "@fortawesome/free-solid-svg-icons/faSignOutAlt";
import {faCalendarAlt} from "@fortawesome/free-solid-svg-icons/faCalendarAlt";

library.add(
    faInstagram,
    faTwitter,
    faGithub,
    faGithubSquare,
    faProductHunt,
    faTelegram,
    faSlack,
    faDiscord,
    faTrello,
    faGitlab,
    faSoundcloud,
    faBell,
    faBellOutlined,
    faClock,
    faEdit,
    faQuestionCircle,
    faGlobe,
    faUsers,
    faComments,
    faPlusSquare,
    faReply,
    faThumbtack,
    faInfoCircle,
    faStream,
    faTrash,
    faChevronLeft,
    faHome,
    faCalendarCheck,
    faShip,
    faFire,
    faChartBar,
    faPlus,
    faCog,
    faCogs,
    faPlug,
    faCheck,
    faCheckCircle,
    faDotCircle,
    faPencilAlt,
    faCamera,
    faUserPlus,
    faCode,
    faLink,
    faColumns,
    faSun,
    faArrowCircleRight,
    faArrowCircleLeft,
    faUpload,
    faGlobeAmericas,
    faCheckSquare,
    faTasks,
    faComment,
    faSignInAlt,
    faSignOutAlt,
    faRocket,
    faEllipsisV,
    faMarkdown,
    faEye,
    faArrowsAlt,
    faTrophy,
    faStar,
    faUserCircle,
    faLock,
    faBullhorn,
    faBug,
    faPlay,
    faArrowCircleDown,
    faSearch,
    faShieldAlt,
    faBed,
    faTwitch,
    faMugHot,
    faExternalLinkAlt,
    faCalendarAlt,
);