import { Link, NavLink } from "react-router-dom";

import Chip from "../../../components/Chip";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import GlobalSearchBar from "../../../features/search/components/GlobalSearchBar";
import { Icon } from "vendor/bulma";
import { NavbarDropdown } from "../../../components/Dropdown";
import { NotificationsLink } from "features/notifications";
import PropTypes from "prop-types";
import React from "react";
import Streak from "components/Streak";
import styled from "styled-components";

const LoggedInMenu = props => (
    <div
        className={props.expanded ? "navbar-menu is-active" : "navbar-menu"}
        onClick={props.onToggleExpand}
    >
        <div className="navbar-start">
            <NavLink
                className="navbar-item"
                activeClassName="is-active"
                to="/log"
            >
                <Icon medium>
                    <FontAwesomeIcon icon={"check-square"} />
                </Icon>{" "}
                Log
            </NavLink>
            <NavbarDropdown
                to="/tasks"
                link={() => (
                    <>
                        <Icon medium>
                            <FontAwesomeIcon icon={"check"} />
                        </Icon>{" "}
                        Tasks
                    </>
                )}
            >
                <NavLink
                    className="navbar-item"
                    activeClassName="is-active"
                    to="/tasks"
                    exact
                >
                    <Icon medium>
                        <FontAwesomeIcon icon={"tasks"} />
                    </Icon>{" "}
                    <span>Tasks</span>
                </NavLink>

                <NavLink
                    className="navbar-item"
                    activeClassName="is-active"
                    to="/products"
                    exact
                >
                    <Icon medium>
                        <FontAwesomeIcon icon={"ship"} />
                    </Icon>{" "}
                    <span>Products</span>
                </NavLink>

                <NavLink
                    className="navbar-item"
                    activeClassName="is-active"
                    to="/apps"
                    exact
                >
                    <Icon medium>
                        <FontAwesomeIcon icon={"plug"} />
                    </Icon>{" "}
                    <span>Integrations</span>
                </NavLink>

                <NavLink
                    className="navbar-item"
                    activeClassName="is-active"
                    to="/streak"
                    exact
                >
                    <Icon medium>
                        <FontAwesomeIcon icon={"fire"} />
                    </Icon>{" "}
                    <span>Streak</span>
                </NavLink>
            </NavbarDropdown>

            <NavLink
                className="navbar-item"
                activeClassName="is-active"
                to="/discussions"
            >
                <Icon medium>
                    <FontAwesomeIcon icon={"comment"} />
                </Icon>{" "}
                Talk
            </NavLink>

            <NavbarDropdown
                to="/explore"
                link={() => (
                    <>
                        <Icon medium>
                            <FontAwesomeIcon icon={"globe-americas"} />
                        </Icon>{" "}
                        More
                    </>
                )}
            >
                <NavLink
                    className="navbar-item"
                    activeClassName="is-active"
                    to="/events"
                    exact
                >
                    <Icon medium>
                        <FontAwesomeIcon icon={"check-circle"} />
                    </Icon>{" "}
                    <span>Events</span>
                </NavLink>
                <NavLink
                    className="navbar-item"
                    activeClassName="is-active"
                    to="/explore"
                    exact
                >
                    <Icon medium>
                        <FontAwesomeIcon icon={"fire"} />
                    </Icon>{" "}
                    <span>Popular</span>
                </NavLink>
                <NavLink
                    className="navbar-item"
                    activeClassName="is-active"
                    to="/live"
                    exact
                >
                    <Icon medium>
                        <FontAwesomeIcon icon={"play"} />
                    </Icon>{" "}
                    <span>Live</span>
                </NavLink>
                <NavLink
                    className="navbar-item"
                    activeClassName="is-active"
                    to="/explore/products"
                    exact
                >
                    <Icon medium>
                        <FontAwesomeIcon icon={"ship"} />
                    </Icon>{" "}
                    <span>Products</span>
                </NavLink>
            </NavbarDropdown>
        </div>
        <div className="navbar-end">
            <GlobalSearchBar />

            <NotificationsLink />

            <div className="navbar-item has-dropdown is-hoverable">
                {
                    // eslint-disable-next-line
                }{" "}
                <a className="navbar-link">
                    <Chip>
                        <img
                            alt={props.user.username}
                            src={props.user.avatar}
                        />
                        <div>
                            <Streak days={props.user.streak} />
                        </div>
                    </Chip>
                </a>
                <div className="navbar-dropdown is-right">
                    {!props.user.gold && (
                        <a
                            className={"navbar-item is-gold"}
                            href={"https://makerlog.io/gold"}
                            target={"_blank"}
                            rel="noopener noreferrer"
                        >
                            <Icon>
                                <FontAwesomeIcon icon={"check-circle"} />
                            </Icon>{" "}
                            Get Gold &raquo;
                        </a>
                    )}
                    <NavLink
                        className="navbar-item"
                        to={`/@${props.user.username}`}
                    >
                        <Icon>
                            <FontAwesomeIcon icon={"user-circle"} />
                        </Icon>{" "}
                        You
                    </NavLink>

                    <NavLink className="navbar-item" to={`/chat`}>
                        <Icon>
                            <FontAwesomeIcon icon={["fab", "telegram"]} />
                        </Icon>{" "}
                        Chat
                    </NavLink>

                    <a
                        className={"navbar-item"}
                        href={"https://pm.mattei.dev/projects/makerlog/issues"}
                        target={"_blank"}
                        rel="noopener noreferrer"
                    >
                        <Icon>
                            <FontAwesomeIcon icon={"info-circle"} />
                        </Icon>{" "}
                        Feedback
                    </a>

                    <NavLink className="navbar-item" to={`/settings`}>
                        <Icon>
                            <FontAwesomeIcon icon={"cog"} />
                        </Icon>{" "}
                        Settings
                    </NavLink>

                    <a
                        className="navbar-item"
                        href={"/logout"}
                        onClick={e => {
                            e.preventDefault();
                            props.onClickLogout();
                        }}
                    >
                        <Icon>
                            <FontAwesomeIcon icon={"sign-out-alt"} />
                        </Icon>{" "}
                        Sign out
                    </a>
                </div>
            </div>
        </div>
    </div>
);

LoggedInMenu.propTypes = {
    onClickLogout: PropTypes.func.isRequired,
    isSyncing: PropTypes.bool.isRequired,
    user: PropTypes.shape({
        avatar: PropTypes.string
    })
};

export default LoggedInMenu;
