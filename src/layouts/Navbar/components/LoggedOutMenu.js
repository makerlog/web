import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import GlobalSearchBar from "../../../features/search/components/GlobalSearchBar/index";
import { Icon } from "vendor/bulma";
import { NavLink } from "react-router-dom";
import { NavbarDropdown } from "components/Dropdown";
import React from "react";

const LoggedOutMenu = props => (
    <div
        className={props.expanded ? "navbar-menu is-active" : "navbar-menu"}
        onClick={props.onToggleExpand}
        id="navMenu"
    >
        <div className="navbar-start">
            <NavLink
                className="navbar-item"
                activeClassName="is-active"
                exact
                to="/"
            >
                <Icon medium>
                    <FontAwesomeIcon icon={"home"} />
                </Icon>{" "}
                Home
            </NavLink>

            <NavLink
                className="navbar-item"
                activeClassName="is-active"
                to="/discussions"
            >
                <Icon medium>
                    <FontAwesomeIcon icon={"comment"} />
                </Icon>{" "}
                Talk
            </NavLink>

            <NavbarDropdown
                to="/explore"
                link={() => (
                    <>
                        <Icon medium>
                            <FontAwesomeIcon icon={"globe-americas"} />
                        </Icon>{" "}
                        More
                    </>
                )}
            >
                <NavLink
                    className="navbar-item"
                    activeClassName="is-active"
                    to="/events"
                    exact
                >
                    <Icon medium>
                        <FontAwesomeIcon icon={"check-circle"} />
                    </Icon>{" "}
                    <span>Events</span>
                </NavLink>
                <NavLink
                    className="navbar-item"
                    activeClassName="is-active"
                    to="/explore"
                    exact
                >
                    <Icon medium>
                        <FontAwesomeIcon icon={"fire"} />
                    </Icon>{" "}
                    <span>Popular</span>
                </NavLink>
                <NavLink
                    className="navbar-item"
                    activeClassName="is-active"
                    to="/live"
                    exact
                >
                    <Icon medium>
                        <FontAwesomeIcon icon={"play"} />
                    </Icon>{" "}
                    <span>Live</span>
                </NavLink>
                <NavLink
                    className="navbar-item"
                    activeClassName="is-active"
                    to="/explore/products"
                    exact
                >
                    <Icon medium>
                        <FontAwesomeIcon icon={"ship"} />
                    </Icon>{" "}
                    <span>Products</span>
                </NavLink>
            </NavbarDropdown>
        </div>
        <div className="navbar-end">
            <GlobalSearchBar />
            <NavLink
                className="navbar-item is-brand-green"
                activeClassName="is-active"
                to="/begin"
            >
                <Icon medium>
                    <FontAwesomeIcon icon={"rocket"} />
                </Icon>{" "}
                Join us
            </NavLink>

            <NavLink
                className="navbar-item"
                activeClassName="is-active"
                to="/login"
            >
                <Icon medium>
                    <FontAwesomeIcon icon={"sign-in-alt"} />
                </Icon>{" "}
                Log in
            </NavLink>
        </div>
    </div>
);

export default LoggedOutMenu;
