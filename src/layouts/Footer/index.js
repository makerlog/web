import React from "react";
import "./style.css";
import { Link } from "react-router-dom";
import { Level } from "../../vendor/bulma";
import { withTheme } from "styled-components";

const Footer = props => (
    <footer id="footer" className={props.withMargin ? "" : "no-margin-top"}>
        <div className={"container"}>
            <Level>
                <Level.Left>
                    <Level.Item
                        style={{ width: "100%" }}
                        className="has-hv-aligned-content branded"
                    >
                        <div>
                            <a
                                rel="noopener noreferrer"
                                target="_blank"
                                href="https://sergiomattei.com"
                            >
                                <img
                                    src={
                                        props.theme.isDark
                                            ? "https://sergiomattei.com/img/Arkus-Fatter.png"
                                            : "https://sergiomattei.com/img/Arkus-Fatter-Black.png"
                                    }
                                    alt="Sergio Mattei's Logo"
                                />
                            </a>
                        </div>
                    </Level.Item>
                </Level.Left>
                <Level.Right>
                    <Level.Item centered>
                        <Link to={"/about"}>About</Link>
                    </Level.Item>
                    
                    <Level.Item />

                    <Level.Item>
                        <a
                            href="https://blog.getmakerlog.com"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            Blog
                        </a>
                    </Level.Item>

                    <Level.Item />

                    <Level.Item>
                        <a
                            href="https://gold.getmakerlog.com"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            Gold
                        </a>
                    </Level.Item>

                    <Level.Item />

                    <Level.Item>
                        <a
                            href="https://status.getmakerlog.com"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            Status
                        </a>
                    </Level.Item>

                    <Level.Item />

                    <Level.Item>
                        <a href="https://open.getmakerlog.com">Open</a>
                    </Level.Item>

                    <Level.Item />

                    <Level.Item>
                        <Link to={"/migrate"}>Migrate</Link>
                    </Level.Item>

                    <Level.Item />

                    <Level.Item>
                        <a
                            href="https://pm.mattei.dev/projects/makerlog/issues"
                            rel="noopener noreferrer"
                        >
                            Feedback
                        </a>
                    </Level.Item>

                    <Level.Item />

                    <Level.Item>
                        <a
                            href="https://twitter.com/getmakerlog"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            Twitter
                        </a>
                    </Level.Item>
                </Level.Right>
            </Level>
        </div>
    </footer>
);

export default withTheme(Footer);
