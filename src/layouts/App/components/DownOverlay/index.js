import React from 'react';
import './index.scss';
import {Icon, Level, SubTitle, Title} from "../../../../vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome/src/components/FontAwesomeIcon";
import OutboundLink from "../../../../components/OutboundLink";

export default (props) => (
    <div className={"DownOverlay"}>
        <Title is={"5"} className={"brand"}>Makerlog</Title>
        <div>
            <center>
                <Title>Uh oh, something went wrong.</Title>
                <SubTitle is={"5"}>Makerlog is a little overloaded or down due to maintenance.</SubTitle>
                <br />
                <Level>
                    <Level.Item>
                        <OutboundLink className={"button is-text"} href={"https://twitter.com/getmakerlog"}>
                            <Icon><FontAwesomeIcon color={"#3498db"} icon={['fab', 'twitter']} /></Icon> <span className={"has-text-grey"}>Check Twitter</span>
                        </OutboundLink>
                    </Level.Item>

                    <Level.Item>
                        <OutboundLink className={"button is-text"} href={"https://t.me/makerlog"}>
                            <Icon><FontAwesomeIcon color={"#3498db"} icon={['fab', 'telegram']} /></Icon> <span className={"has-text-grey"}>Talk on Telegram</span>
                        </OutboundLink>
                    </Level.Item>

                    <Level.Item>
                        <OutboundLink className={"button is-text"} href={"https://status.getmakerlog.com"}>
                            <Icon><FontAwesomeIcon color={"#47E0A0"} icon={'check-circle'} /></Icon> <span className={"has-text-grey"}>Platform Health</span>
                        </OutboundLink>
                    </Level.Item>
                </Level>
            </center>
        </div>
    </div>
)