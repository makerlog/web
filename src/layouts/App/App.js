import React from "react";
import { ConnectedRouter } from "react-router-redux";
import { connect } from "react-redux";
import { PersistGate } from "redux-persist/lib/integration/react";
import { history, persistor } from "store";
import Spinner from "components/Spinner";
import routes from "./routes";
import { actions as statsActions } from "ducks/stats";
import { actions as tasksActions } from "ducks/tasks";
import { actions as appActions } from "ducks/app";
import { NotificationsView } from "features/notifications";
import { Editor } from "features/stream";
import Helmet, { HelmetProvider } from "react-helmet-async";
import Colors from "styles/colors";
import { ThemeProvider } from "styled-components";
import DownOverlay from "./components/DownOverlay/index";
import { Redirect } from "react-router-dom";

class App extends React.Component {
    componentDidMount() {
        this.props.appInit();
        this.checkDemoMode();
    }

    componentWillUnmount() {
        this.exitDarkMode();
    }

    getColor = () => {
        if (!this.props.user || !this.props.user.gold) return null;

        return this.props.user.accent ? this.props.user.accent : null;
    };

    isDark = () => {
        if (this.timer) return true;
        if (!this.props.user || !this.props.user.gold) return false;

        return this.props.user.dark_mode ? this.props.user.dark_mode : false;
    };

    enterDarkMode = () => {
        console.log("Makerlog: Entering the dark si- err, mode...");
        if (!document.body.classList.contains("is-dark-mode")) {
            document.body.classList.add("is-dark-mode");
        }
    };

    exitDarkMode = () => {
        console.log("Makerlog: Exiting the dark side...");
        if (document.body.classList.contains("is-dark-mode")) {
            document.body.classList.remove("is-dark-mode");
        }
        this.timer = null;
    };

    checkDemoMode = () => {
        if (window.location.search.includes("dark-mode-demo")) {
            this.enterDarkMode();
            this.timer = setTimeout(() => {
                this.exitDarkMode();
            }, 10000);
        }
    };

    render() {
        const theme = this.getColor()
            ? new Colors(this.getColor(), this.isDark())
            : new Colors();

        if (this.isDark()) {
            console.log("Dark enabled");
            this.enterDarkMode();
        } else {
            this.exitDarkMode();
        }

        if (!this.props.apiHealthy) {
            return <DownOverlay />;
        }

        return (
            <ThemeProvider theme={theme}>
                <PersistGate loading={<Spinner />} persistor={persistor}>
                    <ConnectedRouter history={history}>
                        <HelmetProvider>
                            <div>
                                <Helmet>
                                    <title>
                                        Makerlog{" "}
                                        {process.env.NODE_ENV === "development"
                                            ? "(local)"
                                            : ""}
                                    </title>
                                    <meta
                                        name="description"
                                        content="Makerlog is the dead-simple task log that helps you stay productive and ship faster."
                                    />
                                </Helmet>

                                {routes}

                                {this.props.isNewUser ? (
                                    <Redirect to="/welcome" />
                                ) : null}

                                <NotificationsView />
                                <Editor />
                            </div>
                        </HelmetProvider>
                    </ConnectedRouter>
                </PersistGate>
            </ThemeProvider>
        );
    }
}

const mapStateToProps = state => {
    return {
        isNewUser: state.app.isNewUser,
        user: state.user.me ? state.user.me : null,
        isLoggedIn: state.auth.loggedIn,
        apiHealthy: state.app.healthy,
        feedbackOpen: state.app.feedbackOpen,
        token: state.auth.token,
        statsAlreadyLoaded: state.stats.ready,
        tasksAlreadyLoaded: state.tasks.ready
    };
};

const mapDispatchToProps = dispatch => {
    return {
        loadStats: (silently = false) =>
            dispatch(statsActions.fetchStats(silently)),
        loadTasks: () => dispatch(tasksActions.loadTasks()),
        appInit: () => dispatch(appActions.appInit()),
        toggleFeedback: () => dispatch(appActions.toggleFeedback())
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
