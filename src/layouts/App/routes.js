import { Button, Container, Hero, Title } from "vendor/bulma";
import { Route, Switch } from "react-router-dom";
import { userIsAuthenticated, userIsNotAuthenticated } from "./authRules";

import DiscussionsPage from "../../pages/DiscussionsPage";
import ForgotPage from "../../pages/ForgotPage/index";
import HomePage from "../../pages/HomePage/index";
import Loadable from "react-loadable";
import LoginPage from "pages/LoginPage";
import MigratePage from "pages/MigratePage";
import MyProductsPage from "pages/MyProductsPage";
import NotFound from "../../pages/Error/NotFound/index";
import OnboardingPage from "pages/OnboardingPage";
import Page from "../Page";
import React from "react";
import RegisterPage from "../../pages/RegisterPage/index";
import SearchPage from "../../pages/SearchPage";
import StreakPage from "pages/StreakPage";
import StreamPage from "../../pages/StreamPage/index";
import { SubTitle } from "../../vendor/bulma";
import TasksPage from "../../pages/TasksPage/index";
import UserPage from "../../pages/ProfilePage/index";

function Loading(props) {
    if (props.error) {
        return (
            <Page transparent className={"hero-page"}>
                <Hero fullheight>
                    <Hero.Body>
                        <Container>
                            <Title className={"has-text-white"}>
                                Uh oh, something went wrong.
                            </Title>
                            <SubTitle className={"has-text-white"}>
                                We pushed an update, which is usually what
                                causes this error.
                            </SubTitle>
                            <Button
                                className={"is-rounded"}
                                onClick={() => window.location.reload(false)}
                            >
                                Retry
                            </Button>
                        </Container>
                    </Hero.Body>
                </Hero>
            </Page>
        );
    } else if (props.timedOut || props.pastDelay) {
        return <Page loading={true} />;
    } else {
        return null;
    }
}

const LoadableAboutPage = Loadable({
    loader: () => import("pages/AboutPage"),
    loading: Loading
});

const LoadableExplorePage = Loadable({
    loader: () => import("pages/ExplorePage"),
    loading: Loading
});

const LoadableSettingsPage = Loadable({
    loader: () => import("pages/SettingsPage"),
    loading: Loading
});

const LoadableStreamersPage = Loadable({
    loader: () => import("pages/StreamersPage"),
    loading: Loading
});

const LoadableAppsPage = Loadable({
    loader: () => import("pages/AppsPage"),
    loading: Loading
});

const LoadableEntryPage = Loadable({
    loader: () => import("pages/EntryPage"),
    loading: Loading
});

const LoadableProductPage = Loadable({
    loader: () => import("pages/ProductPage"),
    loading: Loading
});

const LoadableMilestonePage = Loadable({
    loader: () => import("pages/MilestonePage"),
    loading: Loading
});

const LoadableEventsPage = Loadable({
    loader: () => import("pages/EventsPage"),
    loading: Loading
});

const LoadableEventPage = Loadable({
    loader: () => import("pages/EventPage"),
    loading: Loading
});

const LoadableEventHostPage = Loadable({
    loader: () => import("pages/EventHostPage"),
    loading: Loading
});

const LoadableEventAttendeePanel = Loadable({
    loader: () => import("pages/EventAttendeePanel"),
    loading: Loading
});

const LoadableEventStreamersPage = Loadable({
    loader: () => import("pages/EventStreamersPage"),
    loading: Loading
});

const routes = (
    <Switch>
        <Route exact path="/" component={userIsNotAuthenticated(HomePage)} />
        <Route path="/begin" component={userIsNotAuthenticated(RegisterPage)} />
        <Route path="/forgot" component={userIsNotAuthenticated(ForgotPage)} />
        <Route path="/login" component={userIsNotAuthenticated(LoginPage)} />
        <Route path="/log" component={userIsAuthenticated(StreamPage)} />
        <Route
            path="/products/"
            exact
            component={userIsAuthenticated(MyProductsPage)}
        />
        <Route path="/products/:slug" component={LoadableProductPage} />
        <Route path="/milestones/:slug" component={LoadableMilestonePage} />
        <Route path="/tasks/:id(\d+)" component={LoadableEntryPage} />
        <Route path="/tasks" component={userIsAuthenticated(TasksPage)} />
        <Route path="/streak" component={userIsAuthenticated(StreakPage)} />
        <Route path="/apps" component={LoadableAppsPage} />
        <Route
            path="/settings"
            component={userIsAuthenticated(LoadableSettingsPage)}
        />
        <Route path="/migrate" component={userIsAuthenticated(MigratePage)} />
        <Route path="/explore" component={LoadableExplorePage} />
        <Route path="/live" component={LoadableStreamersPage} />
        <Route path="/about" component={LoadableAboutPage} />
        <Route path="/discussions" component={DiscussionsPage} />
        <Route path="/search" component={SearchPage} />
        <Route
            path="/events/host"
            component={userIsAuthenticated(LoadableEventHostPage)}
        />
        <Route
            path="/events/:slug/live/"
            component={LoadableEventStreamersPage}
        />
        <Route
            path="/events/:slug/attendance/"
            component={LoadableEventAttendeePanel}
        />
        <Route path="/events/:slug" component={LoadableEventPage} />
        <Route path="/events" component={LoadableEventsPage} />
        <Route
            path="/welcome"
            component={userIsAuthenticated(OnboardingPage)}
        />
        <Route
            path="/chat"
            component={() => (window.location = "https://t.me/makerlog")}
        />
        <Route path="/@:username" component={UserPage} />
        <Route component={NotFound} />
    </Switch>
);

export default routes;
