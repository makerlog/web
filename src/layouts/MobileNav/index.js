import "./index.scss";

import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import GlobalSearchBar from "../../features/search/components/GlobalSearchBar";
import { NavLink } from "react-router-dom";
import { NotificationsLink } from "../../features/notifications";
import React from "react";
import { connect } from "react-redux";
import { actions as editorActions } from "../../ducks/editor";

const LoggedOutLinks = props => (
    <>
        <NavLink className="item" exact activeClassName="is-active" to="/">
            <span className={"icon"}>
                <FontAwesomeIcon icon={"home"} />
            </span>
            <span>Home</span>
        </NavLink>
        <NavLink
            className="item"
            activeClassName="is-active"
            to="/discussions"
            exact
        >
            <span className={"icon"}>
                <FontAwesomeIcon icon={"comment"} />
            </span>
            <span>Discuss</span>
        </NavLink>
        <NavLink className="item" activeClassName="is-active" to="/begin" exact>
            <span className={"icon"}>
                <FontAwesomeIcon icon={"rocket"} />
            </span>
            <span>Join</span>
        </NavLink>
        <NavLink className="item" activeClassName="is-active" to="/login" exact>
            <span className={"icon"}>
                <FontAwesomeIcon icon={"sign-in-alt"} />
            </span>
            <span>Sign in</span>
        </NavLink>
    </>
);

const LoggedInLinks = props => (
    <>
        <NavLink className="item" activeClassName="is-active" to="/log">
            <span className={"icon"}>
                <FontAwesomeIcon icon={"check-square"} />
            </span>
            <span>Log</span>
        </NavLink>
        <NavLink className="item" activeClassName="is-active" to="/tasks" exact>
            <span className={"icon"}>
                <FontAwesomeIcon icon={"tasks"} />
            </span>
            <span>Tasks</span>
        </NavLink>
        <a className="item">
            <button onClick={props.toggleEditor}>
                <FontAwesomeIcon icon={"plus"} />
            </button>
        </a>
        <NotificationsLink mobile />
        <NavLink className="item" activeClassName="is-active" to="/discussions">
            <span className={"icon"}>
                <FontAwesomeIcon icon={"comments"} />
            </span>
            <span>Talk</span>
        </NavLink>
    </>
);

const MobileNav = props => {
    return (
        <div className={"MobileNav"}>
            {props.isLoggedIn ? (
                <LoggedInLinks toggleEditor={props.toggleEditor} />
            ) : (
                <LoggedOutLinks />
            )}
        </div>
    );
};

const mapDispatchToProps = dispatch => ({
    toggleEditor: () => dispatch(editorActions.toggleEditor())
});

export default connect(
    state => ({
        isLoggedIn: state.auth.loggedIn
    }),
    mapDispatchToProps
)(MobileNav);
