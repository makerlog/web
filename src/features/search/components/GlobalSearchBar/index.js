import "./index.scss";

import { Input, Media, Title } from "../../../../vendor/bulma";
import { Link, NavLink, withRouter } from "react-router-dom";
import {
    searchDiscussions,
    searchProducts,
    searchTasks,
    searchUsers
} from "../../../../lib/search";

import Avatar from "../../../users/components/Avatar/Avatar";
import { StreamCard as Card } from "../../../stream/components/Stream/components/StreamCard/styled";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import { Icon } from "vendor/bulma";
import { ProductList } from "features/products";
import React from "react";
import { Task } from "../../../stream";
import ThreadList from "../../../discussions/components/ThreadList";
import UserMediaList from "../../../users/components/UserMediaList/UserMediaList";
import debounce from "lodash-es/debounce";
import orderBy from "lodash-es/orderBy";

class GlobalSearchBar extends React.Component {
    state = {
        open: false,
        loading: false,
        query: "",
        users: null,
        products: null,
        tasks: null,
        discussions: null,
        failed: false
    };

    onType = e => {
        this.setState({ query: e.target.value });
        this.getPreliminaryResults();
    };

    toggle = () => {
        const open = !this.state.open;
        if (open) {
            this.sc = document.addEventListener(
                "scroll",
                function(e) {
                    e.preventDefault();
                },
                false
            );
        } else {
            if (this.sc) document.removeEventListener("scroll", this.sc);
        }
        this.setState({
            open
        });
    };

    override = e => {
        console.log(e.target);
        if (e.target.matches("#results")) {
            this.toggle();
        } else {
            e.stopPropagation();
        }
    };

    esc = e => {
        e.preventDefault();
        if (e.key === "Escape") this.toggle();
        if (e.key === "Enter") this.navigateToSearch();
    };

    getPreliminaryResults = debounce(async () => {
        this.setState({ loading: true, failed: false });
        try {
            const products = await searchProducts(this.state.query);
            this.setState({
                products: orderBy(products.results, "rank", "asc")
                    .map(el => el.item)
                    .slice(0, 5)
            });
            const users = await searchUsers(this.state.query);
            this.setState({
                users: orderBy(users.results, "rank", "asc")
                    .map(el => el.item)
                    .slice(0, 5)
            });
            const tasks = await searchTasks(this.state.query);
            this.setState({
                loading: false,
                tasks: orderBy(tasks.results, "rank", "asc")
                    .map(el => el.item)
                    .slice(0, 5)
            });
            const discussions = await searchDiscussions(this.state.query);
            this.setState({
                loading: false,
                discussions: orderBy(discussions.results, "rank", "asc")
                    .map(el => el.item)
                    .slice(0, 5)
            });
        } catch (e) {
            this.setState({
                loading: false,
                failed: true,
                products: null,
                users: null,
                tasks: null
            });
        }
    }, 250);

    navigateToSearch = () => {
        this.props.history.push(`/search?q=${this.state.query}`);
    };

    render() {
        const { users, products, tasks, query, discussions } = this.state;

        if (this.state.open) {
            return (
                <div
                    className={"GlobalSearchBar-overlay"}
                    id={"GlobalSearchBar"}
                    onClick={this.override}
                    onKeyUp={this.esc}
                >
                    <div className={"nav-cover"}>
                        <div className={"container form-case"}>
                            <div
                                className={
                                    "control" +
                                    (this.state.loading ? " is-loading" : "")
                                }
                            >
                                <input
                                    className={"input is-large"}
                                    onChange={this.onType}
                                    value={query}
                                    autoFocus={true}
                                    placeholder={
                                        "Search makers, discussions, tasks, and products..."
                                    }
                                />
                            </div>
                            <button
                                className={"search-button"}
                                onClick={this.navigateToSearch}
                            >
                                Search
                            </button>
                        </div>
                    </div>
                    <div
                        className={"container results"}
                        id={"results"}
                        onClick={this.override}
                    >
                        {products && products.length > 0 && (
                            <>
                                <Title is={"6"}>Products</Title>
                                <Card>
                                    <Card.Content>
                                        <ProductList
                                            media
                                            products={this.state.products}
                                        />
                                    </Card.Content>
                                    <Card.Footer>
                                        <Link
                                            onClick={this.toggle}
                                            to={`/search/products?q=${
                                                this.state.query
                                            }`}
                                            className={
                                                "button is-text is-small"
                                            }
                                            href={"#"}
                                        >
                                            See all products &raquo;
                                        </Link>
                                    </Card.Footer>
                                </Card>
                            </>
                        )}
                        {users && users.length > 0 && (
                            <>
                                <Title is={"6"}>Makers</Title>
                                <Card>
                                    <Card.Content>
                                        <UserMediaList
                                            users={this.state.users}
                                        />
                                    </Card.Content>
                                    <Card.Footer>
                                        <Link
                                            onClick={this.toggle}
                                            to={`/search/makers?q=${
                                                this.state.query
                                            }`}
                                            className={
                                                "button is-text is-small"
                                            }
                                            href={"#"}
                                        >
                                            See all makers &raquo;
                                        </Link>
                                    </Card.Footer>
                                </Card>
                            </>
                        )}
                        {tasks && tasks.length > 0 && (
                            <>
                                <Title is={"6"}>Tasks</Title>
                                <Card>
                                    <Card.Content>
                                        {tasks.map(t => (
                                            <Media key={t.id}>
                                                <Media.Left>
                                                    <Avatar
                                                        user={t.user}
                                                        is={32}
                                                    />
                                                </Media.Left>
                                                <Media.Content>
                                                    <Task task={t} />
                                                </Media.Content>
                                            </Media>
                                        ))}
                                    </Card.Content>
                                    <Card.Footer>
                                        <Link
                                            onClick={this.toggle}
                                            to={`/search/tasks?q=${
                                                this.state.query
                                            }`}
                                            className={
                                                "button is-text is-small"
                                            }
                                            href={"#"}
                                        >
                                            See all tasks &raquo;
                                        </Link>
                                    </Card.Footer>
                                </Card>
                            </>
                        )}
                        {discussions && discussions.length > 0 && (
                            <>
                                <Title is={"6"}>Discussions</Title>
                                <Card>
                                    <Card.Content>
                                        <ThreadList threads={discussions} />
                                    </Card.Content>
                                    <Card.Footer>
                                        <Link
                                            onClick={this.toggle}
                                            to={`/search/discussions?q=${
                                                this.state.query
                                            }`}
                                            className={
                                                "button is-text is-small"
                                            }
                                            href={"#"}
                                        >
                                            See all discussions &raquo;
                                        </Link>
                                    </Card.Footer>
                                </Card>
                            </>
                        )}
                    </div>
                </div>
            );
        }

        if (this.props.mobile) {
            return (
                <a onClick={this.toggle} className="item">
                    <span className={"icon"}>
                        <FontAwesomeIcon icon={"search"} />
                    </span>
                    <span>Search</span>
                </a>
            );
        }

        return (
            <a className="navbar-item" onClick={this.toggle}>
                <Icon medium>
                    <FontAwesomeIcon size="lg" icon={"search"} />
                </Icon>
            </a>
        );
    }
}

export default withRouter(GlobalSearchBar);
