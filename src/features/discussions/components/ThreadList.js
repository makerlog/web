import React from 'react';
import {Link} from "react-router-dom";
import {Level} from "../../../vendor/bulma";
import ReplyFaces from "./ReplyFaces";

export default props => {
    return (
        <div className={"RecentQuestionsList"}>
            {props.threads.map((thread) => (
                <Link to={`/discussions/${thread.slug}`}>
                    <div>
                        <h2 className={"question-title"}>
                            {thread.title}
                        </h2>
                        <Level>
                            <Level.Left>
                                <Level.Item className={"has-text-grey-light"}>
                                    {thread.type === 'QUESTION' ? `${thread.reply_count} answers` : `${thread.reply_count} replies`}
                                </Level.Item>
                                <Level.Item>
                                    <ReplyFaces maxFaces={6} withOwner threadSlug={thread.slug} />
                                </Level.Item>
                            </Level.Left>
                        </Level>
                    </div>
                </Link>
            ))}
        </div>
    )
}