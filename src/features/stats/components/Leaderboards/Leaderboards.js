import React from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {Content, Image, Media, Table} from "vendor/bulma";
import Emoji from "../../../../components/Emoji";
import FullName from "../../../users/components/FullName";
import Streak from "../../../../components/Streak";
import Tda from "../../../../components/Tda";
import withProfileModal from "../../../users/containers/ProfileModalAction/withProfileModal";
import UserActivityGraph from "../UserActivityGraph";

const LeaderboardsRow = withProfileModal(
    ({ user, rank, expand, isCurrentUser }) => (
        <Table.Tr className={"Leaderboards Leaderboards-Row"} onClick={expand}>
            <Table.Th className={isCurrentUser ? "is-selected" : null}>
                {rank ? `#${rank}` : 'n/a'}
            </Table.Th>
            <Table.Td className={"Leaderboards-Content"}>
                <Media>
                    <Media.Left>
                        <Image className="img-circle" is='32x32' src={user.avatar} />
                    </Media.Left>
                    <Media.Content>
                        <Content>
                            <p>
                                <strong><FullName user={user} prependUsername={true} /></strong>
                            </p>
                        </Content>
                    </Media.Content>
                </Media>
            </Table.Td>

            <Table.Td className={"Leaderboards-Content"} style={{maxWidth: 80}}>
                <UserActivityGraph user={user} />
            </Table.Td>
            <Table.Td className={"Leaderboards-Content"}>
                <Streak days={user.streak} />
            </Table.Td>
            <Table.Td className={"Leaderboards-Content"}>
                <Tda tda={user.week_tda} />
            </Table.Td>
        </Table.Tr>
    )
)

class Leaderboards extends React.Component {
    getUserPosition = (id) => {
        const userIndex = this.props.topUsers.findIndex(
            user => user.id === id
        );

        if (userIndex === -1) {
            return null
        } else {
            return userIndex + 1
        }
    }

    renderUsers = () => {
        const users = this.props.topUsers.map(
            (user, pos) => (
                <LeaderboardsRow user={user} rank={pos + 1} isCurrentUser={this.props.me && user.id === this.props.me.id} />
            )
        );

        if (this.props.isLoggedIn && this.props.me && this.getUserPosition(this.props.me.id) === null) {
            users.push(
                <LeaderboardsRow user={this.props.me} rank={null} isCurrentUser={true} />
            )
        }

        return users
    }

    render() {
        return (
            <div className="Leaderboards">
                <Table className={"is-fullwidth is-hoverable"} style={{ marginBottom: 0 }}>
                    <Table.Head>
                        <Table.Tr>
                            <Table.Th className="is-narrow">Rank</Table.Th>
                            <Table.Th>Name</Table.Th>
                            <Table.Th><Emoji emoji="📊" /> Activity</Table.Th>
                            <Table.Th><Emoji emoji="🔥" /> Streak</Table.Th>
                            <Table.Th><Emoji emoji="🏁" /> Tasks/day</Table.Th>
                        </Table.Tr>
                    </Table.Head>
                    <Table.Body>
                        {this.renderUsers()}
                    </Table.Body>
                </Table>
            </div>
        )
    }
}

Leaderboards.propTypes = {
    topUsers: PropTypes.array.isRequired,
}


const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.auth.isLoggedIn,
        isLoading: state.stats.isLoading || state.user.isLoading,
        failed: state.stats.failed || state.user.failed,
        me: state.user.me,
        tda: state.stats.user.tda,
        streak: state.stats.user.streak,
        trend: state.stats.user.activity_trend,
    }
}

export default connect(
    mapStateToProps
)(Leaderboards);