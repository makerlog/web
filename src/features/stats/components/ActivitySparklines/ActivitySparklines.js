import React from 'react';
import PropTypes from 'prop-types';
import Trend from "react-trend";
import {SubTitle} from "vendor/bulma";

const ActivitySparklines = (props) => {
    if (!props.trend || props.trend.length <= 1) {
        return <center><SubTitle is="5">No tasks yet.</SubTitle></center>
    }

    return (
        <Trend
            smooth
            autoDraw
            autoDrawDuration={500}
            autoDrawEasing="ease-out"
            data={props.trend}
            gradient={["#67B26F", "#47E0A0", "#38ef7d"]}
            radius={5}
            strokeWidth={props.strokeWidth ? props.strokeWidth : 2.5}
            strokeLinecap={'round'}
            padding={props.padding ? props.padding : 5}
        />
    )
}

ActivitySparklines.propTypes = {
    trend: PropTypes.array,
}

export default ActivitySparklines;