import React from 'react';
import {Card} from 'vendor/bulma';
import {ActivityCard, ActivityCardPanel} from "./ActivityCard";
import {connect} from "react-redux";
import Spinner from "components/Spinner";

const UserActivityCard = (props) => {
    if (props.isLoading) {
        return (
            <ActivityCardPanel>
                <Card.Content>
                    <Spinner text={"Loading your activity..."} small={true} />
                </Card.Content>
            </ActivityCardPanel>
        );
    } else if (props.failed || !props.me) {
        return (
            <ActivityCardPanel>
                <Card.Content>
                    <strong>Failed to load your user.</strong>
                </Card.Content>
            </ActivityCardPanel>
        )
    }

    return <ActivityCard user={props.me} trend={props.trend ? props.trend : [0]} />
}

UserActivityCard.propTypes = {}

const mapStateToProps = (state) => {
    return {
        isLoading: state.stats.isLoading || state.user.isLoading,
        failed: state.stats.failed || state.user.failed,
        me: state.user.me,
        tda: state.stats.user.tda,
        streak: state.stats.user.streak,
        trend: state.stats.user.activity_trend,
    }
}

export default connect(
    mapStateToProps
)(UserActivityCard);