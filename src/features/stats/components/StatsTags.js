import React from 'react';
import Sadness from '../../../components/Sadness/index';
import Tda from '../../../components/Tda';
import Streak from '../../../components/Streak';
import {Tag} from "vendor/bulma";

const StatsTags = (props) => (
    <div>
        {!props.user.streak || !props.user.week_tda ? <Sadness /> : null} <Tag><Streak days={props.user.streak} /></Tag>  <Tag><Tda tda={props.user.week_tda} /></Tag>
    </div>
)

export default StatsTags;