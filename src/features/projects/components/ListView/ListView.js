import React from 'react';
import {groupTasksByDone, orderByDate} from "../../../../lib/utils/tasks";
import {Button, Card, Heading, Icon, Level, SubTitle, Title} from "vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {connect} from 'react-redux';
import mapDispatchToProps from '../../containers/mapDispatchToProps';
import InProgressCard from "../InProgressCard";
import {colorFromProject, joinProjectsWithTasks} from "../../utils";
import KanbanTask from "../KanbanView/KanbanTask";
import {CSSTransitionGroup} from 'react-transition-group';
import Emoji from "../../../../components/Emoji";
import Switch from 'react-switch';
import {actions as editorActions} from "../../../../ducks/editor";
import {actions as tasksActions} from "../../../../ducks/tasks";
import {actions as projectsActions} from "../../../../ducks/projects";
import {deleteProject} from "../../../../lib/projects";
import styled from 'styled-components';

let ProjectTask = ({ task, markDone, markInProgress, markRemaining }) => (
    <div style={{margin: 10 }}>
        <KanbanTask task={task} extraButtons={
            <React.Fragment>
                {!task.done &&
                    // eslint-disable-next-line
                    <a className={"mark-done"} onClick={() => markDone(task.id)}>
                        <Icon><FontAwesomeIcon icon={'check'}/></Icon> Mark done
                    </a>
                }
                {!task.done && !task.in_progress &&
                    // eslint-disable-next-line
                    <a className={"mark-in-progress"} onClick={() => markInProgress(task.id)}>
                        <Icon><FontAwesomeIcon icon={'dot-circle'}/></Icon> Mark in progress
                    </a>
                }
                {(task.done || task.in_progress) &&
                    // eslint-disable-next-line
                    <a className={"mark-remaining"} onClick={() => markRemaining(task.id)}>
                        <Icon><FontAwesomeIcon icon={'dot-circle'}/></Icon> Mark remaining
                    </a>
                }
            </React.Fragment>
        } />
    </div>
)

ProjectTask = connect(
    null,
    mapDispatchToProps
)(ProjectTask);

class ProjectTaskList extends React.Component {
    state = {
        confirmDelete: false,
        deleting: false,
        failed: false,
    }

    deleteTag = async () => {
        if (!this.state.confirmDelete) {
            this.setState({
                confirmDelete: true,
            })
            return true;
        }

        await this.setState({ deleting: true });

        try {
             await deleteProject(this.props.project.id);
             this.setState({
                 deleting: false,
                 failed: false,
             })
            if (this.props.onDeleteList) {
                this.props.onDeleteList(this.props.project)
            }
        } catch (e) {
            this.setState({
                deleting: false,
                failed: true,
            })
        }
    }

    render() {
        let project = this.props.project;

        if (!this.props.project) {
            project = {
                name: 'Miscellaneous',
                id: 0,
                user: 0,
            }
        }

        let tasks = groupTasksByDone(this.props.tasks);

        return (
            <div className={"ProjectTaskList"}>
                <Card className={"ProjectTaskList-Header"}>
                    <Card.Content>
                        <Level>
                            <Level.Left>
                                <Level.Item>
                                    <div style={{ borderColor: colorFromProject(project) }} className={"color-circle"}></div>
                                </Level.Item>
                                <Level.Item>
                                    <Title is={"5"}>{this.props.project ? `#${project.name}` : project.name}</Title>
                                </Level.Item>
                            </Level.Left>
                            <Level.Right>
                                <Button
                                    disabled={project.id === 0}
                                    onClick={this.deleteTag}
                                    danger={this.state.confirmDelete}
                                    text={!this.state.confirmDelete}
                                    loading={this.state.deleting}>
                                    <Icon><FontAwesomeIcon icon={'trash'} /></Icon> {this.state.confirmDelete && `Are you sure? You will lose ${this.props.tasks.length} tasks.`}
                                </Button>
                            </Level.Right>
                        </Level>
                    </Card.Content>
                </Card>

                <CSSTransitionGroup
                    transitionName="fade"
                    transitionEnterTimeout={200}
                    transitionLeaveTimeout={200}>
                    {this.props.tasks.length === 0 &&
                        <Card style={{margin: 10, }}>
                            <Card.Content className={"has-text-centered"}>
                                <SubTitle is={"5"}>No tasks yet. <Emoji emoji={"🌻"} /></SubTitle>
                            </Card.Content>
                        </Card>
                    }
                    {tasks.in_progress.length > 0 &&
                        <div>
                            {tasks.in_progress.map(task => (
                                <ProjectTask task={task} />
                            ))}
                            <hr />
                        </div>
                    }
                    {tasks.remaining.map(task => (
                        <ProjectTask task={task} />
                    ))}
                    {tasks.done.map(task => (
                        <ProjectTask task={task} />
                    ))}
                </CSSTransitionGroup>
            </div>
        )
    }
}

const ProjectLink = styled.button`
  border-radius: 2px;
  color: #363636;
  cursor: pointer;
  display: block;
  padding: .5em .75em;
  text-decoration: none;
  width: 100%;
  border: none;
  font-size: 15px;
  text-align: left;
  outline: none;
`


class ListView extends React.Component {

    state = {
        selectedProject: null,
        showDone: false,
    }

    componentDidMount() {
        if (this.props.projects.length > 0) {
            this.setState({
                selectedProject: this.props.projects[0].id
            })
        }
    }

    renderInProgress = (in_progress) => {
        if (!in_progress) return null;

        return (
            <InProgressCard large tasks={in_progress} />
        )
    }

    toggleDone = () => {
        this.setState({
            showDone: !this.state.showDone,
        })
    }

    selectProject = (id) => {
        this.setState({
            selectedProject: id
        })
    }

    onDeleteList = (project) => {
        // THIS MAY CAUSE TROUBLE WHEN REUSING THIS COMPONENT
        // get the tasks we deleted
        const batch = this.props.tasks.filter(task => task.project_set.map(p => p.id).indexOf(project.id) >= 0).map(task => task.id)
        this.props.purgeBatch(batch)
        this.props.purgeProject(project.id)
    }

    render() {
        let filteredTasks = orderByDate(this.props.tasks);
        if (!this.state.showDone) {
            filteredTasks = filteredTasks.filter(task => !task.done)
        }
        const tasks = groupTasksByDone(filteredTasks);

        // Hide certain projects
        let projects = joinProjectsWithTasks(this.props.projects, filteredTasks);
        let miscTasks = filteredTasks.filter(task => task.project_set.length === 0);


        let selectedProject = null;
        if (this.state.selectedProject) {
            selectedProject = projects.find(project => project.id === this.state.selectedProject)
        }

        return (
            <div className="ListView">
                <div style={{marginTop: 20}}>
                    {this.renderInProgress(tasks.in_progress)}
                </div>

                <div className={"columns"} style={{marginTop: 20}}>
                    <div className={"column is-one-quarter"}>
                        <Card>
                            <Card.Content>
                                <center>
                                    <Button medium primary onClick={this.props.toggleEditor}>
                                        <Icon><FontAwesomeIcon icon={'plus'} /></Icon>
                                        Add task
                                    </Button>
                                </center>
                                <hr />
                                <Heading>Show done tasks</Heading>
                                <Switch
                                    onChange={this.toggleDone}
                                    onClick={this.toggleDone}
                                    checked={this.state.showDone}
                                    checkedIcon={
                                        <div
                                            style={{
                                                display: "flex",
                                                justifyContent: "center",
                                                alignItems: "center",
                                                height: "100%",
                                                fontSize: 15,
                                                paddingRight: 2
                                            }}
                                        >
                                            <Emoji emoji="✅" />
                                        </div>}
                                    uncheckedIcon={
                                        <div
                                            style={{
                                                display: "flex",
                                                justifyContent: "center",
                                                alignItems: "center",
                                                height: "100%",
                                                fontSize: 15,
                                                paddingRight: 2
                                            }}
                                        >
                                            <Emoji emoji="🕒" />
                                        </div>}
                                    onColor="#47E0A0"
                                    height={30}
                                    width={60}
                                    handleDiameter={20}
                                />
                                <hr />

                                <aside className="menu">
                                    <Heading>
                                        Tags
                                    </Heading>
                                    <ul className="menu-list">
                                        {projects.map(
                                            project => (
                                                <li>
                                                    <ProjectLink 
                                                        onClick={(e) => { e.preventDefault(); this.selectProject(project.id)}}>
                                                        #{project.name} {groupTasksByDone(project.tasks).remaining.length > 0 && <span className={"has-text-warning"}>{groupTasksByDone(project.tasks).remaining.length}</span>}
                                                    </ProjectLink>
                                                </li>
                                            )
                                        )}
                                        <li>
                                            {
                                                // eslint-disable-next-line
                                            }
                                            <ProjectLink
                                                onClick={() => this.selectProject(null)}>
                                                Untagged
                                            </ProjectLink>
                                        </li>
                                    </ul>
                                </aside>
                            </Card.Content>
                        </Card>
                    </div>
                    <div className={"column"}>
                        {!selectedProject &&
                            <ProjectTaskList tasks={miscTasks} />
                        }
                        {selectedProject &&
                            <ProjectTaskList project={selectedProject} tasks={selectedProject.tasks} onDeleteList={this.onDeleteList} />
                        }
                    </div>
                </div>
            </div>
        )
    }
}

ListView.propTypes = {}

export default connect(
    null,
    (dispatch) => ({
        toggleEditor: () => dispatch(editorActions.toggleEditor()),
        purgeBatch: (val) => dispatch(tasksActions.purgeBatch(val)),
        purgeProject: (val) => dispatch(projectsActions.purgeProject(val)),
    })
)(ListView);