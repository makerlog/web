import React from "react";
import {SubTitle} from "vendor/bulma";
import styled from 'styled-components';
import {Icon} from "../../../vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import Emoji from "../../../components/Emoji";
import OutboundLink from "../../../components/OutboundLink";

const CelebratoryThing = styled.div`
 
  .message-container { 
    padding: 20px;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
  }
  
  .message-container > .subtitle {
    margin-bottom: 0px;
  }
  
  .message-container > div {
    margin-top: 15px;
  }
`;

const renderTweetButton = (user) => {
    const text = `I completed all my tasks on @getmakerlog! 💪 \n #TogetherWeMake`;
    const url = `${process.env.REACT_APP_BASE_URL}/@${user.username}`;

    return (
        <OutboundLink href={`https://twitter.com/share?text=${encodeURIComponent(text)}&url=${url}`}
                      className="button is-info is-small is-rounded" style={{backgroundColor: "#1b95e0"}} target="_blank">
            <Icon><FontAwesomeIcon icon={['fab', 'twitter']} /></Icon> Tweet your victory
        </OutboundLink>
    )
}

export default ({ me }) => (
    <CelebratoryThing>
        <div className={"message-container"}>
            <SubTitle><Emoji emoji={"🎉"} /> All done! </SubTitle>
            <div>{renderTweetButton(me)}</div>
        </div>
    </CelebratoryThing>
)