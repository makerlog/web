import React from "react";
import {Link} from "react-router-dom";
import Markdown from "../../../components/Markdown";
import Emoji from "../../../components/Emoji";
import {Title, Heading, Media, Image, Icon, Button, Field, Control, Textarea, Input} from 'vendor/bulma';

import './MilestoneMedia.scss';
import {truncate} from "../../../lib/utils/random";
import {Praisable} from "../../stream/components/Task/components/Praise";
import withCurrentUser from "../../users/containers/withCurrentUser";
import FontAwesomeIcon from "@fortawesome/react-fontawesome/src/components/FontAwesomeIcon";
import {deleteProject} from "../../../lib/projects";
import {deleteMilestone, editMilestone} from "../../../lib/milestones";


class MilestoneMediaComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            confirmDelete: false,
            loadingDelete: false,
            loadingEdit: false,
            deleting: false,
            title: this.props.milestone ? this.props.milestone.title : '',
            body: this.props.milestone ? this.props.milestone.body : '',
            deleted: false,
            editing: false,
            failedEditing: false,
            failedDeleting: false,
        }
    }

    onPraise = e => {
        e.preventDefault()
    }

    edit = async () => {
        this.setState({
            loadingEdit: true
        })
        try {
            await editMilestone(this.props.milestone.slug, {
                title: this.state.title,
                body: this.state.body,
            });
            this.setState({
                loadingEdit: false,
                editing: false,
                failed: false,
            })
        } catch (e) {
            this.setState({
                loadingEdit: false,
                failed: true,
            })
        }
    }

    delete = async () => {
        if (!this.state.confirmDelete) {
            this.setState({
                confirmDelete: true,
            })
            return true;
        }

        this.setState({
            deleting: true
        })

        try {
            await deleteMilestone(this.props.milestone.slug);
            this.setState({
                confirmDelete: false,
                deleting: false,
                failed: false,
                deleted: true,
                loadingDelete: false,
            })
        } catch (e) {
            this.setState({
                confirmDelete: false,
                deleting: false,
                failed: true,
            })
        }
    }

    render() {
        const {
            milestone,
            large=true,
            withIcon=true,
            xs=false,
        } = this.props

        return (
            <Media className={"MilestoneMedia" + (xs ? ' xs' : '')}>
                {milestone.icon && withIcon &&
                    <Media.Left>
                        <Image className={"img-rounded"} is={'48x48'} src={milestone.icon} />
                    </Media.Left>
                }
                <Media.Content>
                    <Heading>Milestone</Heading>
                    {!xs && !this.state.editing && <Title is={large ? "4" : "5"}>{this.state.title}</Title>}
                    {xs && !this.state.editing && <Title is={"6"}>{this.state.title}</Title>}
                    {!xs && !this.props.stream && !this.state.deleted && !this.state.editing && this.state.body && (
                        <p className={"content"}>
                            <Markdown body={this.state.body.split('\n', 1)[0]} />
                        </p>
                    )}
                    {!xs && this.props.stream && !this.state.deleted && !this.state.editing && this.state.body && (
                        <p className={"content"}>
                            <Markdown body={this.state.body.split('\n', 1)[0] + '...'} />
                        </p>
                    )}
                    {this.state.editing &&
                        <>
                            <Field>
                                <Control>
                                    <Input onChange={e => this.setState({ title: e.target.value })} value={this.state.title} />
                                </Control>
                            </Field>
                            <Field>
                                <Control>
                                    <Textarea onChange={e => this.setState({ body: e.target.value })} value={this.state.body} />
                                </Control>
                            </Field>
                            <Button loading={this.state.loadingEdit} onClick={this.edit} primary>Submit</Button>
                        </>
                    }
                    {this.state.deleted && <em>This milestone was deleted.</em>}
                    {xs && (
                        <p className={"content"}>
                            {truncate(milestone.body, 25, '...')}
                        </p>
                    )}
                    {!xs && (
                        <div className={"buttons"} onClick={this.onPraise}>
                            <Praisable
                                button
                                indexUrl={`/milestones/${milestone.slug}`}
                                initialAmount={milestone.praise}
                                item={milestone}>
                            </Praisable>

                            <Link to={`/milestones/${milestone.slug}`} className={"button is-small is-rounded"}>
                                <Emoji emoji={"💬"} />&nbsp; {milestone.comment_count}
                            </Link>
                            {this.props.me.id === milestone.user.id && !(this.props.xs || this.props.stream) && (
                                <Button onClick={e => this.setState({ editing: !this.state.editing })} small className={"is-rounded" + ((this.props.xs || this.props.stream) ? ' hidden-button' : '')}>
                                    <FontAwesomeIcon icon={'edit'} />
                                </Button>
                            )}
                            {this.props.me.id === milestone.user.id && (
                                <Button loading={this.state.deleting} onClick={this.delete} small danger={this.state.confirmDelete} className={"is-rounded" + ((this.props.xs || this.props.stream) ? ' hidden-button' : '')}>
                                    <FontAwesomeIcon icon={'trash'} /> {this.state.confirmDelete ? 'Are you sure?' : ''}
                                </Button>
                            )}
                        </div>
                    )}
                </Media.Content>
            </Media>
        )
    }
}

MilestoneMediaComponent = withCurrentUser(MilestoneMediaComponent)

export default ({ linked=true, ...props }) => {
    if (linked) {
        return <Link className={"LinkWrapped"} to={`/milestones/${props.milestone.slug}`}><MilestoneMediaComponent {...props} /></Link>
    } else {
        return <MilestoneMediaComponent {...props} />
    }
}