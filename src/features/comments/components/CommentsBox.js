import React from 'react';
import PropTypes from 'prop-types';
import {Box, Button} from "vendor/bulma";
import {getComments} from "lib/comments";
import {withCurrentUser} from "features/users";
import CommentList from "./CommentList";
import CommentInput from "./CommentInput";
import {StreamCard as Card} from "../../stream/components/Stream/components/StreamCard/styled";
import Emoji from "../../../components/Emoji";
import {Link} from "react-router-dom";


class CommentsBox extends React.Component {
    state = {
        loading: false,
        comments: null,
        failed: false,
    }

    messagesEnd = React.createRef()

    componentDidMount() {
        this.getComments();
    }

    getComments = async () => {
        this.setState({loading: true,})
        try {
            const comments = await getComments(this.props.indexUrl);
            let failed = false;
            let loading = false;
            this.setState({ comments, failed, loading });
            if (this.messagesEnd && comments.length) {
                this.messagesEnd.current.scrollIntoView()
            }
        } catch (e) {
            this.setState({ loading: false, failed: true })
        }
    }

    onCreate = (c) => {
        this.setState({ comments: [...this.state.comments, c] })
        if (this.messagesEnd) {
            this.messagesEnd.current.scrollIntoView({ behavior: 'smooth' })
        }
    }

    render() {
        return (
            <div>
                {this.state.failed &&
                    <Box>
                    Couldn't load comments. <Button onClick={this.getComments}>Retry</Button>
                    </Box>
                }
                <Card>
                    <Card.Content style={{padding: 20}}>
                        {this.state.comments && !this.props.isLoggedIn && this.state.comments.length === 0 &&
                            <center style={{margin: 30}}><strong>
                                No comments yet. <Link to={'/begin'}>Sign in or join</Link> to post a comment!
                            </strong></center>
                        }
                        {this.state.comments && this.props.isLoggedIn && this.state.comments.length === 0 &&
                            <center style={{margin: 30}}><strong>
                                No comments yet. Start the conversation!
                            </strong> <Emoji emoji={"👇"} /></center>
                        }
                        {this.state.comments && this.state.comments.length > 0 &&
                            <div style={{maxHeight: '50vh', overflow: 'auto'}}>
                                <CommentList indexUrl={this.props.indexUrl} comments={this.state.comments} />


                                <div ref={this.messagesEnd}></div>
                            </div>
                        }
                    </Card.Content>
                    <Card.Footer>
                        <CommentInput indexUrl={this.props.indexUrl} onCreate={this.onCreate} isLoading={this.state.loading} />
                    </Card.Footer>
                </Card>
            </div>
        )
    }
}

CommentsBox.propTypes = {
    task: PropTypes.object,
}

export default withCurrentUser(CommentsBox);