import React from "react";
import styled from "styled-components";
import { Avatar } from "features/users";

const FaceStack = styled.div`
    list-style: none;
    line-height: 0px;

    span {
        display: inline-block;
    }

    span:not(:first-child) {
        margin-left: -11px;
    }

    span img {
        width: 30px;
        height: 30px;
        border-radius: 50%;
        border: 2px solid #ffffff;
    }
`;

export default ({ users, is = 24, limit = 5 }) => (
    <FaceStack className={"FaceStack"}>
        {users.slice(0, limit).map(u => (
            <Avatar is={is ? is : 24} user={u} withAura={false} />
        ))}
    </FaceStack>
);
