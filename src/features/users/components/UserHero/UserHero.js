import React from 'react';
import PropTypes from 'prop-types';
import {Heading, Level, Media, SubTitle, Title} from "vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import FullName from "../FullName";
import Tda from "../../../../components/Tda";
import Streak from "../../../../components/Streak";
import Emoji from "../../../../components/Emoji";
import SocialMediaLevel from 'components/SocialMediaLevel';
import {getUserStats} from 'lib/stats';

class UserHero extends React.Component {
    state = {
        stats: null,
        failed: false,
    }

    componentDidMount() {
        this.loadStats()
    }

    loadStats = async () => {
        try {
            const stats = await getUserStats(this.props.user.id);
            this.setState({
                failed: false,
                stats: stats
            })
        } catch (e) {
            this.setState({
                failed: true,
            })
        }
    }

    getHeaderCss = (heroBody=false) => {
        const header = this.props.user.header;
        if (header) {
            if (heroBody) return {backgroundColor: 'rgba(0, 0, 0, 0.3)',}

            return {
                backgroundImage: `url(${header})`,
                backgroundSize: 'cover',
                backgroundPosition: 'center'
            }
        } else {
            return {}
        }
    }

    render() {
        const props = this.props;

        return (
            <section className="UserHero hero is-primary" style={this.getHeaderCss()}>
                <div className="hero-body" style={this.getHeaderCss(true)}>
                    <div className="container">
                        <div className="columns">
                            <div className="column UserHero-Media">
                                <Media>
                                    <Media.Left className="UserHero-Avatar is-hidden-mobile">
                                        <img src={props.user.avatar} alt={props.user.username} style={{maxWidth: 150, maxHeight: 150, height: 'auto', width: '100%'}} />
                                    </Media.Left>
                                    <Media.Content>
                                        {props.featured && <strong><Emoji emoji={"✨"} /> Featured user</strong>} {props.user.verified && <strong><FontAwesomeIcon icon={'check'}/> Verified</strong>}
                                        <Title is="2">
                                            <FullName user={props.user} />
                                        </Title>
                                        <SubTitle>
                                            {props.user.description ? props.user.description : "I have no bio yet."}
                                        </SubTitle>
                                        <SocialMediaLevel
                                            user={props.user}
                                            instagramUser={props.user.instagram}
                                            productHuntUser={props.user.product_hunt}
                                            telegramUser={props.user.telegram}
                                            twitterUser={props.user.twitter} />
                                    </Media.Content>
                                </Media>
                            </div>
                            <div className="column UserHero-Stats is-hidden-mobile">
                                <Level mobile>
                                    <Level.Item hasTextCentered>
                                        <Heading>Streak</Heading>
                                        <Title><Streak days={props.user.streak} /></Title>
                                    </Level.Item>
                                    {this.state.stats && this.state.stats.praise_received ?
                                        <Level.Item hasTextCentered>
                                            <Heading>Praise</Heading>
                                            <Title>{this.state.stats.praise_received}</Title>
                                        </Level.Item>
                                        : null
                                    }
                                    <Level.Item hasTextCentered>
                                        <Heading>Tasks/day</Heading>
                                        <Title><Tda tda={props.user.week_tda} /></Title>
                                    </Level.Item>
                                    {this.state.stats && this.state.stats.done_today !== null ?
                                        <Level.Item hasTextCentered>
                                            <Heading>Done today</Heading>
                                            <Title>{this.state.stats.done_today}</Title>
                                        </Level.Item>
                                        : null
                                    }
                                </Level>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}


UserHero.propTypes = {
    user: PropTypes.object.isRequired,
}

export default UserHero;