import React from "react";
import {File} from "vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";

class ProductIconPicker extends React.Component {
    onIconUpload = (event) => {
        const file = event.target.files[0];
        this.props.onIconUpload(file, URL.createObjectURL(file))
    }

    render() {
        return (
            <File name>
                <File.Label>
                    <File.Input accept="image/gif,image/jpeg,image/png" onChange={this.onIconUpload} />
                    <File.Cta>
                        <File.Icon>
                            <FontAwesomeIcon icon={'upload'} />
                        </File.Icon>
                        <File.Label as='span'>
                            Upload…
                        </File.Label>
                    </File.Cta>
                </File.Label>
            </File>
        )
    }
}

export default ProductIconPicker;