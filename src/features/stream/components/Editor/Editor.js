import React from 'react';
import {connect} from 'react-redux';
import {actions as editorActions} from 'ducks/editor';
import ErrorMessageList from "components/forms/ErrorMessageList";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {Box, Button, Icon, Image, SubTitle, Table, Tag, Title} from "vendor/bulma";
import processString from "react-process-string";
import ProjectLink from "components/ProjectLink";
import Modal from "components/Modal";
import {HotKeys} from "react-hotkeys";
import Dropzone from 'react-dropzone'
import {Card, Control, Field, File, Input, Level, Message, Textarea} from "../../../../vendor/bulma";
import Emoji from "../../../../components/Emoji";
import Spinner from "../../../../components/Spinner";
import {getMyProducts} from "../../../../lib/products";
import {createMilestone} from "../../../../lib/milestones";
import {ThreadTypeSelect} from "../../../../pages/DiscussionsPage/DiscussionsPage";
import {Link, Redirect} from "react-router-dom";
import {createThread} from "../../../../lib/discussions";
import GoldCtaButton from "../../../../components/GoldCtaButton";
import chrono from 'chrono-node';
import { format } from "date-fns";

class ProductSelectorDropdown extends React.Component {
    state = {
        loading: true,
        products: [],
        failed: false,
    }

    componentDidMount() {
        this.getProducts()
    }

    getProducts = async () => {
        this.setState({ loading: true, })
        try {
            const products = await getMyProducts();
            this.setState({
                products,
                failed: false,
                loading: false,
            })
        } catch (e) {
            this.setState({
                failed: true,
                loading: false,
            })
        }
    }


    render() {
        if (this.state.loading) return <Spinner small />
        if (this.state.failed) return <Button text small onClick={this.getProducts}>Retry</Button>

        return (
            <div className="select">
                <select value={this.props.value} onChange={this.props.onChange}>
                    <option>No product</option>
                    {this.state.products.map(
                        p => <option value={p.slug}>{p.name}</option>
                    )}
                </select>
            </div>
        )
    }
}

class DiscussionEditor extends React.Component {
    state = {
        loading: false,
        type: 'TEXT',
        title: '',
        body: '',
        thread: null,
        failed: false,
    }

    onSubmit = async () => {
        this.setState({
            loading: true,
        })
        try {
            this.setState({
                loading: true,
                failed: false,
            })

            const thread = await createThread(
                this.state.type,
                this.state.title,
                this.state.body,
            )

            this.setState({
                loading: false,
                thread,
            })
            if (this.props.onClose) this.props.onClose()
            if (this.props.onBack) this.props.onBack()
        } catch (e) {
            this.setState({
                loading: false,
                failed: true,
            })
        }
    }

    onIconUpload = (event) => {
        const file = event.target.files[0];

        this.setState({
            icon: file,
            iconPreview: URL.createObjectURL(file)
        })
    }

    render() {
        if (this.state.thread) {
            return <Redirect to={`/discussions/${this.state.thread.slug}`} />
        }

        return (
            <Modal
                open={this.props.open}
                onClose={this.props.onClose}
                background={'transparent'}>
                <div className={"DiscussionEditor-container"}>
                    <Level>
                        <Level.Left>
                            <Title is={"5"} className={"has-text-white"}>
                                Post a thread
                            </Title>
                        </Level.Left>
                        <Level.Right>
                            <Button text small className={"has-text-white is-rounded"} onClick={this.props.onBack}>
                                ← Go back
                            </Button>
                        </Level.Right>
                    </Level>
                    <Card>
                        <div className={"thread-selectors"}>
                            <ThreadTypeSelect
                                isSelected={this.state.type === 'TEXT'}
                                onClick={(e) => this.setState({ type: 'TEXT' })}>
                                <Emoji emoji={"📝"} /> Textpost
                            </ThreadTypeSelect>
                            <ThreadTypeSelect
                                isSelected={this.state.type === 'QUESTION'}
                                onClick={(e) => this.setState({ type: 'QUESTION' })}>
                                <Emoji emoji={"🤔"} /> Question
                            </ThreadTypeSelect>
                            <ThreadTypeSelect
                                isSelected={this.state.type === 'LINK'}
                                onClick={(e) => this.setState({ type: 'LINK' })}>
                                <Emoji emoji={"🔗"} /> Link
                            </ThreadTypeSelect>
                        </div>
                        <Card.Content>
                            {this.state.failed &&
                            <Message danger>
                                <Message.Body>
                                    Oops! Something went wrong. Make sure the form is all filled up!
                                </Message.Body>
                            </Message>
                            }
                            <Field>
                                <label className="label">Title</label>
                                <Control>
                                    <Input
                                        value={this.state.title}
                                        onChange={e => this.setState({ title: e.target.value })}
                                        large
                                        placeholder={this.state.type === 'QUESTION' ? 'How do I make $1m MRR?' : "A guide on making cool things"} />
                                </Control>
                            </Field>
                            <Field>
                                <label className="label">Post</label>
                                <Control>
                                    <Textarea
                                        value={this.state.body}
                                        onChange={e => this.setState({ body: e.target.value })}
                                        placeholder="Write away..." />
                                </Control>
                            </Field>
                        </Card.Content>
                    </Card>
                    <Level>
                        <Level.Left>
                            <small className={"help has-text-white"}>
                                <FontAwesomeIcon icon={['fab', 'markdown']} /> Markdown is supported.
                            </small>
                        </Level.Left>
                        <Level.Right>
                            <Button onClick={this.onSubmit} loading={this.state.loading} primary className={"is-rounded has-text-weight-bold"}>
                                <Icon><FontAwesomeIcon icon={'check'} /></Icon> <span>Post thread</span>
                            </Button>
                        </Level.Right>
                    </Level>
                </div>
            </Modal>
        )
    }
}

class MilestoneEditor extends React.Component {
    state = {
        loading: false,
        title: '',
        body: '',
        product: null,
        failed: false,
        icon: null,
        iconPreview: null,
    }

    onSubmit = async () => {
        this.setState({
            loading: true,
        })
        try {
            await createMilestone({
                title: this.state.title,
                body: this.state.body,
                product: this.state.product,
                icon: this.state.icon,
            })
            this.setState({
                title: '',
                body: '',
                product: null,
                icon: null,
                iconPreview: null,
            })
            if (this.props.onClose) this.props.onClose()
            if (this.props.onBack) this.props.onBack()
        } catch (e) {
            this.setState({
                failed: true,
                loading: false,
            })
        }
    }

    onIconUpload = (event) => {
        const file = event.target.files[0];

        this.setState({
            icon: file,
            iconPreview: URL.createObjectURL(file)
        })
    }

    render() {
        /*if (!this.props.hasGold) {
            return (

                <Modal
                    open={this.props.open}
                    onClose={this.props.onClose}
                    background={'transparent'}>
                    <div className={"MilestoneEditor-container"}>
                        <Level>
                            <Level.Left>
                                <Title is={"5"} className={"has-text-white"}>
                                    Add a milestone
                                </Title>
                            </Level.Left>
                            <Level.Right>
                                <Button text small className={"has-text-white is-rounded"} onClick={this.props.onBack}>
                                    ← Go back
                                </Button>
                            </Level.Right>
                        </Level>
                        <Card>
                            <Card.Content>
                                <div className={"content"}>
                                    <h2>Milestones is a Gold-only feature.</h2>
                                </div>
                            </Card.Content>
                        </Card>
                    </div>
                </Modal>
            )
        }*/

        return (
            <Modal
                open={this.props.open}
                onClose={this.props.onClose}
                background={'transparent'}>
                <div className={"MilestoneEditor-container"}>
                    <Level>
                        <Level.Left>
                            <Title is={"5"} className={"has-text-white"}>
                                Add a milestone
                            </Title>
                        </Level.Left>
                        <Level.Right>
                            <Button text small className={"has-text-white is-rounded"} onClick={this.props.onBack}>
                                ← Go back
                            </Button>
                        </Level.Right>
                    </Level>
                    <Card>
                        <Card.Content>
                            {!this.props.hasGold && (
                                <Message className={"gold"}>
                                    <Message.Body>
                                        <Level>
                                            <Level.Left>
                                                <div className={"gold-text"}>
                                                    <Title is={"5"}>Milestones is a Gold feature.</Title>
                                                    <SubTitle is={"6"}>Get Gold and support the maker movement for just $5/mo.</SubTitle>
                                                </div>
                                            </Level.Left>
                                            <Level.Right>
                                                <GoldCtaButton />
                                            </Level.Right>
                                        </Level>
                                    </Message.Body>
                                </Message>
                            )}
                            <div className={"milestone-form" + (!this.props.hasGold ? ' disabled' : '')}>
                                {this.state.failed &&
                                    <Message danger>
                                        <Message.Body>
                                            Oops! Something went wrong. Make sure the form is all filled up!
                                        </Message.Body>
                                    </Message>
                                }
                                <Field>
                                    <label className="label">Title</label>
                                    <Control>
                                        <Input
                                            value={this.state.title}
                                            onChange={e => this.setState({ title: e.target.value })}
                                            large
                                            placeholder="Made an amazing product" />
                                    </Control>
                                </Field>
                                <Field>
                                    <label className="label">Post</label>
                                    <Control>
                                        <Textarea
                                            value={this.state.body}
                                            onChange={e => this.setState({ body: e.target.value })}
                                            placeholder="Tell your followers what happened..." />
                                    </Control>
                                </Field>
                                <div className={"columns"}>
                                    <div className={"column"} style={{padding: 0, margin: 0}}>
                                        <Field>
                                            <label className="label">Tag a product</label>
                                            <Control>
                                                <ProductSelectorDropdown value={this.state.product} onChange={e => this.setState({ product: e.target.value })} />
                                            </Control>
                                        </Field>
                                    </div>
                                    <div className={"column"} style={{padding: 0, margin: 0}}>
                                        <Field>
                                            <label className="label">Icon</label>
                                            <Control>
                                                <File name>
                                                    <File.Label>
                                                        <File.Input accept="image/gif,image/jpeg,image/png" onChange={this.onIconUpload} />
                                                        <File.Cta>
                                                            {!this.state.iconPreview &&
                                                            <>
                                                                <File.Icon>
                                                                    <FontAwesomeIcon icon={'upload'} />
                                                                </File.Icon>
                                                                <File.Label as='span'>
                                                                    Upload icon…
                                                                </File.Label>
                                                            </>
                                                            }
                                                            {this.state.iconPreview &&
                                                            <Image is={'48x48'} src={this.state.iconPreview} />
                                                            }
                                                        </File.Cta>
                                                    </File.Label>
                                                </File>
                                            </Control>
                                        </Field>
                                    </div>
                                </div>
                            </div>
                        </Card.Content>
                    </Card>
                    {this.props.hasGold && (
                        <Level>
                            <Level.Left>
                                <small className={"help has-text-white"}>
                                    <FontAwesomeIcon icon={['fab', 'markdown']} /> Markdown is supported.
                                </small>
                            </Level.Left>
                            <Level.Right>
                                <Button onClick={this.onSubmit} loading={this.state.loading} primary className={"is-rounded has-text-weight-bold"}>
                                    <Icon><FontAwesomeIcon icon={'check'} /></Icon> <span>Post milestone</span>
                                </Button>
                            </Level.Right>
                        </Level>
                    )}
                </div>
            </Modal>
        )
    }
}

const BasicTask = ({ task, onDelete }) => {
    //let doneIcon = faCheck;
    //let remainingIcon = faDotCircle;

    let icon = (task.done) ?
        <FontAwesomeIcon icon={'check-circle'} color="#27ae60" />
        :
        <FontAwesomeIcon icon={'dot-circle'} color="#f39c12" />;

    const hashtagConfig = {
        regex: /#([a-z0-9_-]+?)( |,|$|\.)/gim,
        fn: (key, result) => {
            let projectName = result[1];

            return (
                <ProjectLink key={result[1]}>#{projectName}</ProjectLink>
            )
        }
    }

    let processed = processString([hashtagConfig])(task.content);

    return (
        <div className={"entry " + (task.done ? "done" : (task.in_progress ? 'in_progress' : "remaining"))}>
            {icon} &nbsp; {processed} <Button className={"button is-text is-small"}  onClick={onDelete}><FontAwesomeIcon icon={'trash'} /></Button> {task.attachment && <FontAwesomeIcon icon={'camera'} />}
        </div>
    )
}

const HelpBox = ({ onClose }) => (
    <Box>
        <Table bordered fullwidth>
            <Table.Body>
                <Table.Tr>
                    <Table.Th>Key</Table.Th>
                    <Table.Th>Action</Table.Th>
                </Table.Tr>
                <Table.Tr>
                    <Table.Td>
                        <Tag>Shift + N</Tag>
                    </Table.Td>
                    <Table.Td>
                        Open Editor.
                    </Table.Td>
                </Table.Tr>
                <Table.Tr>
                    <Table.Td>
                        <Tag>Enter</Tag>
                    </Table.Td>
                    <Table.Td>
                        Appends current task to queue.
                    </Table.Td>
                </Table.Tr>
                <Table.Tr>
                    <Table.Td>
                        <Tag>Ctrl/Cmd + Enter</Tag>
                    </Table.Td>
                    <Table.Td>
                        Posts all tasks in queue.
                    </Table.Td>
                </Table.Tr>
                <Table.Tr>
                    <Table.Td>
                        <Tag>Ctrl/Cmd + D</Tag>
                    </Table.Td>
                    <Table.Td>
                        Toggle done of current task.
                    </Table.Td>
                </Table.Tr>
                <Table.Tr>
                    <Table.Td>
                        <Tag>Ctrl/Cmd + I</Tag>
                    </Table.Td>
                    <Table.Td>
                        Toggle open image uploader.
                    </Table.Td>
                </Table.Tr>
            </Table.Body>
        </Table>
        <Button text onClick={onClose}>Close</Button>
    </Box>
)

class Editor extends React.Component {
    state = {
        showImageUploader: false,
        showDueEditor: false,
        editorNaturalDate: '',
        typeTick: 0,
        tooLarge: false,
    }

    ticks = 0

    handlers = () => ({
        'enter': () => {
            if (this.props.editorValue.length <= 3) {
                return false
            }

            this.props.addToQueue();
        },
        'mod+enter': this.onSubmit,
        'mod+i': this.toggleImageUploader,
        'mod+d': () => {
            this.handleTypeChange()
            return false
        }
    })

    changeTick = () => {
        let newTick = this.state.typeTick + 1
        // three is number of changes possible (todo, inprogress, remaining)
        // reset to done if not
        this.setState({
            typeTick: (newTick <= 3 ? newTick : 0)
        })
    }

    handleTypeChange = async () => {
        // three is number of changes possible (todo, inprogress, remaining)
        // reset to done if not
        let newTick = this.state.typeTick + 1
        // three is number of changes possible (todo, inprogress, remaining)
        // reset to done if not
        await this.setState({
            typeTick: (newTick <= 2 ? newTick : 0)
        })
    }

    renderIcon = () => {
        let doneIcon = 'check';
        let remainingIcon = 'dot-circle';

        return (this.props.editorDone) ?
            <FontAwesomeIcon size='2x' icon={doneIcon} color={'white'} />
            :
            <FontAwesomeIcon size='2x' icon={remainingIcon} color={'white'} />
    }

    onDrop = (acceptedFiles, rejectedFiles) => {
        if (!acceptedFiles.length) {
            this.setState({ tooLarge: true })
            return;
        } else {
            this.setState({ tooLarge: false })
        }
        const file  = acceptedFiles[0];
        this.props.setEditorAttachment(file)
    }

    onClickIcon = () => {
        this.handleTypeChange()
    }

    onSubmit = () => {
        if (this.props.editorValue.length >= 3) {
            this.props.addToQueue()
        }
        this.props.createTasks();
    }

    renderQueue = () => {
        return this.props.queue.map(
            t => <BasicTask task={t} onDelete={() => this.props.removeFromQueue(t)} />
        )
    }

    toggleImageUploader = () => {
        this.setState({ showImageUploader: !this.state.showImageUploader })
    }

    toggleDueEditor = () => {
        this.setState({ showDueEditor: !this.state.showDueEditor })
    }

    canShowExtraTools = () => {
        return !(!this.props.queue.length && this.props.editorValue.length === 0)
    }


    setEditorDueAt = (e) => {
        this.setState({
            editorNaturalDate: e.target.value
        })
        this.props.setEditorDueAt(
            chrono.parseDate(e.target.value)
        )
    }

    render() {
        switch (this.state.typeTick) {
            case 0:
                this.props.markDone()
                break;

            case 1:
                this.props.markRemaining()
                break;

            case 2:
                this.props.markInProgress()
                break;

            default:
                this.props.markDone()
        }

        if (this.props.creatingDiscussion) return (
            <DiscussionEditor
                open={this.props.open}
                onClose={this.props.onClose}
                onBack={this.props.openDiscussionEditor}
            />
        )



        if (this.props.creatingMilestone) return (
            <MilestoneEditor
                hasGold={this.props.hasGold}
                open={this.props.open}
                onClose={this.props.onClose}
                onBack={this.props.openMilestoneEditor}
            />
        )

        return (
            <Modal
                open={this.props.open}
                onClose={this.props.onClose}
                background={'transparent'}>
                <div className={"Editor-container"}>
                    <section className={"Editor"}>
                        <div className={"editor-flex"}>
                            <div onClick={this.onClickIcon}
                                 className={"icon" + (this.props.editorDone ? ' done' : (this.props.editorInProgress ? ' in_progress' : ' todo'))}>
                                {this.renderIcon()}
                            </div>
                            <HotKeys handlers={this.handlers()} className={"input-div"}>
                                <input
                                    id={"editor-input"}
                                    autoFocus
                                    value={this.props.editorValue}
                                    onChange={e => this.props.setEditorValue(e.target.value)}
                                    autoComplete={"off"}
                                    placeholder={(
                                        this.props.editorDone ? "Write something you did today." :
                                            (this.props.editorInProgress ? "Write what you're working on." : "Write a to-do.")
                                    )}/>
                            </HotKeys>
                            <div onClick={this.toggleDueEditor} className={"icon"}>
                                <FontAwesomeIcon icon={!this.props.editorDueAt ? 'calendar-alt' : 'calendar-check'}/>
                            </div>
                            <div onClick={this.toggleImageUploader} className={"icon uploader"}>
                                <FontAwesomeIcon icon={'camera'}/>
                            </div>
                        </div>
                    </section>

                    {
                        this.state.showDueEditor &&
                            <section className={"Editor DueEditor"}>
                                <div className={"editor-flex"}>
                                    <div className={"input-div"}>
                                        <input
                                            id={"editor-input"}
                                            autoFocus
                                            value={this.state.editorNaturalDate}
                                            onChange={this.setEditorDueAt}
                                            autoComplete={"off"}
                                            placeholder={"When is this task due?"}/>
                                    </div>
                                    {this.props.editorDueAt !== null && (
                                        <div onClick={this.toggleDueEditor} className={"icon"}>
                                            <FontAwesomeIcon icon={'check'} />
                                        </div>
                                    )}
                                </div>
                                {this.props.editorDueAt &&
                                    <div className={"date-preview"}>
                                        <small>{format(this.props.editorDueAt, 'MMMM d, yyyy (h:mm aa)')}</small>
                                    </div>
                                }
                            </section>
                    }
                    {
                        this.state.showImageUploader &&
                        <Box>
                            <Dropzone maxSize={2*1024*1024} className={"dropzone"} accept="image/*" multiple={false} onDrop={this.onDrop}>
                                {!this.props.editorAttachment &&
                                    <SubTitle>{
                                        this.state.tooLarge ? 'Attachment is too large!' : 'Drop an image to attach here'
                                    }</SubTitle>
                                }
                                {this.props.editorAttachment &&
                                    <Image src={this.props.editorAttachment.preview} />
                                }
                            </Dropzone>
                        </Box>
                    }

                    {!this.canShowExtraTools() && !(this.state.showImageUploader || this.state.showDueEditor) && (
                        <div className={"card MultiColumnSelector is-flex-mobile columns"}>
                            <div className={"column"} onClick={this.props.openMilestoneEditor}>
                                <div>
                                    <Emoji emoji={"🚩️"} />
                                </div>
                                <div className={"title is-5 is-hidden-mobile"}>
                                    Add a milestone
                                </div>
                            </div>
                            <div className={"column"} onClick={this.props.openDiscussionEditor}>
                                <div>
                                    <Emoji emoji={"✍️"} />
                                </div>
                                <div className={"title is-5 is-hidden-mobile"}>
                                    Post a topic
                                </div>
                            </div>
                            <Link to={'/products'} className={"column"} onClick={this.props.onClose}>
                                <div>
                                    <Emoji emoji={"🚢"} />
                                </div>
                                <div className={"title is-5 is-hidden-mobile"}>
                                    Add a product
                                </div>
                            </Link>
                        </div>
                    )}

                    {this.canShowExtraTools() && !(this.state.showImageUploader || this.state.showDueEditor) && (
                        <Box className={this.props.queue.length === 0 && "is-hidden-mobile"}>
                            {this.props.createFailed &&
                            <ErrorMessageList errorMessages={this.props.errorMessages} fieldErrors={this.props.fieldErrors} />
                            }
                            {this.props.queue.length > 0 ?
                                this.renderQueue()
                                :
                                <small className={"has-text-grey"}>
                                    <strong>Click on the icon to change task type between done, to-do, or in progress.</strong>
                                    <br />
                                    {
                                        // eslint-disable-next-line
                                    } <Tag>Enter</Tag> adds another task. Press <Tag>Cmd/Ctrl + Enter</Tag> to finish.
                                </small>
                            }
                        </Box>
                    )}

                    {
                        this.canShowExtraTools() ?
                            <Level>
                                <Level.Left>
                                </Level.Left>
                                <Level.Right>
                                    <Button loading={this.props.isCreating} onClick={this.onSubmit} className={'button is-medium is-primary is-rounded'}>
                                        <Icon medium><FontAwesomeIcon icon={'check'} /></Icon> <strong>Submit</strong>
                                    </Button>
                                </Level.Right>
                            </Level>
                            :
                            null
                    }
                </div>
            </Modal>
        )
    }
}

const mapStateToProps = (state) => ({
    hasGold: state.user.me ? state.user.me.gold : false,
    open: state.editor.open,
    queue: state.editor.queue,
    creatingMilestone: state.editor.creatingMilestone,
    creatingDiscussion: state.editor.creatingDiscussion,
    editorDueAt: state.editor.editorDueAt,
    editorAttachment: state.editor.editorAttachment,
    isCreating: state.editor.isCreating,
    editorValue: state.editor.editorValue,
    editorDone: state.editor.editorDone,
    editorInProgress: state.editor.editorInProgress,
    createFailed: state.editor.createFailed,
    errorMessages: state.editor.errorMessages,
    fieldErrors: state.editor.fieldErrors,
})

const mapDispatchToProps = (dispatch) => ({
    onClose: () => dispatch(editorActions.toggleEditor()),
    addToQueue: () => dispatch(editorActions.addToQueue()),
    removeFromQueue: (t) => dispatch(editorActions.removeFromQueue(t)),
    createTasks: () => dispatch(editorActions.createTasks()),
    setEditorValue: (v) => dispatch(editorActions.setEditorValue(v)),
    setEditorDueAt: (v) => dispatch(editorActions.setEditorDueAt(v)),
    toggleEditorDone: () => dispatch(editorActions.toggleEditorDone()),
    setEditorAttachment: (a) => dispatch(editorActions.setEditorAttachment(a)),
    markDone: () => dispatch(editorActions.markDone()),
    markInProgress: () => dispatch(editorActions.markInProgress()),
    markRemaining: () => dispatch(editorActions.markRemaining()),
    openMilestoneEditor: () => dispatch(editorActions.openMilestoneEditor()),
    openDiscussionEditor: () => dispatch(editorActions.openDiscussionEditor()),
})


Editor.propTypes = {}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Editor);