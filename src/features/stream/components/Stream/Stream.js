import React from 'react';
import PropTypes from 'prop-types';
import {Button} from 'vendor/bulma';
import {CSSTransitionGroup} from 'react-transition-group';
import InfiniteScroll from 'react-infinite-scroll-component';
import StreamSection from './components/StreamSection/index';
import NoActivityCard from "../NoActivityCard";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {sortStreamByActivity} from 'lib/utils/tasks';
import StreamFinished from './components/StreamFinished/index';
import Spinner from "components/Spinner";
import {assignModelType} from "../../../../lib/utils/tasks";

/**
 * Stream Component
 * @param tasks tasks to be displayed.
 * @param loadMore called on loadmore click or scroll.
 * @param isSyncing boolean indicating loading status.
 * @param hasMore boolean indicating if there is data to be loaded on server.
 */

function daysBetween(one, another) {
    return Math.round(Math.abs((+one) - (+another))/8.64e7);
}

class Stream extends React.Component {
    render() {
        let data = this.props.tasks;

        data = assignModelType({
            tasks: this.props.tasks ? this.props.tasks : [],
            milestones: this.props.milestones ? this.props.milestones : [],
        })

        data = sortStreamByActivity(data);


        if (Object.keys(data).length === 0 && !this.props.hasMore && !this.props.isSyncing) {
            return this.props.noActivityComponent
        }

        let lastDate = null;

        return (
            <InfiniteScroll
                next={this.props.loadMore}
                hasMore={this.props.hasMore}
                style={{ overflow: 'none'}}>
                <CSSTransitionGroup
                    transitionName="fade"
                    transitionEnterTimeout={200}
                    transitionLeaveTimeout={200}>
                    {Object.keys(data).map(
                        (date) => {
                            return (
                                <StreamSection
                                    key={date}
                                    position={Object.keys(data).indexOf(date)}
                                    canSwitchType={this.props.canSwitchType}
                                    isFollowingFeed={this.props.isFollowingFeed}
                                    onSwitch={this.props.onSwitch}
                                    date={date}
                                    activityData={data[date]} />
                            )
                        }
                    )}
                </CSSTransitionGroup>

                {this.props.hasMore &&
                    <center>
                        <Button loading={this.props.isSyncing} className={"is-rounded"} onClick={this.props.loadMore}>
                            <FontAwesomeIcon icon={'arrow-circle-down'} /> &nbsp; Load more tasks...
                        </Button>
                    </center>
                }
                {!this.props.hasMore && this.props.isSyncing && <Spinner />}
                {!this.props.hasMore && !this.props.isSyncing && <StreamFinished />}
            </InfiniteScroll>
        )
    }
}

Stream.propTypes = {
    tasks: PropTypes.array.isRequired,
    milestones: PropTypes.array,
    loadMore: PropTypes.func.isRequired,
    isSyncing: PropTypes.bool.isRequired,
    hasMore: PropTypes.bool.isRequired,
}

Stream.defaultProps = {
    noActivityComponent: <NoActivityCard/>,
    canSwitchType: false,
    isFollowingFeed: false,
    onSwitch: null,
}

export default Stream;