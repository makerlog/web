import React from 'react';
import {Button, Hero, Icon, SubTitle} from 'vendor/bulma';
import {Link} from 'react-router-dom';
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {actions as editorActions} from "../../../../ducks/editor";
import {connect} from 'react-redux';

import {GlobalStream} from "features/stream";
import {Card, Media, Title} from "../../../../vendor/bulma";

import styled from 'styled-components';
import OutboundLink from "../../../../components/OutboundLink";

const mapDispatchToProps = (dispatch) => ({
    toggleEditor: () => dispatch(editorActions.toggleEditor())
})

const CtaButton = connect(
    null,
    mapDispatchToProps
)(
    (props) => (
        <Button onClick={props.toggleEditor} className={"is-rounded is-medium CtaButton"}>
            <Icon>
                <FontAwesomeIcon icon={'check-circle'} />
            </Icon>
            <span>Add your first task</span>
        </Button>
    )
)

const NewAccountTweetCard = styled(Card)`
   background-color: #3498db !important;
    border-color: #3498db !important;
    display: flex;
    align-items: center;
    justify-content: center;
  
    & .card-content {
      width: 100%;
    }
    
    & .title, & .subtitle {
      color: white;
    }
`

const WorkflowCard = styled(Card)`
   background-color: #e6186d !important;
    border-color: #e6186d !important;
    display: flex;
    align-items: center;
    justify-content: center;
  
    & .card-content {
      width: 100%;
    }
    
    & .title, & .subtitle {
      color: white;
    }
`

const FirstTaskCard = styled(Card)`
   background-color: ${props => props.theme.primaryDarker} !important;
    border-color: ${props => props.theme.primaryDarker} !important;
    display: flex;
    align-items: center;
    justify-content: center;
  
    & .card-content {
      width: 100%;
    }
    
    & .title, & .subtitle {
      color: white;
    }
`

const renderTweetButton = (user) => {
    const text = `I just joined @GetMakerlog! 👋 \n #TogetherWeMake`;
    const url = `${process.env.REACT_APP_BASE_URL}/@${user.username}`;

    return (
        <OutboundLink href={`https://twitter.com/share?text=${encodeURIComponent(text)}&url=${url}`}
                                       className="button is-rounded" target="_blank">
            Tweet
        </OutboundLink>
    )
}

const NoActivityCard = (props) => (
    <div className={"NoActivityCard"}>
        <Hero className={"has-text-centered"}>
            <Hero.Body>
                <FirstTaskCard>
                    <Card.Content>
                        <Media>
                            <Media.Left>
                                <FontAwesomeIcon icon={'check-circle'} color={'white'} size={'3x'} />
                            </Media.Left>
                            <Media.Content>
                                <Title is="5">
                                    Post your first task
                                </Title>
                                <SubTitle is="6">
                                    Start your productivity journey by posting your first task.
                                </SubTitle>
                                <CtaButton />
                            </Media.Content>
                        </Media>
                    </Card.Content>
                </FirstTaskCard>
                <hr />
                <div className={"columns"}>
                    <div className={"column"}>
                        <NewAccountTweetCard>
                            <Card.Content>
                                <Media>
                                    <Media.Left>
                                        <FontAwesomeIcon icon={['fab', 'twitter']} color={'white'} size={'2x'} />
                                    </Media.Left>
                                    <Media.Content>
                                        <Title is="5">
                                            Say hi!
                                        </Title>
                                        <SubTitle is="6">
                                            Tweet #TogetherWeMake and say hi to the community!
                                        </SubTitle>
                                        {renderTweetButton(props.user)}
                                    </Media.Content>
                                </Media>
                            </Card.Content>
                        </NewAccountTweetCard>
                    </div>
                    <div className={"column"}>
                        <WorkflowCard>
                            <Card.Content>
                                <Media>
                                    <Media.Left>
                                        <FontAwesomeIcon icon={'plug'} color={'white'} size={'2x'} />
                                    </Media.Left>
                                    <Media.Content>
                                        <Title is="5">
                                            Discover apps
                                        </Title>
                                        <SubTitle is="6">
                                            Makerlog integrates right with your workflow. Find an app that makes you more productive!
                                        </SubTitle>
                                        <Link className={"button is-rounded"} to={'/apps'}>
                                            Discover apps
                                        </Link>
                                    </Media.Content>
                                </Media>
                            </Card.Content>
                        </WorkflowCard>
                    </div>
                </div>
            </Hero.Body>
        </Hero>
        <GlobalStream />
    </div>
);

export default connect(
    state => ({
        user: state.user.me
    }),
    null,
)(NoActivityCard);