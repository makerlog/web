import React from 'react';
import {Card} from "../../vendor/bulma";
import Emoji from "../Emoji";
import {RecentDiscussionList} from "features/discussions";

export default () => (
    <Card>
        <Card.Content>
            <p className="heading"><Emoji emoji="💬 "/> Recent discussions</p>
            <RecentDiscussionList />
        </Card.Content>
    </Card>
)