import React from "react";
import CarbonAd from "../../vendor/CarbonAd";
import withCurrentUser from "../../features/users/containers/withCurrentUser";
import { Card, Heading } from "../../vendor/bulma";
import Emoji from "../Emoji";
import GoldIcon from "../../GoldIcon";
import Sticky from "react-stickynode";

// Disable ads for gold users.

export default withCurrentUser(props =>
    props.user && props.user.gold ? (
        <Card>
            <Card.Content style={{ fontSize: 14 }}>
                <small>
                    No ads here! Thanks for buying Makerlog Gold. <GoldIcon />{" "}
                    <Emoji emoji={"✌️"} />
                </small>
            </Card.Content>
        </Card>
    ) : (
        <CarbonAd />
    )
);
