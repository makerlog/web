import React from 'react';
import {Card} from "../../vendor/bulma";
import Emoji from "../Emoji";
import {Link} from "react-router-dom";

export default () => (
    <Card>
        <Card.Content>
            <p className="heading"><Emoji emoji="👋"/> Welcome, guest</p>
            <small>
                Join Makerlog to post tasks, share your work, and meet fellow makers.
            </small>

            <div style={{marginTop: 10}}>
                <Link className={"button is-primary is-rounded is-small"} to={'/begin'}>
                    Get started &raquo;
                </Link>
            </div>
        </Card.Content>
    </Card>
)