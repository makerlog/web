import React from 'react';
import {Card} from "../../vendor/bulma";
import styled from 'styled-components';

const Announcement = styled(Card)`
    color: white !important;
    background: #67B26F;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to right, #4ca2cd, #67B26F);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to right, #4ca2cd, #67B26F); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    transition: background-color .25s;
`

export default (props) => (
    <Announcement>
        <Card.Content>
            <Title is="5" className={"has-text-white"} style={{marginBottom: 4}}>
                {props.title}
            </Title>
            {props.children}
        </Card.Content>
    </Announcement>

)