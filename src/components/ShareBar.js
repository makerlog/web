import React from 'react';
import {Button, Icon, Level} from "vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import copy from "clipboard-copy";
import Emoji from "./Emoji";


class CopyLink extends React.Component {
    state = {
        copied: false,
    }

    onClick = async () => {
        try {
            await copy(this.props.url);

            this.setState({
                copied: true,
            })

            setTimeout(() => {
                this.setState({
                    copied: false,
                })
            }, 2000)
        } catch (e) {

        }
    }

    render() {
        return (
            <span onClick={this.onClick} className={this.props.className + (this.state.copied && " has-text-grey-light")}>
                {!this.state.copied &&
                <span>{this.props.children}</span>
                }
                {this.state.copied &&
                <span><Emoji emoji={"💫"} /> Copied!</span>
                }
            </span>
        )
    }
}

class ShareBar extends React.Component {
    renderShareButtons = () => (
        <>
            {this.props.permalink &&
                <Level.Item>
                    <CopyLink url={this.props.permalink}>
                        <Button text small>
                            <Icon>
                                <FontAwesomeIcon icon={'link'} size={'sm'} />
                            </Icon> {!this.props.compact && 'Permalink'}
                        </Button>
                    </CopyLink>
                </Level.Item>
            }
            {this.props.tweetText &&
                <Level.Item>
                    <a className="button is-text is-small"
                       target={'_blank'}
                       href={`https://twitter.com/intent/tweet?text=${encodeURIComponent(this.props.tweetText)}`}>
                        <Icon>
                            <FontAwesomeIcon icon={['fab', 'twitter']} size={'sm'} />
                        </Icon> {!this.props.compact && 'Tweet'}
                    </a>
                </Level.Item>
            }
        </>
    )

    render() {
        return (
            <Level className={"ShareBar has-text-grey-light"} mobile>
                <Level.Left>
                    {this.props.extraItemsFirst && this.props.extraItemsFirst()}

                    {!this.props.rightAlignShare && this.renderShareButtons()}

                    {this.props.extraItemsLeft && this.props.extraItemsLeft()}
                </Level.Left>
                <Level.Right>
                    {this.props.rightAlignShare && this.renderShareButtons()}
                    {this.props.extraItemsRight && this.props.extraItemsRight()}
                </Level.Right>
            </Level>
        )
    }
}

ShareBar.propTypes = {}

export default ShareBar;