import React from 'react';
import styled from "styled-components";

export default styled.div`
    color: whitesmoke;
    height: 28px;
    cursor: default;
    border: none;
    display: inline-flex;
    outline: none;
    padding: 0;
    font-size: 0.8125rem;
    transition: background-color 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
    box-sizing: border-box;
    font-family: "Roboto", "Helvetica", "Arial", sans-serif;
    align-items: center;
    white-space: nowrap;
    border-radius: 16px;
    vertical-align: middle;
    justify-content: center;
    text-decoration: none;
    background-color: rgba(0, 0, 0, 0.2);
    
    img {
    border-radius: 50%;
        width: 28px;
    color: #616161;
    height: 28px;
    font-size: 1rem;
            margin-right: -4px;
        }
 div {
  cursor: inherit;
    display: flex;
    align-items: center;
    user-select: none;
    white-space: nowrap;
    padding-left: 5px;
    padding-right: 5px;
    }
`