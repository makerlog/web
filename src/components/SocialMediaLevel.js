import React from 'react';
import {Button, Icon, Level} from "vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {FollowButton} from "features/users";
import {getHostname, normalizeUrl} from "../lib/utils/products";

class SocialMediaLevel extends React.Component {

    render() {
        return (
            <Level mobile className="SocialMediaLevel">
                <Level.Left>
                    {this.props.user &&
                        <Level.Item>
                            <FollowButton userId={this.props.user.id}/>
                        </Level.Item>
                    }
                    {this.props.website &&
                        <Level.Item>
                            <a target="_blank" rel="noopener noreferrer" href={normalizeUrl(this.props.website)}>
                                <Button className={"is-rounded"} info inverted outlined>
                                    <Icon>
                                        <FontAwesomeIcon icon={'globe'} />
                                    </Icon>
                                    <strong>
                                        {getHostname(this.props.website)}
                                    </strong>
                                </Button>
                            </a>
                        </Level.Item>
                    }
                    {this.props.twitterUser &&
                        <Level.Item>
                            <a target="_blank" rel="noopener noreferrer" href={`https://twitter.com/${this.props.twitterUser}`}><FontAwesomeIcon icon={['fab', 'twitter']} /></a>
                        </Level.Item>
                    }
                    {this.props.telegramUser &&
                    <Level.Item>
                        <a target="_blank" rel="noopener noreferrer" href={`https://t.me/${this.props.telegramUser}`}><FontAwesomeIcon icon={['fab', 'telegram']} /></a>
                    </Level.Item>
                    }
                    {this.props.twitterUrl &&
                    <Level.Item>
                        <a target="_blank" rel="noopener noreferrer" href={this.props.twitterUrl}><FontAwesomeIcon icon={['fab', 'twitter']} /></a>
                    </Level.Item>
                    }
                    {this.props.productHuntUser &&
                        <Level.Item>
                            <a target="_blank" rel="noopener noreferrer" href={`https://producthunt.com/@${this.props.productHuntUser}`}><FontAwesomeIcon icon={['fab', 'product-hunt']} /></a>
                        </Level.Item>
                    }
                    {this.props.productHuntUrl &&
                        <Level.Item>
                            <a target="_blank" rel="noopener noreferrer" href={normalizeUrl(this.props.productHuntUrl)}><FontAwesomeIcon icon={['fab', 'product-hunt']} /></a>
                        </Level.Item>
                    }
                    {this.props.instagramUser &&
                        <Level.Item>
                            <a target="_blank" rel="noopener noreferrer" href={`https://instagram.com/${this.props.instagramUser}`}><FontAwesomeIcon icon={['fab', 'instagram']} /></a>
                        </Level.Item>
                    }
                    {this.props.instagramUrl &&
                        <Level.Item>
                            <a target="_blank" rel="noopener noreferrer" href={this.props.instagramUrl}><FontAwesomeIcon icon={['fab', 'instagram']} /></a>
                        </Level.Item>
                    }
                </Level.Left>
            </Level>
        )
    }
}

SocialMediaLevel.propTypes = {
    // They're mostly optional, sooo....
}

export default SocialMediaLevel;