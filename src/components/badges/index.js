import React from "react";
import Tda from "components/Tda";
import Streak from "components/Streak";
import { Level, Tag } from "vendor/bulma";
import Emoji from "components/Emoji";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import styled from "styled-components";

export const Badge = props => (
    <span
        className={`Badge tag is-rounded ${
            props.className ? props.className : null
        }`}
        style={{
            backgroundColor: props.backgroundColor
                ? props.backgroundColor
                : null
        }}
    >
        {props.children}
    </span>
);

export const GoldBadge = props => (
    <a href={"http://gold.getmakerlog.com/"}>
        <Badge className={"is-dark"} backgroundColor={"rgb(229,193,0)"}>
            <FontAwesomeIcon icon={"check-circle"} color="white" />
            &nbsp;<strong>Gold</strong>
        </Badge>
    </a>
);

export const DonorBadge = props => (
    <Badge className={"is-dark"}>
        <Emoji emoji={"✌️"} /> <span>Donor</span>
    </Badge>
);

export const GTDBadge = props => (
    <Badge className={"is-dark"} backgroundColor={"#666666"}>
        <Emoji emoji={"🚧"} /> <span>Ships Fast</span>
    </Badge>
);

export const StaffBadge = props => (
    <Badge className={"is-dark"}>
        <Emoji emoji={"👋"} /> <span>Staff</span>
    </Badge>
);

export const AwesomeBadge = props => (
    <Badge className={"is-dark"} backgroundColor={"#2d98da"}>
        <Emoji emoji={"💫"} /> <span>Awesome</span>
    </Badge>
);

export const OofBadge = props => (
    <Badge className={"is-dark"} backgroundColor={"#e86654"}>
        <Emoji emoji={"👹"} /> <span>Oof</span>
    </Badge>
);

export const FullSnackBadge = props => (
    <Badge className={"is-dark"} backgroundColor={"#2bae60"}>
        <Emoji emoji={"🌯"} /> <span>Full Snack</span>
    </Badge>
);

export const YearClubBadge = props => (
    <Badge className={"is-dark"} backgroundColor={"#2d98da"}>
        <span
            style={{
                textShadow: "0 0 10px hsla(0, 0%, 100%, 0.6",
                fontFamily:
                    "Apple Color Emoji, Segoe UI Symbol, Segoe UI Symbol",
                fontSize: "1.1em"
            }}
        >
            <Emoji emoji={"1️⃣"} />
        </span>{" "}
        <span>Year Club</span>
    </Badge>
);

export const HundredDayClubBadge = props => (
    <Badge className={"is-dark"} backgroundColor={"#ed776f"}>
        <span style={{ textShadow: "0 0 10px white" }}>
            <Emoji emoji={"💯"} />
        </span>{" "}
        <span>100 Day Club</span>
    </Badge>
);

export const FiftyDayClubBadge = props => (
    <Badge className={"is-dark"} backgroundColor={"#00b77a"}>
        <span style={{ textShadow: "0 0 3px white" }}>
            <Emoji emoji={"✳️"} />
        </span>{" "}
        <span>50 Day Club</span>
    </Badge>
);

export const InlineLevel = props => (
    <Level
        style={{ display: "inline-block", marginBottom: 0 }}
        className={"is-inline-flex"}
    >
        {props.children}
    </Level>
);

export const UserBadges = ({ user }) => {
    return (
        <InlineLevel>
            {user.gold && (
                <Level.Item
                    style={{ paddingRight: 2, paddingLeft: 2, marginRight: 0 }}
                >
                    <GoldBadge />
                </Level.Item>
            )}
            {(user.username.toLowerCase() === "booligoosh" ||
                user.username.toLowerCase() === "tomaswoksepp" ||
                user.username.toLowerCase() === "fajarsiddiq") && (
                <Level.Item
                    style={{ paddingRight: 2, paddingLeft: 2, marginRight: 0 }}
                >
                    <AwesomeBadge />
                </Level.Item>
            )}
            {user.username.toLowerCase() === "alina" && (
                <Level.Item
                    style={{ paddingRight: 2, paddingLeft: 2, marginRight: 0 }}
                >
                    <OofBadge />
                </Level.Item>
            )}
            {user.username.toLowerCase() === "joshmanders" && (
                <Level.Item
                    style={{ paddingRight: 2, paddingLeft: 2, marginRight: 0 }}
                >
                    <FullSnackBadge />
                </Level.Item>
            )}
            {user.streak >= 365 && (
                <Level.Item
                    style={{ paddingRight: 2, paddingLeft: 2, marginRight: 0 }}
                >
                    <YearClubBadge />
                </Level.Item>
            )}
            {user.streak >= 100 && user.streak < 365 && (
                <Level.Item
                    style={{ paddingRight: 2, paddingLeft: 2, marginRight: 0 }}
                >
                    <HundredDayClubBadge />
                </Level.Item>
            )}
            {user.streak >= 50 && user.streak < 100 && (
                <Level.Item
                    style={{ paddingRight: 2, paddingLeft: 2, marginRight: 0 }}
                >
                    <FiftyDayClubBadge />
                </Level.Item>
            )}
            {user.week_tda >= 10 && (
                <Level.Item
                    style={{ paddingRight: 2, paddingLeft: 2, marginRight: 0 }}
                >
                    <GTDBadge />
                </Level.Item>
            )}
            {user.is_staff && (
                <Level.Item
                    style={{ paddingRight: 2, paddingLeft: 2, marginRight: 0 }}
                >
                    <StaffBadge />
                </Level.Item>
            )}
            {user.donor && (
                <Level.Item
                    style={{ paddingRight: 2, paddingLeft: 2, marginRight: 0 }}
                >
                    <DonorBadge />
                </Level.Item>
            )}
        </InlineLevel>
    );
};

export const StatBar = ({ user, showUsername = false }) => (
    <div>
        {showUsername && <small>@{user.username}</small>}&nbsp;
        <Tag className={"is-rounded"}>
            <Streak days={user.streak} />
        </Tag>
        &nbsp;
        <Tag className={"is-rounded"}>
            <Tda tda={user.week_tda} />
        </Tag>
        <UserBadges user={user} />
    </div>
);

const StyledStreakBadge = styled(Tag)`
    color: white !important;
    background-color: ${props =>
        props.user.streak > 0 ? "#E48845" : "#f5f5f5"} !important;
`;

export const StreakBadge = ({ user }) => (
    <StyledStreakBadge className={"is-rounded"} user={user}>
        <Streak days={user.streak} />
    </StyledStreakBadge>
);

export const StatBadges = ({ user }) => (
    <small>
        <div className="tags has-addons">
            {user.streak > 0 && <StreakBadge user={user} />}
            <Tag
                className={"is-rounded is-dark"}
                style={{ backgroundColor: "#5A5A5A" }}
            >
                <Tda tda={user.week_tda} />
            </Tag>
        </div>
    </small>
);
