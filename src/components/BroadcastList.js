import React from 'react';
import {Card, Media, SubTitle, Title} from "../vendor/bulma";
import {getBroadcasts} from "../lib/broadcasts";
import OutboundLink from "./OutboundLink";

export const Broadcast = ({ broadcast }) => (
    <Media className={"Broadcast"}>
        <Media.Content>
            <Title is={"6"}>{broadcast.title}</Title>
            <SubTitle is={"6"} style={broadcast.url ? {marginTop: -18, fontSize: 15, marginBottom: 20} : {marginTop: -18, fontSize: 15}}>
                {broadcast.message}
            </SubTitle>
            {broadcast.url &&
                <OutboundLink className={"button is-small is-primary is-rounded"} to={broadcast.url}>
                    Take a look &raquo;
                </OutboundLink>
            }
        </Media.Content>
        {broadcast.attachment &&
            <Media.Right>
                <img alt={broadcast.title} src={broadcast.attachment} width="75" style={{borderRadius: 5}} />
            </Media.Right>
        }
    </Media>
)

class BroadcastList extends React.Component {
    state = {
        ready: false,
        broadcasts: [],
        failed: false,
    }

    componentDidMount() {
        this.getBroadcasts()
    }

    getBroadcasts = async () => {
        try {
            const broadcasts = await getBroadcasts();
            this.setState({ ready: true, failed: false, broadcasts })
        } catch (e) {
            this.setState({ ready: false, failed: true })
        }
    }

    render() {
        if (this.state.broadcasts.length && !this.state.failed) {
            return (
                <>
                    <Card style={{borderTop: '3px solid #00b77a', borderRadius: 5}}>
                        <Card.Content style={{borderTopLeftRadius: 5, borderTopRightRadius: 5}}>
                            <div className={"BroadcastList"}>
                                {this.state.broadcasts.map(
                                    broadcast => <Broadcast broadcast={broadcast} />
                                )}
                            </div>
                        </Card.Content>
                    </Card>
                    <br />
                </>
            )
        } else {
            return null
        }
    }
}

export default BroadcastList;