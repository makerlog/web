import React from 'react';
import {Icon} from "../vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import OutboundLink from "./OutboundLink";

export default (props) => (
    <OutboundLink className={"button is-dark is-gold is-rounded " + (props.className ? props.className : '')} to={"https://makerlog.io/gold"} target={'_blank'}>
        <Icon><FontAwesomeIcon icon={'check-circle'} /></Icon> <strong>Get Gold</strong>
    </OutboundLink>
)