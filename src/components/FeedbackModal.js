import React from 'react';
import Modal from 'components/Modal';
import {SubTitle, Title} from 'vendor/bulma';
import {StreamCard} from "../features/stream/components/Stream/components/StreamCard/styled";
import {Hero, Icon} from "../vendor/bulma";
import styled from 'styled-components';
import Emoji from "./Emoji";
import FontAwesomeIcon from "@fortawesome/react-fontawesome/src/components/FontAwesomeIcon";

const Option = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    padding-top: 30px;
    padding-bottom: 30px;
    border-radius: 10px;
    width: 100%;
    align-items: center;
    border: 3px solid #efefef;
    background-color: white;
    transition-delay: 0s;
    transition-duration: .25s;
    transition-property: background-color;
    transition-timing-function: ease-in-out;
    &:hover {
        background: #efefef;
    }

`

const OptionContainer = styled.div`
    width: 80%;
    text-align: center;
`


class FeedbackModal extends React.Component {
    render() {
        const { open, toggle } = this.props;

        return (
            <Modal
                open={open}
                background={'transparent'}
                flexDirection={'column'}
                modalStyles={{
                    overflowY: 'hidden'
                }}
                percentWidth={'50'}
                onClose={toggle}>
                <StreamCard>
                    <Hero primary style={{padding: 20}}>
                        <Title is={5}>Help & Feedback</Title>
                    </Hero>
                    <StreamCard.Content>
                        <div style={{ margin: '0 auto', padding: 30 }}>
                            <div className={"columns"}>
                                <div className={"column"}>
                                    <SubTitle is={5} style={{ marginBottom: 3 }}>
                                        <Emoji emoji={"⚡️"} /> Get help
                                    </SubTitle>
                                    <p className={"has-text-grey"} style={{ marginBottom: 20 }}>
                                        Having trouble? I'll help you out!
                                    </p>
                                    <div>
                                        <div className="field  has-addons">
                                            <p className="control">
                                                <a href={"https://twitter.com/messages/compose?recipient_id=727184556499988480"} className="button is-fullwidth is-rounded">
                                                    <Icon><FontAwesomeIcon icon={['fab', 'twitter']} /></Icon> Twitter DM
                                                </a>
                                            </p>
                                            <p className="control">
                                                <a href="https://t.me/matteing" className="button is-rounded">
                                                    <Icon><FontAwesomeIcon icon={['fab', 'telegram']} /></Icon> Telegram
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div className={"column"}>
                                    <SubTitle is={5} style={{ marginBottom: 3 }}>
                                        <Emoji emoji={"🐞️"} /> Report a bug
                                    </SubTitle>
                                    <p className={"has-text-grey"} style={{ marginBottom: 20 }}>
                                        I'll fix it right up!
                                    </p>
                                    <div>
                                        <a href={"https://twitter.com/messages/compose?recipient_id=727184556499988480"} className="button is-fullwidth is-rounded">
                                            <Icon><FontAwesomeIcon icon={'bug'} /></Icon> Report a bug
                                        </a>
                                    </div>
                                </div>
                                <div className={"column"}>
                                    <SubTitle is={5} style={{ marginBottom: 3 }}>
                                        <Emoji emoji={"✨"} /> Submit an idea
                                    </SubTitle>
                                    <p className={"has-text-grey"} style={{ marginBottom: 20 }}>
                                        I'm always open to new ideas!
                                    </p>
                                    <div>
                                        <a href={"https://twitter.com/messages/compose?recipient_id=727184556499988480"} className="button is-fullwidth is-rounded">
                                            <Icon><FontAwesomeIcon icon={'bullhorn'} /></Icon> Request a feature
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </StreamCard.Content>
                </StreamCard>
            </Modal>
        )
    }
}

export default FeedbackModal;