import React from 'react';
import ReactDOM from 'react-dom';
// import registerServiceWorker from './registerServiceWorker';
import {unregister} from './registerServiceWorker';
import axios from 'axios';
import ReactGA from 'react-ga';
import 'lib/fa';

import 'styles/theme.scss';

import App from './layouts/App';


if (process.env.NODE_ENV === 'production') {
    ReactGA.initialize('UA-121772728-1');
}

axios.defaults.baseURL = process.env.REACT_APP_API_URL;

ReactDOM.render(<App />, document.getElementById('root'));

if (module.hot && process.env.NODE_ENV !== 'production') {
    module.hot.accept();
}

unregister();
