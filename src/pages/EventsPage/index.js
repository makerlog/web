import "./index.scss";

import EventMedia from "../../features/events/components/EventMedia";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import InfiniteResults from "../../components/InfiniteResults";
import { Link } from "react-router-dom";
import LiveEventsList from "features/events/components/LiveEventsList";
import Page from "layouts/Page";
import React from "react";
import Sidebar from "../../features/events/components/Sidebar";
import { isOcurring } from "../../lib/utils/events";
import orderBy from "lodash/orderBy";

const EventsPage = () => {
    return (
        <Page contained={false} translucent className="EventsPage">
            <div className="main-hero">
                <div className="container">
                    <h1 className="title is-2">
                        <FontAwesomeIcon color="#3FDB96" icon="check-circle" />{" "}
                        Events
                    </h1>
                    <h1 className="subtitle is-4">
                        Find maker events and grow your network.
                    </h1>
                    <div>
                        <Link to="/events/host">
                            <button className="button is-medium is-primary is-rounded">
                                <span className="icon">
                                    <FontAwesomeIcon icon="users" />
                                </span>{" "}
                                &nbsp;
                                <span>Host an event</span>
                            </button>
                        </Link>
                    </div>
                </div>
            </div>

            <div className="container">
                <div className="columns">
                    <div className="column">
                        <LiveEventsList />

                        <h1 className="subtitle is-5 has-text-grey">Events</h1>

                        <InfiniteResults
                            url={"/events/"}
                            orderBy={data =>
                                orderBy(data, "closes_at", "asc").filter(
                                    e => !isOcurring(e)
                                )
                            }
                            component={({ items }) =>
                                items.map(event => (
                                    <EventMedia
                                        large={event.type === "HACKATHON"}
                                        event={event}
                                    />
                                ))
                            }
                        />
                    </div>
                    <div className="column is-one-third">
                        <br />
                        <br />
                        <Sidebar />
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default EventsPage;

/*
import "./index.scss";

import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import Page from "layouts/Page";
import React from "react";
import InfiniteResults from '../../components/InfiniteResults';
import { Link } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { orderBy } from 'lodash-es/orderBy';
import LiveEventsList from '../../features/events/components/LiveEventsList';

const EventsPage = () => {
    return (
        <Page transparent className="EventsPage">
            <div className="columns">
                <div className="column primary">
                    <h1 className="title is-1">
                        <FontAwesomeIcon color="#3FDB96" icon="check-circle" />{" "}
                        Events
                    </h1>
                    <h1 className="subtitle is-3">
                        Find maker events and grow your network.
                    </h1>
                    <div>
                        <button className="button is-medium is-primary is-rounded">
                            <span className="icon">
                                <FontAwesomeIcon icon="users" />
                            </span>{" "}
                            &nbsp;
                            <span>Host an event</span>
                        </button>
                    </div>
                </div>
                <div className="column">
                    <h1 className="subtitle is-5 has-text-grey">Upcoming</h1>
                    <div className="EventCard card">
                        <div className="card-content">
                            <div className="media">
                                <div className="media-left">
                                    <figure class="image is-48x48">
                                        <img src="https://bulma.io/images/placeholders/128x128.png" />
                                    </figure>
                                </div>
                                <div className="media-content">
                                    <h1 className="title is-4">The Fixathon</h1>
                                    <h1 className="subtitle is-6">
                                        The world's first climate change
                                        hackathon for indie makers
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default EventsPage;

*/
