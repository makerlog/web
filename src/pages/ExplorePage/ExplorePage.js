import { Redirect, Route } from "react-router-dom";

import AllProductsTab from "./components/AllProductsTab";
import { Button } from "vendor/bulma";
import Navigation from "./components/Navigation/Navigation";
import Page from "layouts/Page";
import PopularTab from "./components/PopularTab/index";
import React from "react";
import { Switch } from "react-router";
import { getWorldStats } from "../../lib/stats";

class ExplorePage extends React.Component {
    state = {
        isLoading: true,
        ready: false,
        failed: false,
        data: null
    };

    componentDidMount() {
        this.fetchStats();
    }

    fetchStats = async () => {
        try {
            const stats = await getWorldStats();
            this.setState({
                isLoading: false,
                ready: true,
                failed: false,
                data: stats
            });
        } catch (e) {
            this.setState({ isLoading: false, failed: true });
        }
    };

    render() {
        return (
            <Page loading={this.state.isLoading} contained={false}>
                <Route component={Navigation} />
                {this.state.failed && (
                    <div>
                        Failed to load this page.{" "}
                        <Button onClick={this.fetchStats}>Retry</Button>
                    </div>
                )}
                {this.state.ready && this.state.data && !this.state.failed && (
                    <div>
                        <Switch>
                            <Route
                                exact
                                path="/explore"
                                component={() => (
                                    <PopularTab worldStats={this.state.data} />
                                )}
                            />
                            <Route
                                exact
                                path="/explore/products"
                                component={() => (
                                    <AllProductsTab
                                        worldStats={this.state.data}
                                    />
                                )}
                            />
                            <Route
                                path="/explore/popular"
                                component={props => (
                                    <Redirect to={"/explore"} />
                                )}
                            />
                            <Route
                                component={props => <Redirect to={"/404"} />}
                            />
                        </Switch>
                    </div>
                )}
            </Page>
        );
    }
}

ExplorePage.propTypes = {};

export default ExplorePage;
