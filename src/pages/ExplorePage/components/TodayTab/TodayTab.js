import React from 'react';
import {Container} from "vendor/bulma";
import LoggedOutMessage from '../../../../components/LoggedOutMessage';

import {GlobalStream} from "features/stream";
import Sidebar from "pages/HomePage/Sidebar";

const TodayTab = (props) => (
    <Container className={"TodayTab"}>
        <br />
        <LoggedOutMessage />
        <div className={"columns"}>
            <div className={"column"}>
                <GlobalStream />
            </div>
            <div className={"column sidebar is-hidden-mobile"} style={{marginTop: 54}}>
                <Sidebar />
            </div>
        </div>
    </Container>
);


TodayTab.propTypes = {}

export default TodayTab;