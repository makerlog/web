import "./Navigation.css";

import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import { Icon } from "vendor/bulma";
import { NavLink } from "react-router-dom";
import OutboundLink from "components/OutboundLink";
import React from "react";

class Navigation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: false
        };
    }

    onClickCloseExpand = () => {
        if (this.state.expanded) {
            this.setState({ expanded: !this.state.expanded });
        }
    };

    render() {
        return (
            <nav
                className="navbar navbar-secondary is-white Navigation"
                aria-label="main navigation"
            >
                <div className={"container"}>
                    <div className="navbar-brand">
                        <h1 className="navbar-item">
                            <span className={"title is-5 brand-underline"}>
                                Explore
                            </span>
                        </h1>

                        <div
                            className="navbar-burger"
                            onClick={e =>
                                this.setState({
                                    expanded: !this.state.expanded
                                })
                            }
                        >
                            <span />
                            <span />
                            <span />
                        </div>
                    </div>
                    <div
                        className={
                            this.state.expanded
                                ? "navbar-menu is-active"
                                : "navbar-menu"
                        }
                    >
                        <div
                            className="navbar-start"
                            onClick={this.onClickCloseExpand}
                        >
                            <NavLink
                                className="navbar-item"
                                activeClassName="is-active"
                                to="/explore"
                                exact
                            >
                                <Icon medium>
                                    <FontAwesomeIcon icon={"fire"} />
                                </Icon>{" "}
                                <span>Popular</span>
                            </NavLink>
                            <NavLink
                                className="navbar-item"
                                activeClassName="is-active"
                                to="/live"
                                exact
                            >
                                <Icon medium>
                                    <FontAwesomeIcon icon={"play"} />
                                </Icon>{" "}
                                <span>Live</span>
                            </NavLink>
                            <NavLink
                                className="navbar-item"
                                activeClassName="is-active"
                                to="/explore/products"
                                exact
                            >
                                <Icon medium>
                                    <FontAwesomeIcon icon={"ship"} />
                                </Icon>{" "}
                                <span>Products</span>
                            </NavLink>
                        </div>

                        <div
                            className="navbar-end"
                            onClick={this.onClickCloseExpand}
                        >
                            <OutboundLink
                                className="navbar-item"
                                activeClassName="is-active"
                                href="https://open.getmakerlog.com"
                                exact
                            >
                                <Icon medium>
                                    <FontAwesomeIcon
                                        icon={["far", "chart-bar"]}
                                    />
                                </Icon>{" "}
                                <span>Open</span>
                            </OutboundLink>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}

Navigation.propTypes = {};

export default Navigation;
