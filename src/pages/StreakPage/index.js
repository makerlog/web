import React from "react";
import Page from "layouts/Page";
import "./index.scss";
import { Card, Message, SubTitle, Title } from "../../vendor/bulma";
import Emoji from "../../components/Emoji";
import WeekendModeSettings from "../../features/wellness/components/WeekendModeSettings";
import RestDaySettings from "../../features/wellness/components/RestDaySettings";
import withCurrentUser from "../../features/users/containers/withCurrentUser";
import Spinner from "../../components/Spinner";
import RestDayList from "../../features/wellness/components/RestDayList";

export default withCurrentUser(props => {
    if (!props.user) return <Spinner />;
    return (
        <Page className={"StreakPage"} transparent>
            <center>
                <Title is={"3"} className={"has-text-white"}>
                    You have a <Emoji emoji={"🔥"} /> {props.user.streak} day
                    streak.
                </Title>
                <SubTitle className={"has-text-white"}>
                    Remember to take breaks! Streaks should keep you productive,
                    not burn you out.
                </SubTitle>
            </center>

            <div className={"streak-content"}>
                <div className={"columns"}>
                    <div className={"column is-6 is-offset-3"}>
                        <Card>
                            <Card.Content>
                                <Title is={"5"}>
                                    <Emoji emoji={"🔥️"} /> Hardcore mode
                                </Title>
                                <SubTitle is={"6"}>
                                    Hardcore mode means rest days won't be
                                    automatically applied.
                                </SubTitle>

                                <RestDaySettings />

                                <hr />

                                <Title is={"5"}>
                                    <Emoji emoji={"😸️"} /> Weekend mode
                                </Title>
                                <SubTitle is={"6"}>
                                    Weekend mode allows you to take breaks on
                                    weekends. They won't count in your streak,
                                    but they won't break it either!
                                </SubTitle>
                                <Message danger>
                                    <Message.Body>
                                        Note that weekend mode works
                                        retroactively, so ALL past weekends in
                                        your current streak will be subtracted.
                                    </Message.Body>
                                </Message>

                                <WeekendModeSettings />

                                <hr />

                                <Title is={"5"}>
                                    <Emoji emoji={"😴"} /> Rest days taken
                                </Title>
                                <SubTitle is={"6"}>
                                    Here's a history of your breaks.
                                </SubTitle>
                                <Message info>
                                    <Message.Body>
                                        Rest days are vacation days that apply
                                        automatically when you don't log (unless
                                        Hardcore Mode is on). You earn one rest
                                        day for every 10 streak days.
                                    </Message.Body>
                                </Message>

                                <RestDayList />
                            </Card.Content>
                        </Card>
                    </div>
                </div>
            </div>
        </Page>
    );
});
