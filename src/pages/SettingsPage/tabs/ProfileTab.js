import React from 'react';
import {Button, Control, Field, File, Image, Input, Message, SubTitle, Tag, Title} from "vendor/bulma";
import {me, updateSettings} from "lib/user";
import Spinner from "components/Spinner";
import {omit} from "lodash-es";
import {Link} from "react-router-dom";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";

class ProfileTab extends React.Component {
    state = {
        isLoading: true,
        isPosting: false,
        first_name: '',
        last_name: '',
        status: '',
        description: '',
        twitter_handle: '',
        instagram_handle: '',
        telegram_handle: '',
        product_hunt_handle: '',
        shipstreams_handle: '',
        bmc_handle: '',
        website: '',
        github_handle: '',
        avatar: null,
        avatarUploading: false,
        avatarPreviewUrl: null,
        header: null,
        headerUploading: false,
        headerPreviewUrl: null,
        digest: true,
        errorMessages: null,
        accent: '',
    }

    fieldsToExclude = [
        'isLoading',
        'isPosting',
        'avatarPreviewUrl',
        'headerPreviewUrl',
    ]

    componentDidMount() {
        this.prefillFields()
    }

    onSubmit = async (event) => {
        event.preventDefault();
        if (this.state.description.length >= 50) {
            this.setState({ errorMessages: {'Tagline': 'Maximum 50 characters.'} })
            return null;
        }

        this.setState({ isPosting: true, })
        try {
            const formData = omit(this.state, this.fieldsToExclude);
            const user = await updateSettings(formData);
            if (this.props.updateUser) {
                this.props.updateUser(user)
            }
            this.setState({ isPosting: false, })
        } catch (e) {
            if (e.field_errors) {
                this.setState({ isPosting: false, errorMessages: e.field_errors })
            }
        }
    }

    onAvatarUpload = (event) => {
        const file = event.target.files[0];
        const reader  = new FileReader();

        this.setState({
            avatarUploading: true,
        })

        reader.onloadend = () => {
            this.setState({
                avatar: file,
                avatarPreviewUrl: reader.result,
                avatarUploading: false,
            });
        }

        reader.readAsDataURL(file)
    }

    onHeaderUpload = (event) => {
        const file = event.target.files[0];
        const reader  = new FileReader();

        this.setState({
            headerUploading: true
        })

        reader.onloadend = (e) => {
            this.setState({
                header: file,
                headerUploading: false,
                headerPreviewUrl: reader.result
            })
        }


        reader.readAsDataURL(file)
    }

    prefillFields = async () => {
        try {
            const data = await me();
            this.setState({
                isLoading: false,
                first_name: data.first_name || '',
                last_name: data.last_name || '',
                status: data.status || '',
                description: data.description || '',
                twitter_handle: data.twitter_handle || '',
                instagram_handle: data.instagram_handle || '',
                telegram_handle: data.telegram_handle || '',
                product_hunt_handle: data.product_hunt_handle || '',
                shipstreams_handle: data.shipstreams_handle || '',
                github_handle: data.github_handle || '',
                bmc_handle: data.bmc_handle || '',
                website: data.website || '',
                avatarPreviewUrl: data.avatar,
                headerPreviewUrl: data.header,
                digest: data.digest,
            })
        } catch (e) {

        }
    }

    renderErrorMessages = () => {
        let messages = [];
        let errors = this.state.errorMessages;
        if (typeof errors === 'object') {
            for (let key in errors) {
                messages.push(
                    <p>
                        <strong>{key.replace(/[_-]/g, " ")}</strong>: {errors[key]}
                    </p>
                )
            }

            return messages
        } else if (errors.constructor === Array) {
            errors.map((err) => {
                messages.push(
                    <p>{err}</p>
                )

                return true;
            })
        } else {
            return <p>{errors}</p>;
        }

        return messages
    }

    render() {
        if (this.state.isLoading) return <center><Spinner text={"Loading your settings..."} /></center>;

        return (
            <form onSubmit={this.onSubmit}>
                <div className={"columns"}>
                    <div className={"column"}>
                            <section className={"settings-header"}>
                                <Title is={"5"}>
                                    You
                                </Title>
                                <SubTitle is={"6"}>
                                    <small>The essentials.</small>
                                </SubTitle>
                            </section>
                            <Field>
                                <label className="label">First name</label>
                                <Control>
                                    <Input
                                        disabled={this.state.isPosting}
                                        value={this.state.first_name}
                                        placeholder="Your first name"
                                        onChange={(e) => this.setState({first_name: e.target.value})}/>
                                </Control>
                            </Field>
                            <Field>
                                <label className="label">Last name</label>
                                <Control>
                                    <Input
                                        disabled={this.state.isPosting}
                                        value={this.state.last_name}
                                        placeholder="Your last name"
                                        onChange={(e) => this.setState({last_name: e.target.value})} />
                                </Control>
                            </Field>
                            <Field>
                                <label className="label">Tagline</label>
                                <Control>
                                    <Input
                                        disabled={this.state.isPosting}
                                        value={this.state.description}
                                        danger={this.state.description.length >= 50}
                                        placeholder="Your tagline. Keep it short (max 50 chars.)"
                                        onChange={(e) => this.setState({description: e.target.value})} />
                                    {this.state.description.length >= 50 && <span className={"has-text-danger"}>Too long!</span>}
                                </Control>
                            </Field>
                            <label className="label">Website</label>
                            <Field>

                                <Control>
                                    <Input
                                        disabled={this.state.isPosting}
                                        value={this.state.website}
                                        placeholder="https://getmakerlog.com"
                                        onChange={(e) => this.setState({website: e.target.value})} />
                                </Control>
                                <p className="help">This will add a nice button to your profile.</p>
                            </Field>
                        <br />

                            <label className="label">Weekly digest</label>
                            <Field>
                                <Control>
                                    <label>
                                        <input
                                            type="checkbox"
                                            checked={this.state.digest}
                                            onChange={(e) => this.setState({digest: e.target.checked})} /> Subscribe to the weekly newsletter
                                    </label>
                                </Control>
                                <p className="help">
                                    Stay in the loop with a weekly digest chock-full of launches and achievements.
                                </p>
                            </Field>

                            <br/>

                            <Field>
                                <label className="label">Profile picture</label>
                                <File name boxed>
                                    <File.Label>
                                        <File.Input
                                            accept="image/*"
                                            onChange={this.onAvatarUpload} />
                                        <File.Cta>
                                            <File.Label as='span'>
                                                Choose an image…
                                                {this.state.avatarUploading &&
                                                <Spinner small text={"Uploading file..."} />
                                                }
                                                {this.state.avatarPreviewUrl &&
                                                <center>
                                                    <Image className="img-circle" is="64x64" src={this.state.avatarPreviewUrl} />
                                                </center>
                                                }
                                            </File.Label>
                                        </File.Cta>
                                    </File.Label>
                                </File>
                            </Field>

                            <br/>

                            <Field>
                                <label className="label">Header image</label>
                                <File name boxed>
                                    <File.Label>
                                        <File.Input
                                            accept="image/*"
                                            onChange={this.onHeaderUpload} />
                                        <File.Cta>
                                            <File.Label as='span'>
                                                Choose an image…
                                                {this.state.headerUploading &&
                                                <Spinner small text={"Uploading file..."} />
                                                }
                                                {this.state.headerPreviewUrl &&
                                                <center style={{width:200}}>
                                                    <Image ratio="16by9" src={this.state.headerPreviewUrl} />
                                                </center>
                                                }
                                            </File.Label>
                                        </File.Cta>
                                    </File.Label>
                                </File>
                            </Field>
                        </div>
                    <div className={"column"}>
                        <section className={"settings-header"}>
                            <Title is={"5"}>
                                Social
                            </Title>
                            <SubTitle is={"6"}>
                                <small>Show off some social flair in your profile!</small>
                            </SubTitle>
                        </section>

                        <label className="label">
                            Twitter handle
                        </label>
                        <Field hasAddons>
                            <Control>
                                <Button disabled>
                                    <FontAwesomeIcon icon={['fab', 'twitter']} color={"black"} />
                                </Button>
                            </Control>
                            <Control>
                                <Input
                                    disabled={this.state.isPosting}
                                    value={this.state.twitter_handle}
                                    placeholder="getmakerlog"
                                    onChange={(e) => this.setState({twitter_handle: e.target.value})} />
                            </Control>
                        </Field>

                        <label className="label">Telegram handle</label>
                        <Field hasAddons>
                            <Control>
                                <Button disabled>
                                    <FontAwesomeIcon icon={['fab', 'telegram']} color={'black'} />
                                </Button>
                            </Control>
                            <Control>
                                <Input
                                    disabled={this.state.isPosting}
                                    value={this.state.telegram_handle}
                                    placeholder="matteing"
                                    onChange={(e) => this.setState({telegram_handle: e.target.value})} />
                            </Control>
                        </Field>

                        <label className="label">Instagram handle</label>
                        <Field hasAddons>
                            <Control>
                                <Button disabled>
                                    <FontAwesomeIcon icon={['fab', 'instagram']} color={'black'} />
                                </Button>
                            </Control>
                            <Control>
                                <Input
                                    disabled={this.state.isPosting}
                                    value={this.state.instagram_handle}
                                    placeholder="johndoe"
                                    onChange={(e) => this.setState({instagram_handle: e.target.value})} />
                            </Control>
                        </Field>

                        <label className="label">Product Hunt handle</label>
                        <Field hasAddons>
                            <Control>
                                <Button disabled>
                                    <FontAwesomeIcon icon={['fab', 'product-hunt']} color={'black'} />
                                </Button>
                            </Control>
                            <Control>
                                <Input
                                    disabled={this.state.isPosting}
                                    value={this.state.product_hunt_handle}
                                    placeholder="ftxdri"
                                    onChange={(e) => this.setState({product_hunt_handle: e.target.value})} />
                            </Control>
                        </Field>

                        <label className="label">GitHub handle</label>
                        <Field hasAddons>
                            <Control>
                                <Button disabled>
                                    <FontAwesomeIcon icon={['fab', 'github']} color={'black'} />
                                </Button>
                            </Control>
                            <Control>
                                <Input
                                    disabled={this.state.isPosting}
                                    value={this.state.github_handle}
                                    placeholder="matteing"
                                    onChange={(e) => this.setState({github_handle: e.target.value})} />
                            </Control>
                        </Field>

                        <label className="label">Twitch handle</label>
                        <Field>
                            <Field hasAddons>
                                <Control>
                                    <Button disabled>
                                        <FontAwesomeIcon icon={['fab', 'twitch']} color={'black'} />
                                    </Button>
                                </Control>
                                <Control>
                                    <Input
                                        disabled={this.state.isPosting}
                                        value={this.state.shipstreams_handle}
                                        placeholder="sergiomattei"
                                        onChange={(e) => this.setState({shipstreams_handle: e.target.value})} />
                                </Control>
                            </Field>
                            <p className="help">This will turn on the Shipstreams integration. <Link to={'/apps/shipstreams/'}>Learn more</Link></p>
                        </Field>
                        <label className="label">Buy Me A Coffee handle</label>
                        <Field>
                            <Field hasAddons>
                                <Control>
                                    <Button disabled>
                                        <FontAwesomeIcon icon={'mug-hot'} color={'black'} />
                                    </Button>
                                </Control>
                                <Control>
                                    <Input
                                        disabled={this.state.isPosting}
                                        value={this.state.bmc_handle}
                                        placeholder="mattei"
                                        onChange={(e) => this.setState({bmc_handle: e.target.value})} />
                                </Control>
                            </Field>
                            <p className="help">This will add a donate button to your profile.</p>
                        </Field>
                    </div>
                </div>
                <hr />
                {this.state.errorMessages &&
                    <Message danger>
                        <Message.Body>
                            {this.renderErrorMessages()}
                        </Message.Body>
                    </Message>
                }
                <Button className={"is-rounded"} loading={this.state.isPosting} onClick={this.onSubmit} primary type="submit">Save</Button>
            </form>
        )
    }
}

export default ProfileTab;