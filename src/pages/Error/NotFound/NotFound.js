import React from 'react';
import {Button, Container, Hero, SubTitle, Title} from 'vendor/bulma'
import {Link} from "react-router-dom";
import Page from 'layouts/Page';

class NotFound extends React.Component {
    render() {
        return (
            <Page transparent className={"hero-page"}>
                <Hero fullheight>
                    <Hero.Body>
                        <Container>
                            <Title className={"has-text-white"}>
                                We couldn't find that.
                            </Title>
                            <SubTitle className={"has-text-white"}>
                                Makebot says "beep boop, error 404".
                            </SubTitle>
                            <Link to={"/"}>
                                <Button className={"is-rounded"}>Go home</Button>
                            </Link>
                        </Container>
                    </Hero.Body>
                </Hero>
            </Page>
        )
    }
}

NotFound.propTypes = {}

export default NotFound;