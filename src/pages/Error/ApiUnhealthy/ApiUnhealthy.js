import React from 'react';
import './ApiUnhealthy.css';
import {SubTitle, Title} from 'vendor/bulma';
import {connect} from 'react-redux';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

class ApiUnhealthy extends React.Component {
    render() {
        return (
            <div className="ApiUnhealthy">
                <div>
                    <Title is="2" hasTextWhite>
                        Makerlog is down.
                    </Title>
                    <SubTitle is="4">
                        Couldn't connect to Makerlog servers. Please try again later.
                    </SubTitle>
                    <SubTitle is="5">
                        <a href="https://twitter.com/getmakerlog">
                            <FontAwesomeIcon icon={['fab', 'twitter']} />
                        </a>
                    </SubTitle>
                </div>
            </div>
        )
    }
}

ApiUnhealthy.propTypes = {}


const mapStateToProps = (state) => {
    return {
        apiHealthy: state.app.healthy,
        errorMessages: state.app.errorMessages,
    }
}

export default connect(
    mapStateToProps
)(ApiUnhealthy);