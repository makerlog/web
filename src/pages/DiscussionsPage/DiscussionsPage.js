import React from "react";
import {
    Button,
    Card,
    Content,
    Icon,
    Image,
    Level,
    Media,
    Message,
    Title
} from "vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import { orderBy, uniqBy } from "lodash-es";
import {
    deleteReply,
    deleteThread,
    getThread,
    getThreadReplies,
    postReply,
    updateReply,
    updateThread
} from "../../lib/discussions";
import Spinner from "../../components/Spinner";
import TimeAgo from "react-timeago";
import { Link, Redirect, Route, Switch } from "react-router-dom";
import Textarea from "react-autosize-textarea";
import { Avatar, withCurrentUser } from "features/users";
import Modal from "../../components/Modal";
import Emoji from "../../components/Emoji";
import FullName from "../../features/users/components/FullName";
import config from "../../config";
import ShareBar from "../../components/ShareBar";
import Linkify from "react-linkify";
import Helmet from "react-helmet-async";
import Markdown from "components/Markdown";
import { ReplyFaces } from "features/discussions";
import Page from "../../layouts/Page";
import RecentDiscussionsCard from "components/sidebar/RecentDiscussionsCard";
import styled from "styled-components";
import { truncate } from "lib/utils/random";
import InfiniteResults from "../../components/InfiniteResults";
import { Praisable } from "../../features/stream/components/Task/components/Praise";
import ReconnectingWebSocket from "reconnecting-websocket";
import { socketUrl } from "../../lib/utils/random";
import { connect } from "react-redux";
import { actions as editorActions } from "../../ducks/editor";

const HelpText = styled.span`
    font-size: 12px;
    color: gray;
`;

function getThreadHeading(thread) {
    return (
        <Level mobile>
            <Level.Left>
                {thread.pinned && (
                    <Level.Item className={"is-brand-green"}>
                        <strong>
                            <FontAwesomeIcon size="xs" icon={"thumbtack"} />{" "}
                            Pinned
                        </strong>
                    </Level.Item>
                )}
                <Level.Item className={"has-text-grey"}>
                    by @{thread.owner.username}
                </Level.Item>
                <Level.Item>
                    <Icon>
                        <FontAwesomeIcon icon={"comments"} />
                    </Icon>{" "}
                    {thread.reply_count}
                </Level.Item>
                <Level.Item>
                    <Icon>
                        <FontAwesomeIcon icon={"clock"} />
                    </Icon>{" "}
                    <TimeAgo date={thread.created_at} />
                </Level.Item>
            </Level.Left>
        </Level>
    );
}

class ThreadStreamItem extends React.Component {
    renderActions = () => (
        <div className={"card-footer"}>
            <Link
                to={`/discussions/${this.props.thread.slug}`}
                className={"card-footer-item is-small has-text-grey"}
            >
                <Icon medium>
                    <FontAwesomeIcon icon={"reply"} />
                </Icon>
                <strong>Discuss</strong>
                &nbsp;&nbsp;
                <span className={"has-text-grey-light"}>
                    {this.props.thread.reply_count} replies
                </span>
            </Link>
            {this.props.thread.reply_count > 0 && (
                <div className={"has-text-grey card-footer-item"}>
                    <ReplyFaces size={32} threadSlug={this.props.thread.slug} />
                </div>
            )}
        </div>
    );

    renderThread = () => {
        let thread = this.props.thread;

        switch (thread.type) {
            case "TEXT":
            case "QUESTION":
                return (
                    <Card className={thread.pinned ? "pinned" : ""}>
                        <Media className={"has-hv-aligned-content"}>
                            <Media.Left>
                                <Avatar is={32} user={thread.owner} />
                            </Media.Left>
                            <Media.Content>
                                <Title
                                    is="6"
                                    style={{
                                        marginBottom: 4,
                                        fontFamily: "var(--font)"
                                    }}
                                >
                                    {thread.title}
                                </Title>
                                <small className={"has-text-grey"}>
                                    {truncate(thread.body, 25, "...")}
                                </small>
                                <small className={"has-text-grey-light"}>
                                    <Level>
                                        <Level.Left>
                                            <Level.Item>
                                                {getThreadHeading(thread)}
                                            </Level.Item>
                                        </Level.Left>
                                    </Level>
                                </small>
                            </Media.Content>
                            <Media.Right>
                                <ReplyFaces
                                    threadSlug={thread.slug}
                                    withOwner={false}
                                />
                            </Media.Right>
                        </Media>
                    </Card>
                );

            case "LINK":
                return (
                    <Card>
                        <Media className={"has-hv-aligned-content"}>
                            <Media.Left>
                                <Avatar is={32} user={thread.owner} />
                            </Media.Left>
                            <Media.Content>
                                <Title
                                    is="6"
                                    style={{
                                        marginBottom: 4,
                                        fontFamily: "var(--font)"
                                    }}
                                >
                                    {thread.title}
                                </Title>
                                <small className={"has-text-grey"}>
                                    {thread.body.indexOf("://") === -1
                                        ? "http://" + thread.body
                                        : thread.body}
                                </small>
                                <small className={"has-text-grey-light"}>
                                    <Level>
                                        <Level.Left>
                                            <Level.Item>
                                                {getThreadHeading(thread)}
                                            </Level.Item>
                                        </Level.Left>
                                    </Level>
                                </small>
                            </Media.Content>
                            <Media.Right>
                                <ReplyFaces
                                    threadSlug={thread.slug}
                                    withOwner={false}
                                />
                            </Media.Right>
                        </Media>
                    </Card>
                );

            default:
                return null;
        }
    };

    render() {
        let thread = this.props.thread;

        return (
            <Link to={`/discussions/${thread.slug}`}>
                <div className={"ThreadStreamItem"}>{this.renderThread()}</div>
            </Link>
        );
    }
}

const BodyEditor = props => (
    <div>
        <div className="field">
            <p className="control">
                <Textarea
                    rows={3}
                    value={props.value}
                    onChange={props.onChange}
                    className="textarea"
                    placeholder={"Write something..."}
                />
            </p>
        </div>
        <div className={"action-container"}>
            <Level mobile>
                <Level.Left>
                    <Level.Item>
                        <Button
                            className={"is-rounded"}
                            loading={props.loading}
                            disabled={props.loading}
                            onClick={props.onSubmit}
                            primary
                        >
                            <Icon medium>
                                <FontAwesomeIcon icon={"reply"} />
                            </Icon>{" "}
                            Submit
                        </Button>
                    </Level.Item>
                </Level.Left>
            </Level>
        </div>
    </div>
);

class ThreadEditor extends React.Component {
    state = {
        updating: false,
        body: null,
        failed: false
    };

    componentDidMount() {
        if (this.props.thread) {
            this.setState({
                body: this.props.thread.body
            });
        }
    }

    updateThread = async () => {
        try {
            await this.setState({
                updating: true,
                failed: false
            });
            await updateThread(this.props.thread.slug, {
                body: this.state.body
            });
            await this.setState({
                updating: false,
                failed: false
            });

            if (this.props.onFinish) {
                this.props.onFinish(this.state.body);
            }
        } catch (e) {
            this.setState({
                updating: false,
                failed: true
            });
        }
    };

    render() {
        return (
            <BodyEditor
                value={this.state.body}
                onChange={e => this.setState({ body: e.target.value })}
                loading={this.state.updating}
                onSubmit={this.updateThread}
            />
        );
    }
}

class ReplyEditor extends React.Component {
    state = {
        updating: false,
        body: null,
        failed: false
    };

    componentDidMount() {
        if (this.props.reply) {
            this.setState({
                body: this.props.reply.body
            });
        }
    }

    updateReply = async () => {
        try {
            await this.setState({
                updating: true,
                failed: false
            });
            await updateReply(this.props.reply.parent, this.props.reply.id, {
                body: this.state.body
            });
            await this.setState({
                updating: false,
                failed: false
            });

            if (this.props.onFinish) {
                this.props.onFinish(this.state.body);
            }
        } catch (e) {
            this.setState({
                updating: false,
                failed: true
            });
        }
    };

    render() {
        return (
            <BodyEditor
                value={this.state.body}
                onChange={e => this.setState({ body: e.target.value })}
                loading={this.state.updating}
                onSubmit={this.updateReply}
            />
        );
    }
}

const Thread = withCurrentUser(
    class Thread extends React.Component {
        state = {
            body: null,
            editing: false,
            deleting: false,
            deleted: false
        };

        async componentDidUpdate(prevProps) {
            if (this.props.thread !== prevProps.thread) {
                this.setState({
                    body: this.props.thread.body
                });
            }
        }

        getPermalink = () => {
            return `${config.BASE_URL}/discussions/${this.props.thread.slug}`;
        };

        generateTweetText = () => {
            return `${this.props.thread.title} \n ${this.getPermalink()}`;
        };

        toggleEditor = () => {
            this.setState({
                editing: !this.state.editing
            });
        };

        componentDidMount() {
            if (this.props.thread) {
                this.setState({
                    body: this.props.thread.body
                });
            }
        }

        deleteThread = async () => {
            await this.setState({
                deleting: true
            });
            await deleteThread(this.props.thread.slug);
            await this.setState({
                deleting: false,
                deleted: true
            });

            if (this.props.onDelete) {
                this.props.onDelete(this.props.thread.slug);
            }
        };

        render() {
            const { thread, replies = null, onCreateReply = null } = this.props;

            if (!this.props.thread) {
                return (
                    <Message danger>
                        <Message.Body>Invalid thread.</Message.Body>
                    </Message>
                );
            }

            return (
                <div className={"Thread"}>
                    <Level>
                        <Level.Left>
                            <h1 className={"subtitle is-4"}>{thread.title}</h1>
                        </Level.Left>
                        <Level.Right>
                            <a
                                href={"#ReplyForm"}
                                className={"button is-primary is-rounded"}
                            >
                                <Icon>
                                    <FontAwesomeIcon icon={"plus-square"} />
                                </Icon>{" "}
                                Reply
                            </a>
                        </Level.Right>
                    </Level>

                    <Card>
                        <Card.Content>
                            <Media>
                                <Media.Left>
                                    <Link to={`/@${thread.owner.username}`}>
                                        <Image
                                            className="img-circle"
                                            is={"48x48"}
                                            src={thread.owner.avatar}
                                        />
                                    </Link>
                                </Media.Left>
                                <Media.Content>
                                    <small className="heading has-text-grey">
                                        <FullName user={thread.owner} />
                                        &nbsp;&nbsp;
                                        <span className={"has-text-grey-light"}>
                                            <FontAwesomeIcon icon={"clock"} />{" "}
                                            <TimeAgo date={thread.created_at} />
                                        </span>
                                    </small>
                                    {this.state.deleting && (
                                        <Message danger>
                                            <Message.Body>
                                                <Spinner small /> Deleting...
                                            </Message.Body>
                                        </Message>
                                    )}
                                    {this.state.deleted && (
                                        <Redirect to={"/discussions"} />
                                    )}
                                    {!this.state.editing &&
                                        thread.type !== "LINK" && (
                                            <Content>
                                                <Linkify>
                                                    <Markdown
                                                        body={this.state.body}
                                                    />
                                                </Linkify>
                                            </Content>
                                        )}

                                    {!this.state.editing &&
                                        thread.type === "LINK" && (
                                            <Content>
                                                <Linkify>
                                                    <Markdown
                                                        body={`[${
                                                            thread.title
                                                        }](${this.state.body})`}
                                                    />
                                                </Linkify>
                                            </Content>
                                        )}
                                    {this.state.editing && (
                                        <ThreadEditor
                                            thread={thread}
                                            onFinish={body =>
                                                this.setState({
                                                    body: body,
                                                    editing: false
                                                })
                                            }
                                        />
                                    )}
                                    <ShareBar
                                        permalink={this.getPermalink()}
                                        tweetText={this.generateTweetText()}
                                        extraItemsLeft={props => {
                                            if (
                                                this.props.me.id ===
                                                thread.owner.id
                                            ) {
                                                return (
                                                    <React.Fragment>
                                                        <Level.Item>
                                                            <Button
                                                                text
                                                                small
                                                                onClick={
                                                                    this
                                                                        .toggleEditor
                                                                }
                                                            >
                                                                <Icon>
                                                                    <FontAwesomeIcon
                                                                        icon={
                                                                            "edit"
                                                                        }
                                                                        size={
                                                                            "sm"
                                                                        }
                                                                    />
                                                                </Icon>{" "}
                                                                Edit
                                                            </Button>
                                                        </Level.Item>
                                                        <Level.Item>
                                                            <Button
                                                                text
                                                                small
                                                                onClick={
                                                                    this
                                                                        .deleteThread
                                                                }
                                                            >
                                                                <Icon>
                                                                    <FontAwesomeIcon
                                                                        icon={
                                                                            "trash"
                                                                        }
                                                                        size={
                                                                            "sm"
                                                                        }
                                                                    />
                                                                </Icon>{" "}
                                                                Delete
                                                            </Button>
                                                        </Level.Item>
                                                    </React.Fragment>
                                                );
                                            }
                                        }}
                                    />
                                </Media.Content>
                            </Media>
                        </Card.Content>
                    </Card>

                    <br />

                    <h1 className={"subtitle is-5"}>
                        {thread.reply_count} replies
                    </h1>
                    <ReplyList
                        replies={replies}
                        thread={thread}
                        onCreateReply={onCreateReply}
                    />

                    <br />

                    <Card>
                        <Card.Content>
                            <ReplyForm
                                thread={thread}
                                onCreateReply={onCreateReply}
                            />
                        </Card.Content>
                    </Card>
                </div>
            );
        }
    }
);

const Reply = withCurrentUser(
    class Reply extends React.Component {
        state = {
            body: null,
            editing: false,
            deleting: false,
            deleted: false
        };

        componentDidMount() {
            if (this.props.reply) {
                this.setState({
                    body: this.props.reply.body
                });
            }
        }

        async componentDidUpdate(prevProps) {
            if (this.props.reply !== prevProps.reply) {
                this.setState({
                    body: this.props.reply.body
                });
            }
        }

        toggleEditor = () => {
            this.setState({
                editing: !this.state.editing
            });
        };

        deleteReply = async () => {
            await this.setState({
                deleting: true
            });
            await deleteReply(this.props.reply.id, this.props.reply.parent);
            await this.setState({
                deleting: false,
                deleted: true
            });

            if (this.props.onDelete) {
                this.props.onDelete(this.props.reply.id);
            }
        };

        render() {
            const { reply, child } = this.props;

            return (
                <Media
                    className={"Reply"}
                    style={child ? { paddingTop: 20, marginTop: 20 } : {}}
                >
                    <Media.Left>
                        <Link to={`/@${reply.owner.username}`}>
                            <Avatar user={reply.owner} is={24} />
                        </Link>
                    </Media.Left>
                    <Media.Content>
                        <div>
                            <Level style={{ marginBottom: 0 }} mobile>
                                <Level.Left>
                                    <Level.Item>
                                        <small className="heading has-text-grey">
                                            <FullName user={reply.owner} />
                                        </small>
                                    </Level.Item>
                                    <Level.Item>
                                        <small className="heading has-text-grey-light">
                                            <FontAwesomeIcon icon={"clock"} />{" "}
                                            <TimeAgo date={reply.created_at} />
                                        </small>
                                    </Level.Item>
                                </Level.Left>
                            </Level>
                            {this.state.deleting && (
                                <Spinner small text={"One moment..."} />
                            )}
                            {this.state.deleted && (
                                <em>This reply has been deleted.</em>
                            )}
                            {!this.state.editing && !this.state.deleted && (
                                <Content>
                                    <Linkify>
                                        <Markdown body={this.state.body} />
                                    </Linkify>
                                </Content>
                            )}
                            {this.state.editing && (
                                <ReplyEditor
                                    reply={reply}
                                    onFinish={body =>
                                        this.setState({
                                            body: body,
                                            editing: false
                                        })
                                    }
                                />
                            )}
                            <Level>
                                <Level.Left>
                                    <Level.Item>
                                        <Praisable
                                            button
                                            indexUrl={`/discussions/${
                                                reply.parent
                                            }/replies/${reply.id}`}
                                            initialAmount={reply.praise}
                                            item={reply}
                                        />
                                    </Level.Item>
                                    <Level.Item>
                                        <Button
                                            className={"is-rounded"}
                                            text
                                            small
                                            onClick={() =>
                                                this.props.onClickReply(
                                                    this.props.reply
                                                )
                                            }
                                        >
                                            <Icon>
                                                <FontAwesomeIcon
                                                    icon={"reply"}
                                                    size={"sm"}
                                                />
                                            </Icon>{" "}
                                            Reply
                                        </Button>
                                    </Level.Item>
                                    {this.props.me.id ===
                                        this.props.reply.owner.id && (
                                        <>
                                            <Level.Item>
                                                <Button
                                                    text
                                                    small
                                                    onClick={this.toggleEditor}
                                                >
                                                    <Icon>
                                                        <FontAwesomeIcon
                                                            icon={"edit"}
                                                            size={"sm"}
                                                        />
                                                    </Icon>{" "}
                                                    Edit
                                                </Button>
                                            </Level.Item>
                                            <Level.Item>
                                                <Button
                                                    text
                                                    small
                                                    onClick={this.deleteReply}
                                                >
                                                    <Icon>
                                                        <FontAwesomeIcon
                                                            icon={"trash"}
                                                            size={"sm"}
                                                        />
                                                    </Icon>{" "}
                                                    Delete
                                                </Button>
                                            </Level.Item>
                                        </>
                                    )}
                                </Level.Left>
                            </Level>
                        </div>
                        {this.props.children}
                    </Media.Content>
                </Media>
            );
        }
    }
);

const ReplyForm = withCurrentUser(
    class ReplyForm extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                isCreating: false,
                body: props.prefillText ? props.prefillText : "",
                failed: false
            };
        }

        onSubmit = async () => {
            try {
                await this.setState({
                    isCreating: true,
                    failed: false
                });

                const reply = await postReply(
                    this.props.thread.slug,
                    this.state.body,
                    this.props.parentReply ? this.props.parentReply : null
                );

                if (this.props.onCreateReply) {
                    this.props.onCreateReply(reply);
                }

                this.setState({
                    isCreating: false,
                    body: "",
                    failed: false
                });
            } catch (e) {
                this.setState({
                    isCreating: false,
                    failed: true
                });
            }
        };

        render() {
            if (!this.props.isLoggedIn) {
                return (
                    <Message primary id={"ReplyForm"}>
                        <Message.Body>
                            You must be signed in to reply.{" "}
                            <Link
                                className="button is-primary is-small is-rounded"
                                to={"/begin"}
                            >
                                Get started &raquo;
                            </Link>
                        </Message.Body>
                    </Message>
                );
            }

            return (
                <Media className={"ReplyForm"} id={"ReplyForm"}>
                    <Media.Left>
                        <Avatar user={this.props.me} is={32} />
                    </Media.Left>
                    <Media.Content>
                        <div>
                            <div className="field">
                                <p className="control">
                                    <Textarea
                                        innerRef={input =>
                                            input &&
                                            this.props.focused &&
                                            input.focus()
                                        }
                                        rows={3}
                                        value={this.state.body}
                                        onKeyDown={e => {
                                            if (
                                                e.keyCode === 13 &&
                                                (e.ctrlKey || e.metaKey)
                                            )
                                                this.onSubmit(e);
                                        }}
                                        onChange={e => {
                                            this.setState({
                                                body: e.target.value
                                            });
                                        }}
                                        className="textarea"
                                        placeholder={"Write a reply..."}
                                    />
                                </p>
                            </div>
                            {this.state.body.length > 0 && (
                                <HelpText>
                                    <FontAwesomeIcon
                                        icon={["fab", "markdown"]}
                                    />{" "}
                                    Markdown is enabled. Cmd/Ctrl+Enter to
                                    finish.{" "}
                                </HelpText>
                            )}
                            <div className={"action-container"}>
                                <Level mobile>
                                    <Level.Left>
                                        <Level.Item>
                                            <Button
                                                className={"is-rounded"}
                                                loading={this.state.isCreating}
                                                disabled={this.state.isCreating}
                                                onClick={this.onSubmit}
                                                primary
                                            >
                                                <Icon medium>
                                                    <FontAwesomeIcon
                                                        icon={"reply"}
                                                    />
                                                </Icon>{" "}
                                                Post reply
                                            </Button>
                                        </Level.Item>
                                    </Level.Left>
                                </Level>
                            </div>
                        </div>
                    </Media.Content>
                </Media>
            );
        }
    }
);

class ReplyThread extends React.Component {
    state = {
        replying: false,
        prefillText: ""
    };

    onClickReply = () => {
        this.setState({
            replying: true
        });
    };

    onCreateReply = reply => {
        this.setState({
            replying: false
        });

        if (this.props.onCreateReply) {
            this.props.onCreateReply(reply);
        }
    };

    onClickChildReply = reply => {
        this.setState({
            replying: true,
            prefillText: `@${reply.owner.username} `
        });
    };

    render() {
        const { thread, reply, childrenReplies } = this.props;
        return (
            <Card style={{ marginBottom: 20 }}>
                <Card.Content>
                    <Reply reply={reply} onClickReply={this.onClickReply}>
                        {orderBy(childrenReplies, "created_at", "asc").map(
                            ch => (
                                <Reply
                                    child={true}
                                    onClickReply={this.onClickChildReply}
                                    reply={ch}
                                />
                            )
                        )}
                        {this.state.replying && (
                            <ReplyForm
                                focused
                                prefillText={this.state.prefillText}
                                parentReply={reply.id}
                                thread={thread}
                                onCreateReply={this.onCreateReply}
                            />
                        )}
                    </Reply>
                </Card.Content>
            </Card>
        );
    }
}

const ReplyList = ({ replies, thread, onCreateReply = null }) => (
    <div>
        {replies.length === 0 && (
            <Card>
                <Card.Content>
                    <Emoji emoji={"🤔"} /> Nothing yet. Start the conversation!
                </Card.Content>
            </Card>
        )}
        {orderBy(
            replies.filter(rep => !rep.parent_reply),
            "created_at",
            "asc"
        ).map(r => (
            <ReplyThread
                reply={r}
                thread={thread}
                childrenReplies={replies.filter(
                    child => child.parent_reply === r.id
                )}
                onCreateReply={onCreateReply}
            />
        ))}
    </div>
);

class Discussion extends React.Component {
    state = {
        isLoading: true,
        thread: null,
        replies: null,
        isCreating: false,
        failedCreating: false,
        replyEditorValue: "",
        failed: false
    };

    connect = () => {
        this.socket = new ReconnectingWebSocket(
            socketUrl(`/discussions/${this.props.match.params.slug}/`)
        );
        this.socket.onopen = () => {
            console.log(
                `Makerlog: Established connection to /discussions/${
                    this.props.match.params.slug
                }/.`
            );
        };
        this.socket.onmessage = this.onWsEvent;
        this.socket.onclose = () => {
            console.log(
                `Makerlog: Closed connection to /discussions/${
                    this.props.match.params.slug
                }/.`
            );
        };
    };

    onWsEvent = event => {
        const data = JSON.parse(event.data);
        switch (data.type) {
            case "reply.created":
            case "reply.updated":
                this.setState({
                    replies: uniqBy([data.payload, ...this.state.replies], "id")
                });
                break;

            case "reply.deleted":
                this.setState({
                    replies: this.state.replies.filter(
                        t => t.id !== data.payload.id
                    )
                });
                break;

            default:
                return;
        }
    };

    disconnect = () => {
        if (this.socket) {
            this.socket.close();
        }
    };

    async componentDidMount() {
        // Load thread or 404.
        this.setState({ isLoading: true });
        await this.loadThread();
        await this.loadReplies();
        this.setState({ isLoading: false });
        this.connect();
    }

    async componentDidUpdate(prevProps) {
        if (this.props.match.params.slug !== prevProps.match.params.slug) {
            await this.componentDidMount();
        }
    }

    componentWillUnmount() {
        this.disconnect();
    }

    loadThread = async () => {
        try {
            this.setState({
                thread: await getThread(this.props.match.params.slug),
                failed: false
            });
        } catch (e) {
            this.setState({
                thread: null,
                failed: true
            });
        }
    };

    loadReplies = async () => {
        try {
            this.setState({
                replies: await getThreadReplies(this.props.match.params.slug),
                failed: false
            });
        } catch (e) {
            this.setState({
                replies: null,
                failed: true
            });
        }
    };

    onCreateReply = reply => {
        /*const newState = {
            replies: [...this.state.replies, reply]
        }

        this.setState(newState)*/
    };

    render() {
        if (this.state.isLoading) {
            return (
                <center>
                    <Spinner text={"Loading discussion..."} />
                </center>
            );
        }

        if (
            this.state.failed ||
            (!this.state.isLoading && !this.state.thread)
        ) {
            return <Redirect to={"/404"} />;
        }

        return (
            <div>
                <Helmet>
                    <title>{this.state.thread.title} | Makerlog</title>
                    <meta
                        name="description"
                        content={truncate(this.state.thread.body, 10, "...")}
                    />
                    <meta name="twitter:card" content={"summary"} />
                    <meta name="twitter:site" content="@getmakerlog" />
                    <meta
                        name="twitter:title"
                        content={this.state.thread.title}
                    />
                    <meta
                        name="twitter:description"
                        content={truncate(this.state.thread.body, 10, "...")}
                    />
                    <meta
                        name="twitter:image"
                        content={this.state.thread.owner.avatar}
                    />
                </Helmet>
                <Thread
                    thread={this.state.thread}
                    showActions={false}
                    replies={this.state.replies}
                    onCreateReply={this.onCreateReply}
                />
            </div>
        );
    }
}

const ThreadStream = ({ threads, compact = false }) => (
    <InfiniteResults
        url={"/discussions/"}
        withSockets
        socketTypePrefix={"thread"}
        component={({ items }) => (
            <div className={"DiscussionsList timeline"}>
                {items.map(t => (
                    <ThreadStreamItem thread={t} />
                ))}
            </div>
        )}
    />
);

export const ThreadTypeSelect = ({ isSelected, ...props }) => (
    <div
        className={
            isSelected ? `ThreadTypeSelect is-selected` : `ThreadTypeSelect`
        }
        onClick={props.onClick}
    >
        {props.children}
    </div>
);

let NewTopicButton = props => {
    if (!props.isLoggedIn) {
        return (
            <Link to={"/begin"} className={"button is-primary is-rounded"}>
                <Icon large>
                    <FontAwesomeIcon icon={"plus-square"} />
                </Icon>{" "}
                <strong>New topic</strong>
            </Link>
        );
    }
    return (
        <Button primary className={"is-rounded"} onClick={props.openEditor}>
            <Icon large>
                <FontAwesomeIcon icon={"plus-square"} />
            </Icon>{" "}
            <strong>New topic</strong>
        </Button>
    );
};

NewTopicButton = connect(
    state => ({
        isLoggedIn: state.auth.loggedIn
    }),
    dispatch => ({
        openEditor: () => dispatch(editorActions.openDiscussionEditor(false))
    })
)(NewTopicButton);

const GreetingCard = withCurrentUser(props => {
    return (
        <Card className={"PageCard"}>
            <Card.Content>
                <Title
                    is="5"
                    className={"has-text-white"}
                    style={{ marginBottom: 4 }}
                >
                    Join the conversation
                </Title>
                <small>
                    Ask questions, share links, tell us your achievements, and
                    more!
                </small>
            </Card.Content>
        </Card>
    );
});

const HeaderBar = ({ title, onCreate }) => (
    <Level mobile>
        <Level.Left>
            <Level.Item>
                <Title is={"4"}>{title}</Title>
            </Level.Item>
        </Level.Left>
        <Level.Right>
            <NewTopicButton primary onCreate={onCreate} />
        </Level.Right>
    </Level>
);

const Sidebar = props => (
    <div>
        <Route
            path={"/discussions"}
            exact
            component={() => (
                <div>
                    <GreetingCard />
                    <br />
                </div>
            )}
        />
        <RecentDiscussionsCard />
    </div>
);

const DiscussionPage = props => (
    <div>
        <Discussion match={props.match} />
    </div>
);

const DiscussionsPage = withCurrentUser(props => (
    <Page className="DiscussionsPage">
        <div className={"columns"}>
            <div className={"column"}>
                <Switch>
                    <Route
                        path={"/discussions/:slug"}
                        component={DiscussionPage}
                    />
                    <Route
                        path={"/discussions"}
                        component={props => (
                            <div>
                                <HeaderBar title={"Recent discussions"} />
                                <ThreadStream />
                            </div>
                        )}
                    />
                </Switch>
            </div>
            <div
                className={"column is-hidden-mobile"}
                style={{ maxWidth: 400 }}
            >
                <Sidebar />
            </div>
        </div>
    </Page>
));

DiscussionsPage.propTypes = {};

export default DiscussionsPage;
