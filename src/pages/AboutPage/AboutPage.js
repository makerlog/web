import React from "react";
import { actions as userActions } from "ducks/user";
import { connect } from "react-redux";
import { Card, Container, Content, Hero, Tabs, Title } from "vendor/bulma";
import { Link } from "react-router-dom";
import Page from "layouts/Page";

const TermsOfService = () => (
    <Content>
        <h2>Makerlog Terms of Service</h2>
        <h3>1. Terms</h3>
        <p>
            By accessing the website at{" "}
            <a href="http://getmakerlog.com">http://getmakerlog.com</a>, you are
            agreeing to be bound by these terms of service, all applicable laws
            and regulations, and agree that you are responsible for compliance
            with any applicable local laws. If you do not agree with any of
            these terms, you are prohibited from using or accessing this site.
            The materials contained in this website are protected by applicable
            copyright and trademark law.
        </p>
        <h3>2. Use License</h3>
        <ol type="a">
            <li>
                Permission is granted to temporarily download one copy of the
                materials (information or software) on Makerlog's website for
                personal, non-commercial transitory viewing only. This is the
                grant of a license, not a transfer of title, and under this
                license you may not:
                <ol type="i">
                    <li>modify or copy the materials;</li>
                    <li>
                        use the materials for any commercial purpose, or for any
                        public display (commercial or non-commercial);
                    </li>
                    <li>
                        attempt to decompile or reverse engineer any software
                        contained on Makerlog's website;
                    </li>
                    <li>
                        remove any copyright or other proprietary notations from
                        the materials; or
                    </li>
                    <li>
                        transfer the materials to another person or "mirror" the
                        materials on any other server.
                    </li>
                </ol>
            </li>
            <li>
                This license shall automatically terminate if you violate any of
                these restrictions and may be terminated by Makerlog at any
                time. Upon terminating your viewing of these materials or upon
                the termination of this license, you must destroy any downloaded
                materials in your possession whether in electronic or printed
                format.
            </li>
        </ol>
        <h3>3. Disclaimer</h3>
        <ol type="a">
            <li>
                The materials on Makerlog's website are provided on an 'as is'
                basis. Makerlog makes no warranties, expressed or implied, and
                hereby disclaims and negates all other warranties including,
                without limitation, implied warranties or conditions of
                merchantability, fitness for a particular purpose, or
                non-infringement of intellectual property or other violation of
                rights.
            </li>
            <li>
                Further, Makerlog does not warrant or make any representations
                concerning the accuracy, likely results, or reliability of the
                use of the materials on its website or otherwise relating to
                such materials or on any sites linked to this site.
            </li>
        </ol>
        <h3>4. Limitations</h3>
        <p>
            In no event shall Makerlog or its suppliers be liable for any
            damages (including, without limitation, damages for loss of data or
            profit, or due to business interruption) arising out of the use or
            inability to use the materials on Makerlog's website, even if
            Makerlog or a Makerlog authorized representative has been notified
            orally or in writing of the possibility of such damage. Because some
            jurisdictions do not allow limitations on implied warranties, or
            limitations of liability for consequential or incidental damages,
            these limitations may not apply to you.
        </p>
        <h3>5. Accuracy of materials</h3>
        <p>
            The materials appearing on Makerlog's website could include
            technical, typographical, or photographic errors. Makerlog does not
            warrant that any of the materials on its website are accurate,
            complete or current. Makerlog may make changes to the materials
            contained on its website at any time without notice. However
            Makerlog does not make any commitment to update the materials.
        </p>
        <h3>6. Links</h3>
        <p>
            Makerlog has not reviewed all of the sites linked to its website and
            is not responsible for the contents of any such linked site. The
            inclusion of any link does not imply endorsement by Makerlog of the
            site. Use of any such linked website is at the user's own risk.
        </p>
        <h3>7. Modifications</h3>
        <p>
            Makerlog may revise these terms of service for its website at any
            time without notice. By using this website you are agreeing to be
            bound by the then current version of these terms of service.
        </p>
        <h3>8. Governing Law</h3>
        <p>
            These terms and conditions are governed by and construed in
            accordance with the laws of Puerto Rico and you irrevocably submit
            to the exclusive jurisdiction of the courts in that State or
            location.
        </p>
        <h2>Privacy Policy</h2>
        <p>
            Your privacy is important to us. It is Makerlog's policy to respect
            your privacy regarding any information we may collect from you
            across our website,{" "}
            <a href="http://getmakerlog.com">http://getmakerlog.com</a>, and
            other sites we own and operate.
        </p>
        <p>
            We only ask for personal information when we truly need it to
            provide a service to you. We collect it by fair and lawful means,
            with your knowledge and consent. We also let you know why we’re
            collecting it and how it will be used.
        </p>
        <p>
            We only retain collected information for as long as necessary to
            provide you with your requested service. What data we store, we’ll
            protect within commercially acceptable means to prevent loss and
            theft, as well as unauthorised access, disclosure, copying, use or
            modification.
        </p>
        <p>
            We don’t share any personally identifying information publicly or
            with third-parties, except when required to by law.
        </p>
        <p>
            Our website may link to external sites that are not operated by us.
            Please be aware that we have no control over the content and
            practices of these sites, and cannot accept responsibility or
            liability for their respective privacy policies.
        </p>
        <p>
            You are free to refuse our request for your personal information,
            with the understanding that we may be unable to provide you with
            some of your desired services.
        </p>
        <p>
            Your continued use of our website will be regarded as acceptance of
            our practices around privacy and personal information. If you have
            any questions about how we handle user data and personal
            information, feel free to contact us.
        </p>
        <p>This policy is effective as of 13 June 2018.</p>
    </Content>
);

const FrequentlyAsked = props => (
    <Content>
        <h2>What is Makerlog?</h2>
        <p>
            Makerlog is a community of 3000+ makers achieving their goals
            together, created by <Link to={"/@sergio"}>Sergio Mattei</Link>.
        </p>
        <h2>How do I add tasks on Makerlog?</h2>
        <p>
            You can add a task on GetMakerlog.com using the{" "}
            <strong>"Add a new task" button</strong>. To change the task's
            status, use the icon in front of the text field you type the task
            on:
        </p>
        <ul>
            <li>
                The default status is <strong>"done"</strong> and it appears as
                a green tick icon.
            </li>
            <li>
                Click on it and it will change to an orange circle (which sets
                status <strong>"todo"</strong>).
            </li>
            <li>
                Click one more time and you will get the orange circle to
                pulsate (which sets task as <strong>"in progress"</strong>).
            </li>
        </ul>
        <h2>How do I add tasks from Telegram?</h2>
        <p>You can also add tasks easily from Telegram.</p>
        <p>
            Join the <a href="https://t.me/Makerlog">Makerlog Telegram group</a>{" "}
            and then pair your account with Makebot by typing <code>/pair</code>{" "}
            to <a href="https://t.me/Makerlogbot">MakerlogBot</a>.
        </p>
        <p>
            After pairing you can add tasks by using <code>/done</code> or{" "}
            <code>/todo</code> or <code>/now</code> followed by the task text.
        </p>
        <ul>
            <li>
                <code>/done</code> will set the task as done
            </li>
            <li>
                <code>/todo</code> will set the task as todo
            </li>
            <li>
                <code>/now</code> will set the task as in progress
            </li>
        </ul>
        <p>
            You can always type <code>/help</code> to see all available
            commands.
        </p>
        <h2>How do I add tasks from other apps?</h2>
        <p>
            To see Makerlog integrations, check out the{" "}
            <strong>Integrations/Apps Page</strong> at{" "}
            <Link to={"/apps"}>Tasks > Integrations</Link>.
        </p>
        <p>Currently you can log from:</p>
        <ul>
            <li>Github</li>
            <li>Gitlab</li>
            <li>Telegram</li>
            <li>Todoist</li>
            <li>Trello</li>
            <li>NodeHost</li>
            <li>Slack</li>
            <li>Shipstreams</li>
            <li>... or generate a webhook to log from other apps.</li>
        </ul>
        There's also:
        <ul>
            <li>
                a{" "}
                <a
                    href="https://play.google.com/store/apps/details?id=com.brownfingers.getmakerlog"
                    rel={"noopener noreferrer"}
                    target={"_blank"}
                >
                    mobile client for Android
                </a>{" "}
                by <Link to={"/@arnav"}>Arnav</Link>
            </li>
            <li>
                a{" "}
                <a href="https://menubar.getmakerlog.com/">
                    Mac OS menubar app
                </a>{" "}
                by <Link to={"/@Booligoosh"}>Ethan</Link>
            </li>
            <li>
                <a href="https://today.jipfr.nl/">Today for Makerlog</a> by{" "}
                <Link to={"/@jip"}>Jip</Link>
            </li>
            <li>
                a{" "}
                <a
                    href="https://makerlog-buymeacoffee.netlify.com/"
                    rel={"noopener noreferrer"}
                    target={"_blank"}
                >
                    BuyMeACoffee integration
                </a>{" "}
                by <Link to={"/@voinea"}>Mihai Voinea</Link>
            </li>
            <li>
                <a
                    href="https://github.com/MihaiVoinea/makerlog-cli/"
                    rel={"noopener noreferrer"}
                    target={"_blank"}
                >
                    Makerlog CLI
                </a>{" "}
                by <Link to={"/@voinea"}>Mihai Voinea</Link>
            </li>
            <li>
                <a href="https://assistant.getmakerlog.com/">
                    Makerlog for Google Assistant
                </a>{" "}
                by <Link to={"/@arturs"}>Arturs Dobrecovs</Link>
            </li>
        </ul>
        <h2>How does the streak work?</h2>
        <ul>
            <li>
                You must add at least one task{" "}
                <strong>before 12AM in your CURRENT timezone.</strong> (please
                be aware of this when traveling)
            </li>
            <li>In progress and todo tasks are not counted.</li>
            <li>
                <strong>
                    A task counts for the day it was marked as done, not when it
                    was created.
                </strong>{" "}
                If you added a task yesterday and completed it today, it was
                marked as done today and counts for today's streak. It will
                however remain in your log in the day you added it.
            </li>
        </ul>
        <h2>Help, I lost my streak!</h2>
        Possible reasons:
        <ul>
            <li>
                <strong>Travel</strong> (check out "How does the streak work?"
                above)
            </li>
            <li>
                <strong>Algorithm issues</strong>
            </li>
        </ul>
        If you believe this was an error,{" "}
        <a href="https://pm.mattei.dev/issues/">add a support ticket here</a>.
        <h2>What are wellness features?</h2>
        <p>
            <strong>
                Resting is very important in preventing burnout. We know that,
                so Makerlog has Rest Days and Weekend Mode as wellness feature.
            </strong>
        </p>
        <p>Take days off without breaking your streak!</p>
        <p>
            You can access the Wellness features in{" "}
            <Link to={"/streak"}>Tasks > Wellness</Link>.
        </p>
        <h2>How do rest days work?</h2>
        <ul>
            <li>You get 1 rest day for each 10 days of streak</li>
            <li>
                Rest days are automatically applied for the days when you don't
                log any tasks
            </li>
            <li>
                Rest days will not break your streak, but they will not count
                towards it
            </li>
            <li>
                <strong>
                    If you don't want rest days to be automatically applied
                </strong>
                , you can enable Hardcore mode in{" "}
                <Link to={"/streak"}>Tasks > Wellness</Link>
            </li>
        </ul>
        <h2>How does weekend mode work?</h2>
        <p>Take the weekends off without breaking your streak!</p>
        <p>
            If weekends are resting time for you and don't ship, activate
            weekend mode.
        </p>
        With weekend mode enabled:
        <ul>
            <li>
                Weekend days will not break your streak, but they will not count
                towards it
            </li>
            <li>
                <strong>Note that Weekend Mode works retroactively!</strong>{" "}
                Means that when you activate it, it will subtract from your
                current streak all previous weekend days, so in some cases a
                decrease of the streak is possible. Turning it off and adding a
                new task will reset that to the previousl value.
            </li>
        </ul>
    </Content>
);

class AboutPage extends React.Component {
    state = {
        activeTab: 1
    };

    switchToTab = index => {
        this.setState({
            activeTab: index
        });
    };

    renderTabLink = (name, index) => (
        <li className={this.state.activeTab === index ? "is-active" : null}>
            {" "}
            {/* eslint-disable-next-line */}
            <a onClick={() => this.switchToTab(index)}>{name}</a>
        </li>
    );

    render() {
        return (
            <Page contained={false} className="AboutPage">
                <Hero primary>
                    <Hero.Body>
                        <Container>
                            <Title is={"4"}>About, FAQs, Terms & Privacy</Title>
                        </Container>
                    </Hero.Body>
                </Hero>
                <br />
                <Container>
                    <Card>
                        <Card.Header>
                            <Tabs large>
                                <ul>
                                    {this.renderTabLink(
                                        "Frequently Asked Questions",
                                        1
                                    )}
                                    {this.renderTabLink(
                                        "Terms of Service & Privacy",
                                        2
                                    )}
                                </ul>
                            </Tabs>
                        </Card.Header>
                        <Card.Content>
                            {this.state.activeTab === 1 && <FrequentlyAsked />}
                            {this.state.activeTab === 2 && <TermsOfService />}
                        </Card.Content>
                    </Card>
                </Container>
                <br />
            </Page>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    fetchUser: () => dispatch(userActions.loadUser())
});

const mapStateToProps = state => ({
    isLoading: state.user.isLoading,
    user: state.user.me
});

AboutPage.propTypes = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AboutPage);
