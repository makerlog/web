import React, {Component} from 'react';
import Emoji from 'components/Emoji';
import Page from 'layouts/Page';
import {Button, Card, Control, Field, Input, Message, Title} from 'vendor/bulma';
import {migrateWip} from 'lib/user';
import {Link} from 'react-router-dom';
import {withCurrentUser} from 'features/users';

class MigratePage extends Component {
    state = {
        migrating: false,
        failed: false,
        wipUsername: '',
    }

    onSubmit = async (e) => {
        e.preventDefault()
        try {
            await migrateWip(this.state.wipUsername);
            this.setState({
                migrating: true,
                failed: false,
            })
        } catch (e) {
            this.setState({
                migrating: false,
                failed: true,
            })
        }
    }

    render() {
        return (
            <Page>
                <div style={{marginTop: 20}} className="columns is-mobile">
                    <div className="column is-half is-offset-one-quarter">
                        <Title is={'3'}>
                            Migrate from WIP <Emoji emoji="🚧" />
                        </Title>
                        <p>
                            Moving away from WIP.chat? We'll bring over all your tasks.
                            <br/> It's seamless and only takes a few minutes.
                        </p>
                        <br />

                        {this.state.failed &&
                            <Message danger>
                                <Message.Body>
                                    Something went wrong... Try again later.
                                </Message.Body>
                            </Message>
                        }
                        {!this.state.migrating &&
                            <Card>
                                <Card.Content>
                                    <form onSubmit={this.onSubmit}>
                                        <Field>  
                                            <Control>
                                                <Input value={this.state.wipUsername} onChange={e => this.setState({ wipUsername: e.target.value })} large block placeholder="WIP username" />
                                                <p className="help">
                                                    Note: This is irreversible. All completed & remaining tasks will be imported.
                                                </p>
                                            </Control>
                                        </Field>
                                        <hr />
                                        <Field>
                                            <Control>
                                                <Button primary large>
                                                    Begin
                                                </Button>
                                            </Control>
                                        </Field>
                                    </form>
                                </Card.Content>
                            </Card>
                        }
                        {this.state.migrating &&
                            <Card>
                                <Card.Content>
                                    The migration process has begun. This should take a while. <br />
                                    Keep an eye on <Link to={`/@${this.props.me.username}`}>your feed</Link> to see its progress. It will update in realtime.
                                </Card.Content>
                            </Card>
                        }
                    </div>
                </div>
            </Page>
        );
    }
}

export default withCurrentUser(MigratePage);