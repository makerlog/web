import React from "react";
import { getTask } from "lib/tasks";
import { Redirect } from "react-router-dom";
import { Button, Title } from "vendor/bulma";
import { CommentsBox } from "features/comments";
import { EntryDetail } from "features/stream";
import Helmet from "react-helmet-async";
import Page from "../../layouts/Page";
import UserCard from "../../features/users/UserCard";
import Sticky from "react-stickynode";

class EntryPage extends React.Component {
    state = {
        loading: true,
        task: null,
        notFound: false,
        failed: false
    };

    async fetchEntry() {
        this.setState({ loading: true, failed: false });
        try {
            const task = await getTask(this.props.match.params.id);
            this.setState({ task: task, loading: false, failed: false });
        } catch (e) {
            this.setState({ task: null, loading: false, failed: true });
            if (e.status_code && e.status_code === 404) {
                this.setState({ failed: true, notFound: true });
            } else {
                this.setState({ failed: true });
            }
        }
    }

    componentDidMount() {
        this.fetchEntry();
    }

    render() {
        const { task } = this.state;

        if (this.state.notFound) {
            return <Redirect to="/404" />;
        }

        if (this.state.failed) {
            return (
                <center>
                    <Title>
                        Oops! There was a network error.{" "}
                        <Button text onClick={this.fetchEntry}>
                            Retry
                        </Button>
                    </Title>
                </center>
            );
        }

        return (
            <Page
                contained={true}
                loading={this.state.loading}
                className="EntryPage"
            >
                {this.state.task && this.state.task.user && (
                    <>
                        <Helmet>
                            <title>
                                Done by @{this.state.task.user.username} |
                                Makerlog
                            </title>
                            <meta
                                name="description"
                                content={`${
                                    this.state.task.done ? "✅" : "🕐"
                                } ${this.state.task.content}`}
                            />
                            <meta
                                name="twitter:card"
                                content={
                                    this.state.task.attachment
                                        ? "summary_large_image"
                                        : "summary"
                                }
                            />
                            <meta name="twitter:site" content="@getmakerlog" />
                            <meta
                                name="twitter:title"
                                content={`Done by @${
                                    this.state.task.user.username
                                } on Makerlog`}
                            />
                            <meta
                                name="twitter:description"
                                content={`✅ ${this.state.task.content}`}
                            />
                            <meta
                                name="twitter:image"
                                content={
                                    this.state.task.attachment
                                        ? this.state.task.attachment
                                        : this.state.task.user.avatar
                                }
                            />
                        </Helmet>

                        <div className={"columns"}>
                            <div className={"column is-one-quarter"}>
                                <Sticky
                                    enabled={window.innerWidth >= 728}
                                    top={30}
                                >
                                    <UserCard user={task.user} />
                                </Sticky>
                            </div>
                            <div className={"column"}>
                                <EntryDetail task={task} />
                                <br />
                                <CommentsBox indexUrl={`/tasks/${task.id}/`} />
                            </div>
                        </div>
                    </>
                )}
            </Page>
        );
    }
}

EntryPage.propTypes = {};

export default EntryPage;
