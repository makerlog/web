import React from "react";
import { Button, Card, Heading, Icon, Level, Title } from "vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import Spinner from "../../components/Spinner";
import Streak from "../../components/Streak";
import Tda from "../../components/Tda";
import { connect } from "react-redux";
import { getByUsername } from "../../lib/user";
import { Link, Redirect } from "react-router-dom";
import { getUserStats } from "../../lib/stats";
import Emoji from "../../components/Emoji";
import { FollowButton as GatedFollowButton, FullName } from "features/users";
import { UserStream } from "../../features/stream";
import { ProductList, ProductsContainer } from "features/products";
import Linkify from "react-linkify";
import Helmet from "react-helmet-async";
import Page from "layouts/Page";
import { UserActivityGraph } from "features/stats";
import OutboundLink from "../../components/OutboundLink";
import { getHostname } from "../../lib/utils/products";
import { Avatar } from "../../features/users";

const SocialStatsBadges = ({ children }) => (
    <div className={`social-count`}>{children}</div>
);

const TwitterSocialStats = ({ followers }) => (
    <Icon>
        <FontAwesomeIcon
            size={"lg"}
            icon={["fab", "twitter"]}
            color={"#1DA1F2"}
        />
    </Icon>
);

const GitHubSocialStats = ({ followers }) => (
    <Icon>
        <FontAwesomeIcon size={"lg"} icon={["fab", "github"]} color={"black"} />
    </Icon>
);

const InstagramSocialStats = ({ followers }) => (
    <Icon>
        <FontAwesomeIcon
            size={"lg"}
            icon={["fab", "instagram"]}
            color={"#8a3ab9"}
        />
    </Icon>
);

const ProductHuntSocialStats = ({ followers }) => (
    <Icon>
        <FontAwesomeIcon
            size={"lg"}
            icon={["fab", "product-hunt"]}
            color={"#f07810"}
        />
    </Icon>
);

const TelegramIcon = ({ followers }) => (
    <Icon>
        <FontAwesomeIcon
            size={"lg"}
            icon={["fab", "telegram"]}
            color={"#0088cc"}
        />
    </Icon>
);

const TwitchIcon = ({ followers }) => (
    <Icon>
        <FontAwesomeIcon
            size={"lg"}
            icon={["fab", "twitch"]}
            color={"#6441a5"}
        />
    </Icon>
);

const BuyMeACoffeeIcon = () => (
    <Icon>
        <FontAwesomeIcon size={"lg"} icon={"mug-hot"} color={"#FF813F"} />
    </Icon>
);

class SocialStatsContainer extends React.Component {
    render() {
        if (!this.props.user) return null;

        if (
            !this.props.user.twitter_handle &&
            !this.props.user.github_handle &&
            !this.props.user.instagram_handle &&
            !this.props.user.telegram_handle &&
            !this.props.user.shipstreams_handle &&
            !this.props.user.bmc_handle &&
            !this.props.user.product_hunt_handle
        )
            return null;

        return (
            <SocialStatsBadges>
                {this.props.user.twitter_handle && (
                    <a
                        href={`https://twitter.com/${
                            this.props.user.twitter_handle
                        }`}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <TwitterSocialStats />
                    </a>
                )}
                {this.props.user.github_handle && (
                    <a
                        href={`https://github.com/${
                            this.props.user.github_handle
                        }`}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <GitHubSocialStats />
                    </a>
                )}
                {this.props.user.instagram_handle && (
                    <a
                        href={`https://instagram.com/${
                            this.props.user.instagram_handle
                        }`}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <InstagramSocialStats />
                    </a>
                )}
                {this.props.user.product_hunt_handle && (
                    <a
                        href={`https://producthunt.com/@${
                            this.props.user.product_hunt_handle
                        }`}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <ProductHuntSocialStats />
                    </a>
                )}
                {this.props.user.telegram_handle && (
                    <a
                        href={`https://t.me/${this.props.user.telegram_handle}`}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <TelegramIcon />
                    </a>
                )}
                {this.props.user.shipstreams_handle && (
                    <a
                        href={`https://twitch.tv/${
                            this.props.user.shipstreams_handle
                        }`}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <TwitchIcon />
                    </a>
                )}
                {this.props.user.bmc_handle && (
                    <a
                        href={`https://buymeacoffee.com/${
                            this.props.user.bmc_handle
                        }`}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <BuyMeACoffeeIcon />
                    </a>
                )}
            </SocialStatsBadges>
        );
    }
}

const ProfileActions = ({ user }) => {
    if (!user.bmc_handle && !user.telegram_handle && !user.website) return null;

    return (
        <Card className={"ProfileActions"}>
            <Card.Content>
                {user.website && (
                    <OutboundLink
                        className={"button is-rounded is-fullwidth is-primary"}
                        to={user.website}
                    >
                        <Icon>
                            <FontAwesomeIcon icon={"external-link-alt"} />
                        </Icon>
                        <strong>{getHostname(user.website)}</strong>
                    </OutboundLink>
                )}
                {user.bmc_handle && (
                    <OutboundLink
                        className={
                            "button is-rounded is-fullwidth is-dark bmc-button"
                        }
                        to={`https://buymeacoffee.com/${user.bmc_handle}`}
                    >
                        <Icon>
                            <FontAwesomeIcon icon={"mug-hot"} />
                        </Icon>
                        <strong>Donate</strong>
                    </OutboundLink>
                )}
                {user.telegram_handle && (
                    <OutboundLink
                        className={
                            "button is-rounded is-fullwidth is-dark telegram-button"
                        }
                        to={`https://t.me/${user.telegram_handle}`}
                    >
                        <Icon>
                            <FontAwesomeIcon icon={["fab", "telegram"]} />
                        </Icon>
                        <strong>Send message</strong>
                    </OutboundLink>
                )}
            </Card.Content>
        </Card>
    );
};

export const UserCard = ({ user, withProfilePicture = false }) => (
    <Card className={"user-box" + (withProfilePicture ? " standalone" : "")}>
        <Card.Content>
            {withProfilePicture && (
                <Link to={`/@${user.username}`} className={"profile-picture"}>
                    <Avatar is={128} user={user} />
                </Link>
            )}
            <p className={"bio"}>
                <Linkify
                    properties={{
                        target: "_blank",
                        rel: "nofollow noopener noreferrer"
                    }}
                >
                    {user.description
                        ? user.description
                        : "I have no bio... yet!"}
                </Linkify>
                <SocialStatsContainer user={user} />
            </p>
            <p>
                <GatedFollowButton
                    userId={user.id}
                    className={"is-fullwidth"}
                />
            </p>
        </Card.Content>
    </Card>
);

class ProfileBarStats extends React.Component {
    render() {
        const stats = this.props.stats;

        return (
            <Level.Right className={"ProfileBarStats is-hidden-mobile"}>
                <Level.Item hasTextCentered>
                    <Heading>Streak</Heading>
                    <Title is={"3"}>
                        <Streak days={stats.streak} />
                    </Title>
                </Level.Item>
                <Level.Item />
                <Level.Item hasTextCentered>
                    <Heading>Maker Score</Heading>
                    <Title is={"3"}>
                        <Emoji emoji={"🏆"} /> {stats.maker_score}
                    </Title>
                </Level.Item>
                <Level.Item />
                <Level.Item hasTextCentered>
                    <Heading>Tasks/day</Heading>
                    <Title is={"3"}>
                        <Tda tda={stats.tda} />
                    </Title>
                </Level.Item>
                <Level.Item />
                <Level.Item hasTextCentered>
                    <Heading>Followers</Heading>
                    <Title is={"3"}>
                        <Emoji emoji={"👥"} /> {stats.follower_count}
                    </Title>
                </Level.Item>
            </Level.Right>
        );
    }
}

const ProfileBar = ({ user, stats }) => (
    <React.Fragment>
        <div className={"blur-container"}>
            <div className={"blur"} />
        </div>
        <div className={"stats-container"}>
            <div className={"container"}>
                <div className={"columns is-variable is-5"}>
                    <div
                        className={
                            "column is-3 is-hidden-mobile user-card-container"
                        }
                    >
                        <figure className="image">
                            <img
                                className="img-circle"
                                src={user.avatar}
                                alt={user.username}
                            />
                        </figure>
                    </div>
                    <div className={"column stats-bar"}>
                        <div className={"is-vertical-center"}>
                            <div className={"flex-grow"}>
                                <Title is={"3"} className={"is-brand-green"}>
                                    <FullName user={user} />
                                </Title>
                            </div>
                            <ProfileBarStats stats={stats} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </React.Fragment>
);

class ProfilePage extends React.Component {
    state = {
        isLoading: false,
        user: null,
        stats: null,
        failed: false,
        notFound: false
    };

    getUser = async () => {
        this.setState({ failed: false, notFound: false, isLoading: true });
        try {
            const user = await getByUsername(this.props.match.params.username);
            const stats = await getUserStats(user.id);
            this.setState({ user: user, stats: stats, isLoading: false });
        } catch (e) {
            if (e.status_code && e.status_code === 404) {
                this.setState({ failed: true, notFound: true });
            } else {
                this.setState({ failed: true });
            }
        }
    };

    async componentDidMount() {
        await this.getUser();
    }

    componentDidUpdate(prevProps) {
        if (
            this.props.match.params.username !== prevProps.match.params.username
        ) {
            this.getUser();
        }
    }

    render() {
        const { user } = this.state;

        if (this.state.notFound) {
            return <Redirect to="/404" />;
        }

        if (this.state.isLoading) {
            return <Page loading={true} />;
        }

        if (this.state.failed) {
            return (
                <center>
                    <Title>
                        Oops! There was a network error.{" "}
                        <Button text onClick={this.getUser}>
                            Retry
                        </Button>
                    </Title>
                </center>
            );
        }

        if (!this.state.failed && !this.state.isLoading && this.state.user) {
            return (
                <Page
                    translucent
                    contained={false}
                    footer={false}
                    className="UserPage"
                >
                    <Helmet>
                        <title>@{this.state.user.username} | Makerlog</title>
                        <meta
                            name="description"
                            content={`${
                                this.state.user.username
                            } is on Makerlog, the world's most supportive community of makers shipping together.`}
                        />
                        <meta name="twitter:card" content={"summary"} />
                        <meta name="twitter:site" content="@getmakerlog" />
                        <meta
                            name="twitter:title"
                            content={`@${this.state.user.username} on Makerlog`}
                        />
                        <meta
                            name="twitter:description"
                            content={
                                this.state.user.description
                                    ? this.state.user.description
                                    : `${
                                          this.state.user.username
                                      } is on Makerlog, the world's most supportive community of makers shipping together.`
                            }
                        />
                        {this.state.user.avatar && (
                            <meta
                                name="twitter:image"
                                content={this.state.user.avatar}
                            />
                        )}
                    </Helmet>
                    <section
                        className="hero is-medium is-dark has-brand-gradient"
                        style={
                            this.state.user.header && {
                                backgroundImage: `url(${
                                    this.state.user.header
                                })`
                            }
                        }
                    >
                        <div className="hero-body" />
                        <ProfileBar
                            stats={this.state.stats}
                            user={this.state.user}
                        />
                    </section>

                    <div className={"container"}>
                        <div className={"columns is-variable is-4"}>
                            <div className={"column is-3 card-column"}>
                                <UserCard user={this.state.user} />

                                {user && <ProfileActions user={user} />}

                                <Card>
                                    <Card.Content>
                                        {user && (
                                            <div>
                                                <UserActivityGraph
                                                    user={user}
                                                />
                                            </div>
                                        )}
                                        <ProductsContainer
                                            user={user.id}
                                            component={({ products }) => {
                                                if (!products.length)
                                                    return null;

                                                return (
                                                    <div>
                                                        <hr />
                                                        <ProductList
                                                            thumbnail
                                                            products={products}
                                                        />
                                                    </div>
                                                );
                                            }}
                                        />
                                    </Card.Content>
                                </Card>
                            </div>
                            <div className={"column stream-column"}>
                                <div className={"is-hidden-tablet"}>
                                    <UserCard user={this.state.user} />
                                    <br />
                                </div>
                                <UserStream userId={this.state.user.id} />
                            </div>
                        </div>
                    </div>
                </Page>
            );
        } else {
            return (
                <center>
                    <Spinner />
                </center>
            );
        }
    }
}

ProfilePage.propTypes = {};

const mapStateToProps = state => ({
    me: state.user.me,
    isLoggedIn: state.auth.loggedIn
});

export default connect(mapStateToProps)(ProfilePage);
