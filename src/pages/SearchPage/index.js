import React from 'react';
import {Route, withRouter} from 'react-router-dom';
import {Switch} from "react-router";
import Page from 'layouts/Page';
import {Input, Media} from "../../vendor/bulma";
import {StreamCard as Card} from "features/stream/components/Stream/components/StreamCard/styled";
import InfiniteSearch from "../../features/search/components/InfiniteSearch";
import {searchDiscussions, searchProducts, searchTasks, searchUsers} from "../../lib/search";
import {ProductList} from "features/products";
import {Avatar} from "features/users";
import {Task} from "../../features/stream";
import UserMediaList from "../../features/users/components/UserMediaList/UserMediaList";
import Sticky from 'react-stickynode';
import SidebarLink from "../../components/SidebarLink";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import ThreadList from "../../features/discussions/components/ThreadList";

class SearchPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            query: this.getQuery() ? this.getQuery() : ''
        }
    }

    componentDidUpdate(prevProps) {
        if (this.getQuery() !== '' && this.getQuery() !== this.state.query) {
            this.setState({
                query: this.getQuery() ? this.getQuery() : ''
            })
        }
    }

    getQuery = () => {
        const urlParams = new URLSearchParams(window.location.search);
        return urlParams.get('q') ? urlParams.get('q') : ''
    }

    renderProductSearch = () => (
        <InfiniteSearch
            query={this.state.query}
            searchFunc={searchProducts}
            component={props => <ProductList media products={props.items} />} />
    )

    renderDiscussionSearch = () => (
        <InfiniteSearch
            query={this.state.query}
            searchFunc={searchDiscussions}
            component={props => <ThreadList threads={props.items} />} />
    )

    renderTaskSearch = () => (
        <InfiniteSearch
            query={this.state.query}
            searchFunc={searchTasks}
            component={props => (
                <div>
                    {props.items.map(t => (
                        <Media key={t.id}>
                            <Media.Left>
                                <Avatar user={t.user} is={32} />
                            </Media.Left>
                            <Media.Content>
                                <Task task={t} />
                            </Media.Content>
                        </Media>
                    ))}
                </div>
            )} />
    )

    renderUserSearch = () => (
        <InfiniteSearch
            query={this.state.query}
            searchFunc={searchUsers}
            component={props => <UserMediaList users={props.items} />} />
    )

    render() {
        return (
            <Page>
                <div className={"columns"}>
                    <div className={"column is-one-fifth"}>
                        <Sticky enabled={window.innerWidth >= 728} top={30}>
                            <Card>
                                <Card.Content>
                                    <SidebarLink
                                        active={this.props.location.pathname === '/search/products' || this.props.location.pathname === '/search' }
                                        onClick={e => this.props.history.push('/search/products')}>
                                        <span className={"menu-icon"}><FontAwesomeIcon icon={'ship'} /></span>
                                        <span>Products</span>
                                    </SidebarLink>
                                    <SidebarLink
                                        active={this.props.location.pathname === '/search/makers'}
                                        onClick={e => this.props.history.push('/search/makers')}>
                                        <span className={"menu-icon"}><FontAwesomeIcon icon={'users'} /></span>
                                        <span>Makers</span>
                                    </SidebarLink>
                                    <SidebarLink
                                        active={this.props.location.pathname === '/search/tasks'}
                                        onClick={e => this.props.history.push('/search/tasks')}>
                                        <span className={"menu-icon"}><FontAwesomeIcon icon={'check-circle'} /></span>
                                        <span>Tasks</span>
                                    </SidebarLink>
                                    <SidebarLink
                                        active={this.props.location.pathname === '/search/discussions'}
                                        onClick={e => this.props.history.push('/search/discussions')}>
                                        <span className={"menu-icon"}><FontAwesomeIcon icon={'comment'} /></span>
                                        <span>Discussions</span>
                                    </SidebarLink>
                                </Card.Content>
                            </Card>
                        </Sticky>
                    </div>
                    <div className={"column"}>
                        <Card>
                            <Card.Header>
                                <Input
                                    value={this.state.query}
                                    onChange={e => this.setState({query: e.target.value})}
                                    medium
                                    className={"is-fullwidth is-rounded"}
                                    placeholder={"Search anything..."} />
                            </Card.Header>
                            <Card.Content>
                                <Switch>
                                    <Route exact path='/search' component={this.renderProductSearch}/>
                                    <Route exact path='/search/products' component={this.renderProductSearch}/>
                                    <Route exact path='/search/makers' component={this.renderUserSearch}/>
                                    <Route exact path='/search/tasks' component={this.renderTaskSearch}/>
                                    <Route exact path='/search/discussions' component={this.renderDiscussionSearch}/>
                                </Switch>
                            </Card.Content>
                        </Card>
                    </div>
                </div>
            </Page>
        )
    }
}

SearchPage.propTypes = {}

export default withRouter(SearchPage);