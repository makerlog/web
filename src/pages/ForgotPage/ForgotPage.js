import React from "react";
import {
    Button,
    Card,
    Container,
    Control,
    Field,
    Input,
    Level,
    Title
} from "vendor/bulma";
import queryString from "query-string";
import axios from "axios";
import { Redirect } from "react-router-dom";
import ErrorMessageList from "../../components/forms/ErrorMessageList";
import { prettyAxiosError } from "../../lib/utils/error";
import Page from "../../layouts/Page";

class ResetForm extends React.Component {
    state = {
        password: "",
        repeatPassword: "",
        success: false,
        loading: false,
        failed: false,
        errorMessages: null
    };

    onSubmit = e => {
        e.preventDefault();
        this.resetPassword();
    };

    resetPassword = async () => {
        try {
            this.setState({ success: false, failed: false, loading: true });
            const uid = this.props.uid;
            const token = this.props.token;

            await axios.post("/accounts/reset/", {
                uidb64: uid,
                token: token,
                repeat_password: this.state.repeatPassword,
                password: this.state.password
            });
            this.setState({ success: true, failed: false, loading: false });
        } catch (e) {
            try {
                prettyAxiosError(e);
            } catch (e) {
                if (e.field_errors) {
                    this.setState({
                        failed: true,
                        success: false,
                        loading: false,
                        errorMessages: e.field_errors
                    });
                } else {
                    this.setState({
                        failed: true,
                        success: false,
                        loading: false,
                        errorMessages: null
                    });
                }
            }
        }
    };

    render() {
        if (this.state.success) {
            return <Redirect to={"/login"} />;
        }

        return (
            <form onSubmit={this.onSubmit}>
                {this.state.failed && this.state.errorMessages && (
                    <ErrorMessageList fieldErrors={this.state.errorMessages} />
                )}
                <Field>
                    <label className="label">Password</label>
                    <Control>
                        <Input
                            value={this.state.password}
                            onChange={e =>
                                this.setState({ password: e.target.value })
                            }
                            placeholder="Password"
                            type={"password"}
                        />
                    </Control>
                </Field>
                <Field>
                    <label className="label">Password</label>
                    <Control>
                        <Input
                            value={this.state.repeatPassword}
                            onChange={e =>
                                this.setState({
                                    repeatPassword: e.target.value
                                })
                            }
                            placeholder="Repeat password"
                            type={"password"}
                        />
                    </Control>
                </Field>
                <Level>
                    <Level.Left />
                    <Level.Right>
                        <Button
                            onClick={this.onSubmit}
                            type="submit"
                            primary
                            className={"is-rounded"}
                            loading={this.state.loading}
                        >
                            Change password
                        </Button>
                    </Level.Right>
                </Level>
            </form>
        );
    }
}

class ForgotForm extends React.Component {
    state = {
        email: "",
        success: false,
        loading: false,
        failed: false
    };

    onSubmit = async e => {
        e.preventDefault();
        try {
            this.setState({
                loading: true,
                success: false,
                failed: false
            });
            await axios.post("/accounts/forgot/", {
                email: this.state.email
            });
            this.setState({
                email: "",
                success: true,
                loading: false,
                failed: false
            });
        } catch (e) {
            this.setState({
                success: false,
                loading: false,
                failed: true
            });
        }
    };

    render() {
        if (this.state.success || this.state.failed) {
            return (
                <p>
                    If an email with this address exists, we've sent an email.
                </p>
            );
        }

        return (
            <form onSubmit={this.onSubmit}>
                <Field>
                    <label className="label">Email</label>
                    <Control>
                        <Input
                            value={this.state.email}
                            onChange={e =>
                                this.setState({ email: e.target.value })
                            }
                            placeholder="Email"
                        />
                    </Control>
                </Field>
                <Level>
                    <Level.Left />
                    <Level.Right>
                        <Button
                            onClick={this.onSubmit}
                            type="submit"
                            primary
                            className={"is-rounded"}
                            loading={this.state.loading}
                        >
                            Reset
                        </Button>
                    </Level.Right>
                </Level>
            </form>
        );
    }
}

class ForgotPage extends React.Component {
    state = {
        hasCode: false
    };

    componentDidMount() {
        let params = queryString.parse(this.props.location.search);
        this.params = params;
        if (params.uid && params.token) {
            this.setState({ hasCode: true });
        }
    }

    render() {
        return (
            <Page
                contained={false}
                footer={false}
                transparent
                className={"LoginPage"}
            >
                <Container>
                    <div className="columns form-container">
                        <div className={"column is-one-third"}>
                            <Title className={"has-text-white"}>Forgot?</Title>
                            <Card>
                                <Card.Content>
                                    {this.state.hasCode ? (
                                        <ResetForm
                                            uid={this.params.uid}
                                            token={this.params.token}
                                        />
                                    ) : (
                                        <ForgotForm />
                                    )}
                                </Card.Content>
                            </Card>
                        </div>
                    </div>
                </Container>
            </Page>
        );
    }
}

ForgotPage.propTypes = {};

export default ForgotPage;
