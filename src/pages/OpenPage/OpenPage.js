import React from 'react';
import {Card, Container, Heading, Hero, Level, SubTitle, Title} from 'vendor/bulma';
import {Bar as BarChart} from 'react-chartjs-2';
import axios from 'axios';
import Streak from 'components/Streak';
import Page from 'layouts/Page';
import Praise from "../../components/Praise";
import {socketUrl} from "../../lib/utils/random";
import ReconnectingWebSocket from "reconnecting-websocket/dist/reconnecting-websocket";

class NewUsersGraph extends React.Component {
    render() {
        return <BarChart data={this.props.data} />
    }
}

class TasksDayGraph extends React.Component {
    render() {
        return <BarChart data={this.props.data} />
    }
}

class LiveCount extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            count: this.props.initialCount,
        }
    }

    componentDidMount() {
        this.connect();
    }

    componentWillUnmount() {
        this.disconnect()
    }

    connect = () => {
        this.socket = new ReconnectingWebSocket(socketUrl(this.props.indexUrl));
        this.socket.onopen = () => {
            console.log(`Makerlog/LiveCount: Established connection to ${this.props.indexUrl}.`)
        }
        this.socket.onmessage = this.onWsEvent
        this.socket.onclose = () => {
            console.log(`Makerlog/LiveCount: Closed connection to ${this.props.indexUrl}.`)
        }
    }

    onWsEvent = (event) => {
        const data = JSON.parse(event.data)
        switch (data.type) {
            case 'task.created':
                this.setState({
                    count: this.state.count + 1
                });
                break;

            case 'task.deleted':
                this.setState({
                    count: this.state.count - 1
                });
                break;

            default:
                return;
        }
    }

    disconnect = () => {
        if (this.socket) {
            this.socket.close()
        }
    }

    render() {
        let Component = this.props.component;

        return <Component count={this.state.count} />
    }
}

class OpenPage extends React.Component {
    state = {
        loading: true,
        data: null,
        failed: false,
    }

    componentDidMount() {
        this.fetchGraphData()
    }

    fetchGraphData = async () => {
        try {
            let data = await axios.get('/open/')
            data = data.data
            let tda1 = data.done_per_day;
            let tda2 = data.remaining_per_day;
            let telegram = data.telegram_per_day;
            let slack = data.slack_per_day;
            let webhook_per_day = data.webhook_per_day;
            let todoist_per_day = data.todoist_per_day;
            let trello_per_day = data.trello_per_day;
            let git_per_day = data.git_per_day;


            data.users_per_day_graph = {
                labels: Object.keys(data.new_users_per_day),
                datasets: [
                    {
                        label: 'Signups/day',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',
                        data: Object.values(data.new_users_per_day)
                    }
                ]
            }

            data.tasks_per_day_graph = {
                labels: Object.keys(tda1),
                datasets: [
                    {
                        label: 'Completed tasks per day',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',

                        data: Object.values(tda1)
                    },
                    {
                        label: 'Remaining tasks per day',
                        borderWidth: 1,
                        data: Object.values(tda2)
                    }
                ]
            }

            data.telegram_graph = {
                labels: Object.keys(telegram),
                datasets: [
                    {
                        label: 'Telegram tasks per day',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',

                        data: Object.values(telegram)
                    },
                ]
            }

            data.slack_graph = {
                labels: Object.keys(slack),
                datasets: [
                    {
                        label: 'Slack tasks per day',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',

                        data: Object.values(slack)
                    },
                ]
            }

            data.webhook_graph = {
                labels: Object.keys(webhook_per_day),
                datasets: [
                    {
                        label: 'Webhook tasks per day',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',

                        data: Object.values(webhook_per_day)
                    },
                ]
            }

            data.todoist_graph = {
                labels: Object.keys(todoist_per_day),
                datasets: [
                    {
                        label: 'Todoist tasks per day',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',

                        data: Object.values(todoist_per_day)
                    },
                ]
            }

            data.trello_graph = {
                labels: Object.keys(trello_per_day),
                datasets: [
                    {
                        label: 'Trello tasks per day',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',

                        data: Object.values(trello_per_day)
                    },
                ]
            }

            data.git_graph = {
                labels: Object.keys(git_per_day),
                datasets: [
                    {
                        label: 'Git tasks per day',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',

                        data: Object.values(git_per_day)
                    },
                ]
            }

            console.log(data)


            this.setState({
                loading: false,
                data: data,
                failed: false,
            })
        } catch (e) {

        }
    }

    render() {
        return (
            <Page className="OpenPage" loading={this.state.loading} contained={false}>
                <Hero primary>
                    <Container>
                        <Hero.Body>
                            <div className={"columns"}>
                                <div className={"column"}>
                                    <Title is={"3"}>
                                        Makerlog is an open product.
                                    </Title>
                                    <SubTitle>
                                        We openly share statistics on growth and worldwide productivity.
                                    </SubTitle>
                                </div>
                            </div>
                        </Hero.Body>
                    </Container>
                </Hero>
                
                <br />

                <Container>

                    {this.state.data &&
                        <Card>
                            <Card.Content>
                                <Level>
                                    <Level.Item hasTextCentered>
                                        <div>
                                            <Heading>Users</Heading>
                                            <Title is={3}>{this.state.data && this.state.data.user_count.toLocaleString()}</Title>
                                        </div>
                                    </Level.Item>
                                    <Level.Item hasTextCentered>
                                        <div>
                                            <Heading>MRR</Heading>
                                            <Title is={3}>$200</Title>
                                        </div>
                                    </Level.Item>
                                    <Level.Item hasTextCentered>
                                        <div>
                                            <Heading>All tasks</Heading>
                                            {
                                                this.state.data &&
                                                    <LiveCount
                                                        initialCount={this.state.data.tasks_all}
                                                        indexUrl={'/explore/stream/'}
                                                        component={({ count }) => (
                                                        <Title is={3}>{count.toLocaleString()}</Title>
                                                    )} />
                                            }
                                        </div>
                                    </Level.Item>
                                    <Level.Item hasTextCentered>
                                        <div>
                                            <Heading>Tasks done</Heading>
                                            <Title is={3}>{this.state.data && this.state.data.tasks_done.toLocaleString()}</Title>
                                        </div>
                                    </Level.Item>
                                    <Level.Item hasTextCentered>
                                        <div>
                                            <Heading>Tasks remaining</Heading>
                                            <Title is={3}>{this.state.data && (this.state.data.tasks_all - this.state.data.tasks_done).toLocaleString()}</Title>
                                        </div>
                                    </Level.Item>
                                    <Level.Item hasTextCentered>
                                        <div>
                                            <Heading>Products</Heading>
                                            <Title is={3}>{this.state.data && this.state.data.products_total}</Title>
                                        </div>
                                    </Level.Item>
                                    <Level.Item hasTextCentered>
                                        <div>
                                            <Heading>Launches</Heading>
                                            <Title is={3}>{this.state.data && this.state.data.products_launched}</Title>
                                        </div>
                                    </Level.Item>
                                    <Level.Item hasTextCentered>
                                        <div>
                                            <Heading>Praise given</Heading>
                                            <Title is={3}>{this.state.data && <Praise praise={this.state.data.praise_given} />}</Title>
                                        </div>
                                    </Level.Item>
                                    <Level.Item hasTextCentered>
                                        <div>
                                            <Heading>Highest streak</Heading>
                                            <Title is={3}>{this.state.data && <Streak days={this.state.data.highest_streak} />}</Title>
                                        </div>
                                    </Level.Item>
                                </Level>
                            </Card.Content>
                        </Card>
                    }
                    <br />
                    <div className="columns">
                        <div className="column">
                            {this.state.data && this.state.data.users_per_day_graph &&
                                <Card>
                                    <Card.Content>
                                        <NewUsersGraph data={this.state.data.users_per_day_graph} />
                                    </Card.Content>
                                </Card>
                            }
                            <br />
                            {this.state.data && this.state.data.slack_graph &&
                                <Card>
                                    <Card.Content>
                                        <TasksDayGraph data={this.state.data.slack_graph} />
                                    </Card.Content>
                                </Card>
                            }
                            <br />
                            {this.state.data && this.state.data.telegram_graph &&
                            <Card>
                                <Card.Content>
                                    <TasksDayGraph data={this.state.data.telegram_graph} />
                                </Card.Content>
                            </Card>
                            }
                            <br />
                            {this.state.data && this.state.data.trello_graph &&
                                <Card>
                                    <Card.Content>
                                        <TasksDayGraph data={this.state.data.trello_graph} />
                                    </Card.Content>
                                </Card>
                            }
                            <br />
                        </div>
                        <div className="column">
                            {this.state.data && this.state.data.tasks_per_day_graph &&
                                <Card>
                                    <Card.Content>
                                        <TasksDayGraph data={this.state.data.tasks_per_day_graph} />
                                    </Card.Content>
                                </Card>
                            }
                            <br />
                            {this.state.data && this.state.data.webhook_graph &&
                            <Card>
                                <Card.Content>
                                    <TasksDayGraph data={this.state.data.webhook_graph} />
                                </Card.Content>
                            </Card>
                            }
                            <br />
                            {this.state.data && this.state.data.todoist_graph &&
                            <Card>
                                <Card.Content>
                                    <TasksDayGraph data={this.state.data.todoist_graph} />
                                </Card.Content>
                            </Card>
                            }
                            <br />
                            {this.state.data && this.state.data.git_graph &&
                            <Card>
                                <Card.Content>
                                    <TasksDayGraph data={this.state.data.git_graph} />
                                </Card.Content>
                            </Card>
                            }
                            <br />
                        </div>
                    </div>
                </Container>
                <br />
            </Page>
        )
    }
}

OpenPage.propTypes = {}

export default OpenPage;