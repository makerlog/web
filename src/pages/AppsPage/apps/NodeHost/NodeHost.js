import React from 'react';
import {connect} from 'react-redux';
import {Card, Container, Hero, Message, SubTitle, Title} from "vendor/bulma";
import {mapDispatchToProps, mapStateToProps} from "ducks/apps";
import Spinner from "components/Spinner";
import {WebhookTable} from '../Webhooks/components/WebhooksTable';
import AppWebhookCreator from '../Webhooks/components/AppWebhookCreator';

class NodeHost extends React.Component {
    render() {
        const style = {
            backgroundColor: "#2282f8",
            borderRadius: 5,
            color: 'white',
        }

        if (this.props.isLoading && !this.props.apps) {
            return <Spinner />
        }

        const params = new URLSearchParams(this.props.location.search); 
        const success = params.get('success');

        return (
            <div>
                <Hero style={style} dark>
                    <Hero.Body>
                        <Container>
                            <Title white>
                                NodeHost
                            </Title>
                            <SubTitle>
                                Log new websites and other tasks from NodeHost instantly.
                            </SubTitle>
                        </Container>
                    </Hero.Body>
                </Hero>
                <br />
                <div className={"container"}>
                    <SubTitle is='5'>Link to project</SubTitle>
                    <Card>
                        <Card.Content>
                            Select a project and NodeHost will post to it. It's seamless.
                            <hr />
                            {success && <Message success><Message.Body>Done! Your NodeHost account is now linked to Makerlog.</Message.Body></Message>}
                            <AppWebhookCreator appName="NodeHost" identifier="nodehost" sendKeyTo={"https://nodehost.cloud/webhook/account_integrations_makerlog"} />
                        </Card.Content>
                    </Card>
                    <br />
                    <SubTitle is='5'>Active webhooks</SubTitle>
                    <Card>
                        <Card.Content>

                            {this.props.apps['nodehost'] && this.props.apps['nodehost'].installed &&
                            <WebhookTable webhooks={this.props.apps['nodehost'].webhooks} />
                            }
                            {this.props.apps['nodehost'] && !this.props.apps['nodehost'].installed &&
                            <center><SubTitle>Nothing here.</SubTitle></center>
                            }
                        </Card.Content>
                    </Card>
                </div>
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NodeHost);