import React from 'react';
import {connect} from 'react-redux';
import {Container, Hero, SubTitle, Title} from "vendor/bulma";
import SlackInstallCard from "./components/SlackInstallCard";
import {mapDispatchToProps, mapStateToProps} from "ducks/apps";
import Spinner from "components/Spinner";

class Slack extends React.Component {
    render() {
        const style = {
            backgroundColor: "#e6186d",
            color: 'white'
        }

        if (this.props.isLoading && !this.props.apps) {
            return <Spinner />
        }

        return (
            <div>
                <Hero style={style} dark>
                    <Hero.Body>
                        <Container>
                            <Title white>
                                Slack
                            </Title>
                            <SubTitle>
                                Seamlessly add logs from Slack, and stay updated right from your chat client.
                            </SubTitle>
                        </Container>
                    </Hero.Body>
                </Hero>
                <Container>
                    <br />
                    <SlackInstallCard linkKey={this.props.linkKey} />
                </Container>
            </div>
        )
    }
}

Slack.propTypes = {}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Slack);