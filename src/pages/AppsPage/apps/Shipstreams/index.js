import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Card, Container, Hero, SubTitle, Title} from "vendor/bulma";
import {mapDispatchToProps, mapStateToProps} from "ducks/apps";
import Spinner from "components/Spinner";
import {me, patchSettings} from "../../../../lib/user";
import {Button, Control, Field, Input} from "../../../../vendor/bulma";

class ShipstreamsSettings extends React.Component {
    state = {
        loading: true,
        submitting: false,
        done: false,
        me: null,
        shipstreamsHandle: '',
        failed: false,
    }

    componentDidMount() {
        this.fetchData()
    }

    fetchData = async () => {
        this.setState({ loading: true, failed: false })
        try {
            const user = await me();
            this.setState({
                shipstreamsHandle: user.shipstreams_handle,
                me: user,
                loading: false, failed: false,
            })
        } catch (e) {
            this.setState({
                loading: false,
                failed: true
            })
        }
    }

    onSubmit = async (e) => {
        e.preventDefault()
        this.setState({ submitting: true, failed: false,  })
        try {
            const me = await patchSettings({ shipstreams_handle: this.state.shipstreamsHandle })
            this.setState({ submitting: false, done: true, failed: false, me })
            setTimeout(() => this.setState({ done: false}), 3000)
        } catch (e) {
            this.setState({
                submitting: false,
                failed: true
            })
        }
    }

    onDelete = async e => {
        await this.setState({ deleting: true, shipstreamsHandle: "" })
        await this.onSubmit(e)
        await this.setState({ deleting: false })
    }

    render() {
        if (this.state.loading) return <Card><Card.Content><Spinner /></Card.Content></Card>
        if (this.state.failed) return <Card><Card.Content><center><button onClick={this.fetchData}>Try again</button></center></Card.Content></Card>

        return (
            <Card>
                <Card.Content>
                    <form onSubmit={this.onSubmit}>
                        <Field>
                            <label className="label">Shipstreams username</label>
                            <Control>
                                <Input value={this.state.shipstreamsHandle} onChange={e => this.setState({ shipstreamsHandle: e.target.value })} medium placeholder="sergiomattei" />
                            </Control>
                        </Field>
                        <Field>
                            {!this.state.deleting &&
                                <Button disabled={this.state.done} type={"submit"} medium className={"is-fullwidth"} primary loading={this.state.submitting}>
                                    {this.state.done ? 'Linked!' : 'Link'}
                                </Button>
                            }
                        </Field>
                        <Field>
                            {((this.state.me.shipstreams_handle && this.state.me.shipstreams_handle.length > 0) || this.state.deleting) &&
                                <Button onClick={this.onDelete} medium className={"is-fullwidth"} danger loading={this.state.submitting}>
                                    Unlink
                                </Button>
                            }
                        </Field>
                    </form>
                </Card.Content>
            </Card>
        )
    }
}

class Shipstreams extends React.Component {
    render() {
        if (this.props.isLoading && !this.props.apps) {
            return <Spinner />
        }

        return (
            <div className="Trello">
                <Hero info>
                    <Hero.Body>
                        <Container>
                            <Title white>
                                Shipstreams
                            </Title>
                            <SubTitle>
                                Log when you go live and showcase your streams!
                            </SubTitle>
                        </Container>
                    </Hero.Body>
                </Hero>
                <br />
                <Container>
                    <div className={"columns"}>
                        <div className={"column is-one-third is-offset-one-third"}>
                            <Title is={"5"}>Link your Shipstreams.com account</Title>
                            <ShipstreamsSettings />
                        </div>
                    </div>
                </Container>
            </div>
        )
    }
}

Shipstreams.propTypes = {}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withRouter(Shipstreams));