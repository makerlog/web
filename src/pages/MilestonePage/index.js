import React from "react";
import { Redirect } from "react-router-dom";
import { Button, Title } from "vendor/bulma";
import { getMilestoneBySlug } from "../../lib/milestones";
import Helmet from "react-helmet-async";
import Page from "layouts/Page";

import "./index.scss";
import MilestoneMedia from "../../features/milestones/components/MilestoneMedia";
import { Card } from "../../vendor/bulma";
import { CommentsBox } from "features/comments";
import Sticky from "react-stickynode";
import UserCard from "../../features/users/UserCard";

class MilestonePage extends React.Component {
    state = {
        loading: false,
        milestone: null,
        failed: false,
        notFound: false
    };

    getMilestone = async () => {
        this.setState({ failed: false, notFound: false, loading: true });
        try {
            const milestone = await getMilestoneBySlug(
                this.props.match.params.slug
            );
            this.setState({ milestone, loading: false });
        } catch (e) {
            if (e.status_code && e.status_code === 404) {
                this.setState({ failed: true, notFound: true });
            } else {
                this.setState({ failed: true });
            }
        }
    };

    componentDidMount() {
        this.getMilestone();
    }

    render() {
        const { milestone } = this.state;

        if (this.state.notFound) {
            return <Redirect to="/404" />;
        }

        if (this.state.loading) {
            return <Page loading={true} />;
        }

        if (this.state.failed) {
            return (
                <center>
                    <Title>
                        Oops! There was a network error.{" "}
                        <Button text onClick={this.getMilestone}>
                            Retry
                        </Button>
                    </Title>
                </center>
            );
        }

        if (!this.state.failed && !this.state.loading && milestone) {
            return (
                <Page className="MilestonePage">
                    <Helmet>
                        <title>{milestone.title} | Makerlog</title>
                        <meta
                            name="description"
                            content={`${
                                milestone.title
                            } was achieved on Makerlog, the world's most supportive community of makers shipping together.`}
                        />
                        <meta name="twitter:card" content={"summary"} />
                        <meta name="twitter:site" content="@getmakerlog" />
                        <meta
                            name="twitter:title"
                            content={`${milestone.title} on Makerlog`}
                        />
                        <meta
                            name="twitter:description"
                            content={`${
                                milestone.title
                            } was achieved on Makerlog, the world's most supportive community of makers shipping together.`}
                        />
                        {milestone.icon && (
                            <meta
                                name="twitter:image"
                                content={milestone.icon}
                            />
                        )}
                    </Helmet>

                    <div className={"columns"}>
                        <div className={"column is-one-quarter"}>
                            <Sticky enabled={window.innerWidth >= 728} top={30}>
                                <UserCard user={milestone.user} />
                            </Sticky>
                        </div>
                        <div className={"column"}>
                            <Card>
                                <Card.Content>
                                    <MilestoneMedia
                                        large
                                        linked={false}
                                        milestone={milestone}
                                    />
                                </Card.Content>
                            </Card>
                            <br />
                            <CommentsBox
                                indexUrl={`/milestones/${milestone.slug}/`}
                            />
                        </div>
                    </div>
                </Page>
            );
        } else {
            return <Page loading />;
        }
    }
}

MilestonePage.propTypes = {};

export default MilestonePage;
