import React from 'react';
import {mapDispatchToProps, mapStateToProps} from 'ducks/apps';
import {connect} from 'react-redux';
import AppsList from '../components/AppsList';
import './Shop.css';
import Spinner from '../../../../../components/Spinner';

class Shop extends React.Component {

    render() {
        if (this.props.isLoading && !this.props.apps) {
            return <Spinner />
        }

        return (
            <div className="Shop">
                <AppsList />
            </div>
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Shop);