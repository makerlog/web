import React from 'react';
import CreateProductForm from '../../../../../MyProductsPage/components/CreateProductForm';

class CreateProductPage extends React.Component {
    render() {
        return (
            <div className="CreateProductPage">
            <br />
                <CreateProductForm />
            </div>
        )
    }
}

CreateProductPage.propTypes = {}

export default CreateProductPage;